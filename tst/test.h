#pragma once

#include <chrono>
#include <vector>

int g_status = EXIT_SUCCESS;
int g_passed = 0;
std::vector<int> g_failed;
auto g_start = std::chrono::steady_clock::now();
auto g_end = std::chrono::steady_clock::now();

#define MSG_INPUT_OTHER( _dest_ )       MsgBus::get_msg( msg_t::endpoint_t::_dest_, 0, false, &__l_msg )
#define CLEAR_MSG_Q()                   MsgBus::clear()
#define REGISTER_MSG_THREAD( _self_ )   MsgBus::register_thread( msg_t::endpoint_t::_self_ )

#define TC( _name_ )                std::cout << KCYN << "----------------------------------------\nRunning " << #_name_ << "\n" << KNRM << std::flush;\
                                    g_start = std::chrono::steady_clock::now();\
                                    _name_();\
                                    g_end = std::chrono::steady_clock::now();\
                                    std::cout << "  Test took " << std::chrono::duration_cast<std::chrono::microseconds>( g_end - g_start ).count() << " useconds\n" << KCYN << "----------------------------------------\n" << KNRM << std::flush

#define ASSERT( _cond_ )            do{\
                                        if( _cond_ ){\
                                            g_passed++;\
                                            std::cout << KGRN << "SUCCESS" << KNRM << std::flush;\
                                        }\
                                        else{\
                                            std::stringstream l_ss;\
                                            g_failed.push_back( __LINE__ );\
                                            std::cout << KRED << "FAILED" << KNRM << "  " << #_cond_ << std::flush;\
                                            g_status = EXIT_FAILURE;\
                                        }\
                                        std::cout << " on line " << __LINE__ << "\n" << std::flush;\
                                    } while( 0 )

#define SUMMARY()                   do{\
                                        if( g_failed.empty() ){\
                                            std::cout << KGRN << std::flush;\
                                        }\
                                        else{\
                                            std::cout << KRED << std::flush;\
                                        }\
                                        std::cout << "================ SUMMARY ===============\n" << g_passed << " / " << g_passed + g_failed.size() << " assertions passed\n" << std::flush;\
                                        if( !g_failed.empty() ){\
                                            std::cout << "Failed assertions:\n" << std::flush;\
                                            for( const int i_line : g_failed ){\
                                                std::cout << "Line " << i_line << "\n" << std::flush;\
                                            }\
                                        }\
                                        std::cout << "========================================\n" << KNRM << std::flush;\
                                    } while( 0 )
