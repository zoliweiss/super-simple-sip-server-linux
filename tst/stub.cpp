#include <sys/socket.h>
#include <sys/eventfd.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <poll.h>
#include <chrono>
#include <thread>
#include <vector>
#include <string>
#include <queue>
#include <map>
#include <set>
#include <iostream>
#include <sstream>
#include <shared_mutex>
#include <mutex>

/***************************************/
/**************** MACROS ***************/
/***************************************/
#define STUBLOG 0

/***************************************/
/*** CLASS AND STRUCT IMPLEMENTATION ***/
/***************************************/
class StubLogger
{
public:
    StubLogger() = default;

    ~StubLogger()
    {
#if STUBLOG
        std::cout << ss.str() << std::endl << std::flush;
#endif
    }

    template <class T>
    StubLogger& operator <<( const T &t )
    {
        ss << t << " ";
        return *this;
    }

private:
    std::stringstream ss;
};

struct BindElem
{
    int         socket = 0;
    std::string address;
};

struct Msg
{
    std::string address;
    std::string data;
};

int                            g_socket = 0;
std::vector<BindElem>          g_bindelems;
std::map<int, std::queue<Msg>> g_msg_q_map;
std::multiset<int>             g_socket_activity;
std::shared_mutex              g_mtx;

static int get_next_socket( void )
{
    return ++g_socket;
}

static std::string get_address_by_socket( const int socket )
{
    for( const BindElem &elem : g_bindelems )
    {
        if( elem.socket == socket )
        {
            return elem.address;
        }
    }

    return "";
}

static int get_socket_by_address( const std::string &address )
{
    for( const BindElem &elem : g_bindelems )
    {
        if( elem.address == address )
        {
            return elem.socket;
        }
    }

    return -1;
}

/***************************************/
/*********** STUBBED FUNCTIONS *********/
/***************************************/
extern "C" __attribute__ ((visibility ("hidden")))
int rand( void )
{
    StubLogger() << "STUB RAND";

    return 42;
}

extern "C" __attribute__ ((visibility ("hidden")))
time_t time( time_t* )
{
    StubLogger() << "STUB TIME";

    return 1'672'039'848;
}

extern "C" __attribute__ ((visibility ("hidden")))
int eventfd( unsigned int, int )
{
    StubLogger() << "STUB EVENTFD";

    return get_next_socket();
}

extern "C" __attribute__ ((visibility ("hidden")))
int socket( int, int, int )
{
    StubLogger() << "STUB SOCKET";

    return get_next_socket();
}

extern "C" __attribute__ ((visibility ("hidden")))
int close( int socket )
{
    StubLogger() << "STUB CLOSE";

    for( auto it = g_bindelems.begin(); it != g_bindelems.end(); it++ )
    {
        if( it->socket == socket )
        {
            std::unique_lock l_lock( g_mtx );

            g_bindelems.erase( it );
            g_socket_activity.extract( socket );
            break;
        }
    }

    return 0;
}

extern "C" __attribute__ ((visibility ("hidden")))
int bind( int socket, const struct sockaddr *address, socklen_t addrlen )
{
    StubLogger() << "STUB BIND" << socket;

    std::string l_address;

    l_address.assign( reinterpret_cast<const char*>( address ), addrlen );
    g_bindelems.push_back( { socket, l_address } );

    return 0;
}

extern "C" __attribute__ ((visibility ("hidden")))
ssize_t sendto( int socket, const void *buf, size_t buflen, int, const struct sockaddr *dest_address, socklen_t addrlen )
{
    StubLogger() << "STUB SENDTO" << "socket:" << socket;

    std::string l_dest_address;

    l_dest_address.assign( reinterpret_cast<const char*>( dest_address ), addrlen );

    const std::string l_source_address = get_address_by_socket( socket );
    const int         l_dest_socket = get_socket_by_address( l_dest_address );

    if( l_source_address.empty()
     || -1 == l_dest_socket )
    {
        return -1;
    }

    std::string l_data;

    l_data.assign( reinterpret_cast<const char*>( buf ), buflen );

    std::unique_lock l_lock( g_mtx );

    g_msg_q_map[l_dest_socket].push( { l_source_address, l_data } );
    g_socket_activity.insert( l_dest_socket );

    return buflen;
}

extern "C" __attribute__ ((visibility ("hidden")))
ssize_t recvfrom( int socket, void *buf, size_t buflen, int, struct sockaddr *address, socklen_t *addrlen )
{
    StubLogger() << "STUB RECVFROM" << "socket:" << socket;

    std::unique_lock l_lock( g_mtx );

    while( !g_msg_q_map.contains( socket )
         || g_msg_q_map.at( socket ).empty() )
    {
        std::this_thread::sleep_for( std::chrono::milliseconds( 10 ) );
    }

    if( nullptr != address
     && nullptr != addrlen )
    {
        *addrlen = g_msg_q_map.at( socket ).front().address.length();
        memcpy( address, g_msg_q_map.at( socket ).front().address.data(), *addrlen );
    }

    int l_length = std::min( g_msg_q_map.at( socket ).front().data.length(), buflen );
    memcpy( buf, g_msg_q_map.at( socket ).front().data.data(), l_length );

    g_socket_activity.extract( socket );
    g_msg_q_map.at( socket ).pop();

    return l_length;
}

extern "C" __attribute__ ((visibility ("hidden")))
ssize_t read( int socket, void *buf, size_t buflen )
{
    StubLogger() << "STUB READ" << "socket:" << socket;

    return recvfrom( socket, buf, buflen, 0, nullptr, nullptr );
}

extern "C" __attribute__ ((visibility ("hidden")))
ssize_t write( int socket, const void *buf, size_t buflen )
{
    StubLogger() << "STUB WRITE" << "socket:" << socket;

    if( -1 == socket )
    {
        return -1;
    }

    std::string l_data;

    l_data.assign( reinterpret_cast<const char*>( buf ), buflen );

    std::unique_lock l_lock( g_mtx );

    g_msg_q_map[socket].push( { "", l_data } );
    g_socket_activity.insert( socket );

    return buflen;
}

extern "C" __attribute__ ((visibility ("hidden")))
int poll( struct pollfd *fds, nfds_t nfds, int )
{
    StubLogger() << "STUB POLL" << "number of nfds:" << nfds;

    int l_total = 0;

    while( 0 == l_total )
    {
        for( nfds_t i = 0; i < nfds; i++ )
        {
            std::shared_lock l_lock( g_mtx );

            if( g_socket_activity.contains( fds[i].fd ) )
            {
                fds[i].revents = POLLIN;
                l_total++;
            }
        }

        std::this_thread::sleep_for( std::chrono::milliseconds( 10 ) );
    }

    return l_total;
}

extern "C" __attribute__ ((visibility ("hidden")))
ssize_t __recvfrom_chk( int socket, void *buf, size_t buflen, size_t, int, struct sockaddr *address, socklen_t *addrlen )
{
    return recvfrom( socket, buf, buflen, 0, address, addrlen );
}

extern "C" __attribute__ ((visibility ("hidden")))
ssize_t __read_chk( int socket, void *buf, size_t buflen, size_t )
{
    return read( socket, buf, buflen );
}

extern "C" __attribute__ ((visibility ("hidden")))
int __poll_chk( struct pollfd *fds, nfds_t nfds, int, size_t )
{
    return poll( fds, nfds, 0 );
}
