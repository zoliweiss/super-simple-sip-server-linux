#include "test.h"

#include "common.h"
#include "logger.h"
#include "deparser.h"
#include "parser.h"
#include "cli.h"
#include "messaging.h"
#include "sip.h"
#include "timer.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <fcntl.h>

/***************************************/
/************** TESTCASES **************/
/***************************************/
static void test_parse_packet_request( void )
{
    packet_t                l_packet;
    std::vector<message_t>  l_messages;
    const char             *l_buf = "\
INVITE sip:bob@biloxi.example.com SIP/2.0\r\n\
Via:SIP/2.0/TCP client.atlanta.example.com:5060;branch=z9hG4bK74bf9\r\n\
Max-Forwards: 70\r\n\
From: Alice <sip:alice@atlanta.example.com:5060;transport=udp>;tag=9fxced76sl\r\n\
To: \"Uncle Bob\" <sip:bob@biloxi.example.com>\r\n\
Call-ID: 3848276298220188511@atlanta.example.com\r\n\
CSeq: 2 INVITE\r\n\
Contact: <sip:alice;user=phone@client.atlanta.example.com;transport=tcp>\r\n\
Diversion: Carol <sip:carol@atlanta.example.com>;privacy=off;reason=no-answer;counter=1;screen=no\r\n\
Remote-Party-ID: Alice <sip:alice@atlanta.example.com>\r\n\
Allow: PRACK,UPDATE\r\n\
Supported: 100rel\r\n\
Require: 100rel\r\n\
P-Asserted-Identity: Alice <sip:alice@atlanta.example.com>\r\n\
P-Charge-Info: <sip:eve@atlanta.example.com>\r\n\
P-Source-Device: 216.3.128.12\r\n\
Route: <sip:89.200.14.123:5060;lr;MPXON=Y>\r\n\
Record-Route: <sip:123.200.14.123:5060;lr;MPXON=Y>\r\n\
Content-Type: application/sdp\r\n\
Authorization: nonce=\"42\", uri=\"192.168.0.1\", nc=auth, cnonce=\"123456\", response=\"575f33fee220fc503ab0526f153513e8\", opaque=\"42\"\r\n\
Content-Length:   11\r\n\
\r\n\
I am an SDP\r\n\
\r\n\
INVITE sip:chris@ciloxi.example.com SIP/2.0\r\n\r\n\
";

    strcpy( l_packet.data, l_buf );
    l_packet.len = strlen( l_buf );

    parse_packet( l_packet, &l_messages );

    ASSERT( l_messages[0].method == sip_method_t::SIP_INVITE );
    ASSERT( l_messages[0].ruri.display_name == "" );
    ASSERT( l_messages[0].ruri.scheme == uri_scheme_t::SIP );
    ASSERT( l_messages[0].ruri.name == "bob" );
    ASSERT( l_messages[0].ruri.domain == "biloxi.example.com" );
    ASSERT( l_messages[0].ruri.port == 0 );
    ASSERT( l_messages[0].cseq.num == 2 );
    ASSERT( l_messages[0].cseq.method == sip_method_t::SIP_INVITE );
    ASSERT( l_messages[0].from.display_name == "Alice" );
    ASSERT( l_messages[0].from.scheme == uri_scheme_t::SIP );
    ASSERT( l_messages[0].from.name == "alice" );
    ASSERT( l_messages[0].from.domain == "atlanta.example.com" );
    ASSERT( l_messages[0].from.port == 5060 );
    ASSERT( l_messages[0].from.tag == "9fxced76sl" );
    ASSERT( l_messages[0].to.display_name == "Uncle Bob" );
    ASSERT( l_messages[0].to.scheme == uri_scheme_t::SIP );
    ASSERT( l_messages[0].to.name == "bob" );
    ASSERT( l_messages[0].to.domain == "biloxi.example.com" );
    ASSERT( l_messages[0].to.port == 0 );
    ASSERT( l_messages[0].to.tag == "" );
    ASSERT( l_messages[0].contact.display_name == "" );
    ASSERT( l_messages[0].contact.scheme == uri_scheme_t::SIP );
    ASSERT( l_messages[0].contact.name == "alice" );
    ASSERT( l_messages[0].contact.name == "alice" );
    ASSERT( l_messages[0].contact.domain == "client.atlanta.example.com" );
    ASSERT( l_messages[0].contact.port == 0 );
    ASSERT( l_messages[0].contact.tag == "" );
    ASSERT( l_messages[0].vias[0].full == "SIP/2.0/TCP client.atlanta.example.com:5060;branch=z9hG4bK74bf9" );
    ASSERT( l_messages[0].call_id == "3848276298220188511@atlanta.example.com" );
    ASSERT( l_messages[0].supported.rel100 );
    ASSERT( l_messages[0].require.rel100 );
    ASSERT( l_messages[0].allow.prack );
    ASSERT( l_messages[0].allow.update );
    ASSERT( l_messages[0].authorization.nonce == "42" );
    ASSERT( l_messages[0].authorization.uri == "192.168.0.1" );
    ASSERT( l_messages[0].authorization.nc == "auth" );
    ASSERT( l_messages[0].authorization.cnonce == "123456" );
    ASSERT( l_messages[0].authorization.response == "575f33fee220fc503ab0526f153513e8" );
    ASSERT( l_messages[0].authorization.opaque == "42" );
    ASSERT( l_messages[0].routes[0].full == "<sip:89.200.14.123:5060;lr;MPXON=Y>" );
    ASSERT( l_messages[0].record_routes[0].full == "<sip:123.200.14.123:5060;lr;MPXON=Y>" );
    ASSERT( l_messages[0].content.type == "application/sdp" );
    ASSERT( l_messages[0].expires == 900 );
    ASSERT( l_messages[0].content.len == 11 );
    ASSERT( l_messages[0].response_code == sip_status_t::SIP_STATUS_NOT_DEF );
    ASSERT( !strcmp( l_messages[0].content.payload, "I am an SDP" ) );
    ASSERT( l_messages[1].method == sip_method_t::SIP_INVITE );
    ASSERT( l_messages[1].ruri.display_name == "" );
    ASSERT( l_messages[1].ruri.scheme == uri_scheme_t::SIP );
    ASSERT( l_messages[1].ruri.name == "chris" );
    ASSERT( l_messages[1].ruri.domain == "ciloxi.example.com" );
    ASSERT( l_messages[1].ruri.port == 0 );
}

static void test_parse_uri_sip( void )
{
    const char             *l_buf = " \r\n\
From: \"Alice\" <sip:alice@atlanta.example.com>\r\n";
    std::vector<message_t>  l_messages;
    packet_t                l_packet;

    strcpy( l_packet.data, l_buf );
    l_packet.len = strlen( l_buf );

    parse_packet( l_packet, &l_messages );

    ASSERT( l_messages[0].from.display_name == "Alice" );
    ASSERT( l_messages[0].from.scheme == uri_scheme_t::SIP );
    ASSERT( l_messages[0].from.name == "alice" );
    ASSERT( l_messages[0].from.domain == "atlanta.example.com" );
    ASSERT( l_messages[0].from.port == 0 );
    ASSERT( l_messages[0].from.tag == "" );
}

static void test_parse_uri_sip_wo_userpart( void )
{
    const char             *l_buf = " \r\n\
From: <sip:atlanta.example.com>\r\n";
    std::vector<message_t>  l_messages;
    packet_t                l_packet;

    strcpy( l_packet.data, l_buf );
    l_packet.len = strlen( l_buf );

    parse_packet( l_packet, &l_messages );

    ASSERT( l_messages[0].from.display_name == "" );
    ASSERT( l_messages[0].from.scheme == uri_scheme_t::SIP );
    ASSERT( l_messages[0].from.name == "" );
    ASSERT( l_messages[0].from.domain == "atlanta.example.com" );
    ASSERT( l_messages[0].from.port == 0 );
    ASSERT( l_messages[0].from.tag == "" );
}

static void test_parse_uri_sip_only_uri( void )
{
    const char             *l_buf = " \r\n\
From: sip:jack@atlanta.example.com;tag=123456\r\n";
    std::vector<message_t>  l_messages;
    packet_t                l_packet;

    strcpy( l_packet.data, l_buf );
    l_packet.len = strlen( l_buf );

    parse_packet( l_packet, &l_messages );

    ASSERT( l_messages[0].from.display_name == "" );
    ASSERT( l_messages[0].from.scheme == uri_scheme_t::SIP );
    ASSERT( l_messages[0].from.name == "jack" );
    ASSERT( l_messages[0].from.domain == "atlanta.example.com" );
    ASSERT( l_messages[0].from.port == 0 );
    ASSERT( l_messages[0].from.tag == "123456" );
}

static void test_parse_uri_siptel( void )
{
    const char             *l_buf = " \r\n\
From   :\"Alice in wonderland\" <sip:123456789@atlanta.example.com:5060>\r\n";
    std::vector<message_t>  l_messages;
    packet_t                l_packet;

    strcpy( l_packet.data, l_buf );
    l_packet.len = strlen( l_buf );

    parse_packet( l_packet, &l_messages );

    ASSERT( l_messages[0].from.display_name == "Alice in wonderland" );
    ASSERT( l_messages[0].from.scheme == uri_scheme_t::SIP );
    ASSERT( l_messages[0].from.name == "123456789" );
    ASSERT( l_messages[0].from.domain == "atlanta.example.com" );
    ASSERT( l_messages[0].from.port == 5060 );
    ASSERT( l_messages[0].from.tag == "" );
}

static void test_parse_uri_tel( void )
{
    const char             *l_buf = " \r\n\
From:<tel:123456789>\r\n";
    std::vector<message_t>  l_messages;
    packet_t                l_packet;

    strcpy( l_packet.data, l_buf );
    l_packet.len = strlen( l_buf );

    parse_packet( l_packet, &l_messages );

    ASSERT( l_messages[0].from.display_name == "" );
    ASSERT( l_messages[0].from.scheme == uri_scheme_t::TEL );
    ASSERT( l_messages[0].from.name == "123456789" );
    ASSERT( l_messages[0].from.domain == "" );
    ASSERT( l_messages[0].from.port == 0 );
    ASSERT( l_messages[0].from.tag == "" );
}

static void test_parse_uri_invalid( void )
{
    const char             *l_buf = " \r\n\
From: <kiskutya:123456789>\r\n";
    std::vector<message_t>  l_messages;
    packet_t                l_packet;

    strcpy( l_packet.data, l_buf );
    l_packet.len = strlen( l_buf );

    parse_packet( l_packet, &l_messages );

    ASSERT( l_messages[0].from.display_name == "" );
    ASSERT( l_messages[0].from.scheme == uri_scheme_t::NOT_DEF );
    ASSERT( l_messages[0].from.name == "" );
    ASSERT( l_messages[0].from.domain == "" );
    ASSERT( l_messages[0].from.port == 0 );
    ASSERT( l_messages[0].from.tag == "" );
}

static void test_deparse_response( void )
{
    message_t   l_message;
    config_t    l_config;
    user_t      l_user;
    char        l_buf[BUFF_SIZE];
    via_t       l_via;
    const char *l_exp_buf = "\
SIP/2.0 401 Unauthorized\r\n\
Via: SIP/2.0/TCP client.atlanta.example.com:5060;branch=z9hG4bK74bf9\r\n\
From: <sip:a99999999999999999999999999999999999999999lice@127.0.0.1>;tag=9fxced76sl\r\n\
To: <sip:boooooooooooooooooooooooooooooooooob@127.0.0.1>;tag=123456789\r\n\
Call-ID: 3848276298220188511@atlanta.example.com\r\n\
CSeq: 2 INVITE\r\n\
WWW-Authenticate: Digest realm=\"127.0.0.1\", qop=\"auth\", algorithm=MD5, nonce=\"42\", opaque=\"42\"\r\n\
Content-Length: 0\r\n\r\n\
";

    memset( l_buf, 0, sizeof( l_buf ) );

    l_user.nonce = std::to_string( rand() );
    l_user.opaque = std::to_string( rand() );

    l_via.full = "SIP/2.0/TCP client.atlanta.example.com:5060;branch=z9hG4bK74bf9";
    l_message.vias.push_back( l_via );

    l_message.nw_data.protocol = protocol_t::TCP;
    l_message.cseq.num = 2;
    l_message.cseq.method = sip_method_t::SIP_INVITE;
    l_message.from.name = "a99999999999999999999999999999999999999999lice";
    l_message.to.name = "boooooooooooooooooooooooooooooooooob";
    l_message.from.tag = "9fxced76sl";
    l_message.to.tag = "123456789";
    l_message.call_id = "3848276298220188511@atlanta.example.com";
    l_message.content.type = "application/sdp";
    l_message.expires = 999;
    l_message.content.len = 13;
    strcpy( l_message.content.payload, "I am an SDP\r\n" );

    int l_len = deparse_response( l_message, l_config, &l_user, sip_status_t::SIP_UNAUTHORIZED, l_buf );

    ASSERT( l_len == 436 );
    ASSERT( !strcmp( l_buf, l_exp_buf ) );
}

static void test_deparse_relay_request( void )
{
    message_t     l_message;
    config_t      l_config;
    user_t        l_user;
    char          l_buf[BUFF_SIZE];
    via_t         l_via;
    const call_t  l_call;
    const char   *l_exp_buf = "\
INVITE sip:bob@127.0.0.1 SIP/2.0\r\n\
Via: SIP/2.0/UDP 127.0.0.1:5060;branch=z9hG4bK42\r\n\
Via: SIP/2.0/TCP client.atlanta.example.com:5060;branch=z9hG4bK74bf9\r\n\
From: Géza <sip:alice@127.0.0.1>;tag=9fxced76sl\r\n\
To: <sip:bob@127.0.0.1>\r\n\
Call-ID: 3848276298220188511@atlanta.example.com\r\n\
Max-Forwards: 70\r\n\
CSeq: 2 INVITE\r\n\
Contact: <sip:alice@127.0.0.1:5060>\r\n\
Supported: 100rel\r\n\
Require: 100rel\r\n\
Allow: INVITE, ACK, BYE, CANCEL, PRACK\r\n\
Record-Route: <sip:127.0.0.1:5060;transport=UDP;lr>\r\n\
Content-Type: application/sdp\r\n\
Content-Length: 13\r\n\
\r\n\
I am an SDP\r\n\
";

    memset( l_buf, 0, sizeof( l_buf ) );

    l_user.display_name = "Géza";

    l_via.full = "SIP/2.0/TCP client.atlanta.example.com:5060;branch=z9hG4bK74bf9";
    l_message.vias.push_back( l_via );

    l_message.method = sip_method_t::SIP_INVITE;
    l_message.cseq.num = 2;
    l_message.cseq.method = sip_method_t::SIP_INVITE;
    l_message.ruri.name = "bob";
    l_message.from.name = "alice";
    l_message.to.name = "bob";
    l_message.from.tag = "9fxced76sl";
    l_message.to.tag = "";
    l_message.contact.name = "alice";
    l_message.call_id = "3848276298220188511@atlanta.example.com";
    l_message.require.rel100 = true;
    l_message.supported.rel100 = true;
    l_message.allow.prack = true;
    l_message.authorization.nonce = "";
    l_message.authorization.uri = "";
    l_message.authorization.nc = "";
    l_message.authorization.cnonce = "";
    l_message.authorization.response = "";
    l_message.authorization.opaque = "";
    l_message.content.type = "application/sdp";
    l_message.expires = 0;
    l_message.content.len = 13;
    l_message.max_forwards = 71;
    l_message.response_code = sip_status_t::SIP_STATUS_NOT_DEF;
    strcpy( l_message.content.payload, "I am an SDP\r\n" );

    int l_len = deparse_relay( l_message, l_config, &l_user, protocol_t::UDP, l_call, l_buf );

    ASSERT( l_len == 545 );
    ASSERT( !strcmp( l_buf, l_exp_buf ) );
}

static void test_deparse_relay_response( void )
{
    message_t     l_message;
    config_t      l_config;
    char          l_buf[BUFF_SIZE];
    via_t         l_via;
    const call_t  l_call;
    const char   *l_exp_buf = "\
SIP/2.0 180 Ringing\r\n\
Via: SIP/2.0/TCP 127.0.0.1:5060;branch=z9hG4bK74bf9ppppppppppppppppppppppppppp\r\n\
From: <sip:alicqqqqqqqqqqqqqqqqqqqqqqqqqqqqe@127.0.0.1>;tag=9fxced7tttttttttttttttttttttttttt6sl\r\n\
To: <sip:boqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqb@127.0.0.1>;tag=sssssssssssssssssssssssssssssssssssssssss\r\n\
Call-ID: 3848276298220188511@atlanta.example.com\r\n\
Max-Forwards: 69\r\n\
CSeq: 2 UPDATE\r\n\
Contact: <sip:123456@127.0.0.1:5060>\r\n\
Allow: INVITE, ACK, BYE, CANCEL, UPDATE\r\n\
Content-Type: application/sdp\r\n\
Content-Length: 13\r\n\
\r\n\
I am an SDP\r\n\
";

    memset( l_buf, 0, sizeof( l_buf ) );

    l_via.full = "SIP/2.0/TCP client.atlanta.example.com:5060;branch=z9hG4bK74bf9";
    l_message.vias.push_back( l_via );
    l_via.full = "SIP/2.0/TCP 127.0.0.1:5060;branch=z9hG4bK74bf9ppppppppppppppppppppppppppp";
    l_message.vias.push_back( l_via );

    l_message.method = sip_method_t::SIP_NOT_DEF;
    l_message.cseq.num = 2;
    l_message.cseq.method = sip_method_t::SIP_UPDATE;
    l_message.from.name = "alicqqqqqqqqqqqqqqqqqqqqqqqqqqqqe";
    l_message.to.name = "boqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqb";
    l_message.from.tag = "9fxced7tttttttttttttttttttttttttt6sl";
    l_message.to.tag = "sssssssssssssssssssssssssssssssssssssssss";
    l_message.contact.name = "123456";
    l_message.call_id = "3848276298220188511@atlanta.example.com";
    l_message.allow.update = true;
    l_message.content.type = "application/sdp";
    l_message.expires = 2000;
    l_message.content.len = 13;
    l_message.response_code = sip_status_t::SIP_RINGING;
    strcpy( l_message.content.payload, "I am an SDP\r\n" );

    int l_len = deparse_relay( l_message, l_config, nullptr, protocol_t::TCP, l_call, l_buf );

    ASSERT( l_len == 536 );
    ASSERT( !strcmp( l_buf, l_exp_buf ) );
}

static void test_user_printout( void )
{
    user_t      l_user;
    const char *l_exp_buf = "\
\n\
  Name ................ 5000\n\
  Auth name ........... 5000_auth\n\
  Display name ........ Garfield\n\
  Network ............. 0.0.0.0:0 TCP socket 6\n\
  Call ID ............. 1234567890\n\
  Password ............ 5000_pass\n\
  Registered .......... YES\n\
    Initreg time ...... 2022-12-26T06:24:08.000Z\n\
    Last rereg time ... -\n\
    Last dereg time ... -\n\
\n\
";

    l_user.name = "5000";
    l_user.auth_name = "5000_auth";
    l_user.display_name = "Garfield";
    l_user.nw_data.ip_data.sin_family = AF_INET;
    l_user.nw_data.socket = 6;
    l_user.nw_data.protocol = protocol_t::TCP;
    l_user.call_id = "1234567890";
    l_user.password = "5000_pass";
    l_user.registered = true;
    l_user.time_initial_reg.set( 1672035848 );

    TRACE( KMAG, "%s", l_user.str().c_str() );

    ASSERT( !strcmp( l_user.str().c_str(), l_exp_buf ) );
}

static void test_users_printout( void )
{
    user_t      l_user;
    users_t     l_users;
    const char *l_exp_buf = "\
+-------------------------------------------------------------------------------------------+\n\
|    Name    |  Auth name  |    IP address    |  Port  | Socket | Proto | Registered | Busy |\n\
+-------------------------------------------------------------------------------------------+\n\
|       5000 |   5000_auth |                - |      - |      6 |   TCP |        YES |  YES |\n\
+-------------------------------------------------------------------------------------------+\n\
| TOTAL OF 1 ENTRIES\n\
+-------------------------------------------------------------------------------------------+\n\
";

    l_user.name = "5000";
    l_user.auth_name = "5000_auth";
    l_user.display_name = "Garfield";
    l_user.nw_data.socket = 6;
    l_user.nw_data.protocol = protocol_t::TCP;
    l_user.call_id = "1234567890";
    l_user.password = "5000_pass";
    l_user.registered = true;
    l_user.time_initial_reg.set( 1672035848 );

    l_users.insert( {l_user.name, l_user} );

    TRACE( KMAG, "%s", l_users.str().c_str() );

    ASSERT( !strcmp( l_users.str().c_str(), l_exp_buf ) );
}

static void test_call_printout( void )
{
    via_t       l_via;
    call_t      l_call;
    const char *l_exp_buf = "\
\n\
  Call state .......... CANCEL PENDING\n\
  Call ID ............. call_id\n\
  A user .............. a_username\n\
  B user .............. b_username\n\
  INVITE Vias ......... 1st via\n\
                        2nd via\n\
  INVITE CSeq ......... 888 ACK\n\
  INVITE From tag ..... invite_from_tag\n\
  INVITE To tag ....... invite_to_tag\n\
  Last req From tag ... last_request_from_tag\n\
  Last req To tag ..... last_request_to_tag\n\
  Call started ........ 2022-12-26T06:24:08.456Z\n\
    Duration .......... 1h 6m 40s\n\
\n\
";

    l_call.state = call_state_t::CANCEL_PENDING;
    l_call.call_id = "call_id";
    l_call.invite_cseq.num = 888;
    l_call.invite_cseq.method = sip_method_t::SIP_ACK;
    l_call.invite_from_tag = "invite_from_tag";
    l_call.invite_to_tag = "invite_to_tag";
    l_call.a_username = "a_username";
    l_call.b_username = "b_username";
    l_call.last_request_from_tag = "last_request_from_tag";
    l_call.last_request_to_tag = "last_request_to_tag";
    l_call.time_call_started.set( 1672035848, 456000000 );

    l_via.full = "1st via";
    l_call.invite_vias.push_back( l_via );

    l_via.full = "2nd via";
    l_call.invite_vias.push_back( l_via );

    TRACE( KMAG, "%s", l_call.str().c_str() );

    ASSERT( !strcmp( l_call.str().c_str(), l_exp_buf ) );
}

static void test_calls_printout( void )
{
    call_t      l_call;
    calls_t     l_calls;
    const char *l_exp_buf = "\
+--------------------------------------------------------------------------------------------+\n\
|             Call ID             |   A user   |   B user   |   Call state   | Call duration |\n\
+--------------------------------------------------------------------------------------------+\n\
| call_id9999999999999999999999 $ | a_username | b_username | CANCEL PENDING |     1h 6m 40s |\n\
+--------------------------------------------------------------------------------------------+\n\
| TOTAL OF 1 ENTRIES\n\
+--------------------------------------------------------------------------------------------+\n\
";

    l_call.state = call_state_t::CANCEL_PENDING;
    l_call.call_id = "call_id999999999999999999999999999999";
    l_call.a_username = "a_username";
    l_call.b_username = "b_username";
    l_call.time_call_started.set( 1672035848 );

    l_calls.insert( {l_call.call_id, l_call} );

    TRACE( KMAG, "%s", l_calls.str().c_str() );

    ASSERT( !strcmp( l_calls.str().c_str(), l_exp_buf ) );
}

static void test_users_save_restore( void )
{
    users_t l_users_saved;
    users_t l_users_restored;
    user_t  l_user;

    l_user.name = "5000";
    l_user.auth_name = "5000_auth";
    l_user.display_name = "Garfield";
    l_user.nw_data.ip_data.sin_family = AF_INET;
    l_user.nw_data.socket = 6;
    l_user.nw_data.protocol = protocol_t::TCP;
    l_user.registered = true;
    l_user.time_initial_reg.set( 1672035848 );
    l_user.time_re_reg.set( 123456789 );

    l_users_saved.set_users_db_save_path( "test_file" );
    l_users_restored.set_users_db_save_path( "test_file" );

    l_users_saved.insert( {l_user.name, l_user} );
    l_users_saved.save();
    l_users_restored.restore();
    unlink( "test_file" );

    ASSERT( l_users_restored.size() == 1 );
    ASSERT( l_user.name == l_users_restored[l_user.name].name );
    ASSERT( l_user.auth_name == l_users_restored[l_user.name].auth_name );
    ASSERT( l_user.display_name == l_users_restored[l_user.name].display_name );
    ASSERT( 0 == memcmp( &l_user.nw_data, &l_users_restored[l_user.name].nw_data, sizeof( l_user.nw_data ) ) );
    ASSERT( !l_users_restored[l_user.name].registered );
    ASSERT( l_user.time_initial_reg == l_users_restored[l_user.name].time_initial_reg );
    ASSERT( l_user.time_re_reg == l_users_restored[l_user.name].time_re_reg );
}

static void test_calls_ser_deser( void )
{
    calls_t           l_calls_saved;
    calls_t           l_calls_restored;
    call_t            l_call;
    std::stringstream l_ss;

    l_call.state = call_state_t::ALERTING;
    l_call.call_id = "call_id";

    via_t l_via = { .full = "full" };
    l_call.invite_vias.push_back( l_via );

    cseq_t l_cseq = { .num = 6, .method = sip_method_t::SIP_INVITE };
    l_call.invite_cseq = l_cseq;

    l_call.invite_from_tag = "invite_from_tag";
    l_call.invite_to_tag = "invite_to_tag";
    l_call.a_username = "a_username";
    l_call.b_username = "b_username";
    l_call.last_request_from_tag = "last_request_from_tag";
    l_call.last_request_to_tag = "last_request_to_tag";
    l_call.time_call_started.set( 123456789 );

    l_calls_saved.insert( {l_call.call_id, l_call} );
    l_calls_saved.serialize( &l_ss );
    l_calls_restored.deserialize( &l_ss );

    ASSERT( l_calls_restored.size() == 1 );
    ASSERT( l_call.state == l_calls_restored[l_call.call_id].state );
    ASSERT( l_call.call_id == l_calls_restored[l_call.call_id].call_id );
    ASSERT( l_call.invite_vias[0].full == l_calls_restored[l_call.call_id].invite_vias[0].full );
    ASSERT( l_call.invite_cseq.str() == l_calls_restored[l_call.call_id].invite_cseq.str() );
    ASSERT( l_call.invite_from_tag == l_calls_restored[l_call.call_id].invite_from_tag );
    ASSERT( l_call.invite_to_tag == l_calls_restored[l_call.call_id].invite_to_tag );
    ASSERT( l_call.a_username == l_calls_restored[l_call.call_id].a_username );
    ASSERT( l_call.b_username == l_calls_restored[l_call.call_id].b_username );
    ASSERT( l_call.last_request_from_tag == l_calls_restored[l_call.call_id].last_request_from_tag );
    ASSERT( l_call.last_request_to_tag == l_calls_restored[l_call.call_id].last_request_to_tag );
    ASSERT( l_call.time_call_started == l_calls_restored[l_call.call_id].time_call_started );
}

static void test_calls_ser_deser_empty( void )
{
    calls_t           l_calls_saved;
    calls_t           l_calls_restored;
    std::stringstream l_ss;

    l_calls_saved.serialize( &l_ss );
    l_calls_restored.deserialize( &l_ss );

    ASSERT( l_calls_restored.size() == 0 );
}


static void test_cli_flatten( void )
{
    std::string l_in = "  a b       c \"   def  \"g h";
    std::string l_res = "abc\"   def  \"gh";

    std::string &l_out = flatten_str( l_in );

    ASSERT( l_out == l_res );
}

static void test_cli_nohelp( void )
{
    std::string l_cli = R"({
        show:{
            calls:SHOW_CALLS,
            users:SHOW_USERS,
            info:SHOW_INFO
        },
        drop:{
            calls:DELETE_CALLS
        },
        set:{
            loglevel:{
                error:SET_LOG_ERROR,
                warn:SET_LOG_WARN,
                info:SET_LOG_INFO,
                debug:SET_LOG_DEBUG,
                trace:SET_LOG_TRACE
            }
        },
        exit:EXIT
    })";

    std::string l_res = R"(ELEMS HELP:
ID:show ENDPOINT: CHILD:YES HELP:

---- CHILDREN OF show ----
ELEMS HELP:
ID:calls ENDPOINT:SHOW_CALLS CHILD:NO HELP:
ID:users ENDPOINT:SHOW_USERS CHILD:NO HELP:
ID:info ENDPOINT:SHOW_INFO CHILD:NO HELP:
---- END OF CHILDREN ----

ID:drop ENDPOINT: CHILD:YES HELP:

---- CHILDREN OF drop ----
ELEMS HELP:
ID:calls ENDPOINT:DELETE_CALLS CHILD:NO HELP:
---- END OF CHILDREN ----

ID:set ENDPOINT: CHILD:YES HELP:

---- CHILDREN OF set ----
ELEMS HELP:
ID:loglevel ENDPOINT: CHILD:YES HELP:

---- CHILDREN OF loglevel ----
ELEMS HELP:
ID:error ENDPOINT:SET_LOG_ERROR CHILD:NO HELP:
ID:warn ENDPOINT:SET_LOG_WARN CHILD:NO HELP:
ID:info ENDPOINT:SET_LOG_INFO CHILD:NO HELP:
ID:debug ENDPOINT:SET_LOG_DEBUG CHILD:NO HELP:
ID:trace ENDPOINT:SET_LOG_TRACE CHILD:NO HELP:
---- END OF CHILDREN ----

---- END OF CHILDREN ----

ID:exit ENDPOINT:EXIT CHILD:NO HELP:
)";

    elems_t l_elems( l_cli );

    ASSERT( l_elems.str() == l_res );
}

static void test_cli_ep_help( void )
{
    std::string l_cli = R"({
        show:{
            calls:[
                SHOW_CALLS,
                ?:"This is endpoint help"
            ],
            users:SHOW_USERS,
            info:SHOW_INFO
        },
        drop:{
            calls:DELETE_CALLS
        },
        set:{
            loglevel:{
                error:SET_LOG_ERROR,
                warn:SET_LOG_WARN,
                info:SET_LOG_INFO,
                debug:SET_LOG_DEBUG,
                trace:SET_LOG_TRACE
            }
        },
        exit:EXIT
    })";

    std::string l_res = R"(ELEMS HELP:
ID:show ENDPOINT: CHILD:YES HELP:

---- CHILDREN OF show ----
ELEMS HELP:
ID:calls ENDPOINT:SHOW_CALLS CHILD:NO HELP:This is endpoint help
ID:users ENDPOINT:SHOW_USERS CHILD:NO HELP:
---- END OF CHILDREN ----

ID:drop ENDPOINT: CHILD:YES HELP:

---- CHILDREN OF drop ----
ELEMS HELP:
ID:calls ENDPOINT:DELETE_CALLS CHILD:NO HELP:
---- END OF CHILDREN ----

ID:set ENDPOINT: CHILD:YES HELP:

---- CHILDREN OF set ----
ELEMS HELP:
ID:loglevel ENDPOINT: CHILD:YES HELP:

---- CHILDREN OF loglevel ----
ELEMS HELP:
ID:error ENDPOINT:SET_LOG_ERROR CHILD:NO HELP:
ID:warn ENDPOINT:SET_LOG_WARN CHILD:NO HELP:
ID:info ENDPOINT:SET_LOG_INFO CHILD:NO HELP:
ID:debug ENDPOINT:SET_LOG_DEBUG CHILD:NO HELP:
ID:trace ENDPOINT:SET_LOG_TRACE CHILD:NO HELP:
---- END OF CHILDREN ----

---- END OF CHILDREN ----

ID:exit ENDPOINT:EXIT CHILD:NO HELP:
)";

    elems_t l_elems( l_cli );

    ASSERT( l_elems.str() == l_res );
}

static void test_cli_child_help( void )
{
    std::string l_cli = R"({
        show:{
            calls:SHOW_CALLS,
            users:SHOW_USERS,
            info:SHOW_INFO
        },
        drop:{
            calls:DELETE_CALLS
        },
        set:{
            loglevel:{
                error:SET_LOG_ERROR,
                warn:SET_LOG_WARN,
                info:SET_LOG_INFO,
                debug:SET_LOG_DEBUG,
                trace:SET_LOG_TRACE,
                ?:"This is child help"
            }
        },
        exit:EXIT
    })";

    std::string l_res = R"(ELEMS HELP:
ID:show ENDPOINT: CHILD:YES HELP:

---- CHILDREN OF show ----
ELEMS HELP:
ID:calls ENDPOINT:SHOW_CALLS CHILD:NO HELP:
ID:users ENDPOINT:SHOW_USERS CHILD:NO HELP:
ID:info ENDPOINT:SHOW_INFO CHILD:NO HELP:
---- END OF CHILDREN ----

ID:drop ENDPOINT: CHILD:YES HELP:

---- CHILDREN OF drop ----
ELEMS HELP:
ID:calls ENDPOINT:DELETE_CALLS CHILD:NO HELP:
---- END OF CHILDREN ----

ID:set ENDPOINT: CHILD:YES HELP:

---- CHILDREN OF set ----
ELEMS HELP:
ID:loglevel ENDPOINT: CHILD:YES HELP:

---- CHILDREN OF loglevel ----
ELEMS HELP:This is child help
ID:error ENDPOINT:SET_LOG_ERROR CHILD:NO HELP:
ID:warn ENDPOINT:SET_LOG_WARN CHILD:NO HELP:
ID:info ENDPOINT:SET_LOG_INFO CHILD:NO HELP:
ID:debug ENDPOINT:SET_LOG_DEBUG CHILD:NO HELP:
ID:trace ENDPOINT:SET_LOG_TRACE CHILD:NO HELP:
---- END OF CHILDREN ----

---- END OF CHILDREN ----

ID:exit ENDPOINT:EXIT CHILD:NO HELP:
)";

    elems_t l_elems( l_cli );

    ASSERT( l_elems.str() == l_res );
}

static void test_cli_mix_help( void )
{
    std::string l_cli = R"({
        show:{
            calls:SHOW_CALLS,
            users:SHOW_USERS,
            info:[
                SHOW_INFO,
                ?:"This is endpoint help"
            ]
        },
        drop:{
            calls:DELETE_CALLS
        },
        set:{
            loglevel:{
                error:SET_LOG_ERROR,
                warn:SET_LOG_WARN,
                info:SET_LOG_INFO,
                debug:SET_LOG_DEBUG,
                trace:SET_LOG_TRACE
            },
            ?:"This is child help"
        },
        exit:[
            EXIT,
            ?:"This is SPARTA!!!!!!!!!!!!!!!"
        ]
    })";

    std::string l_res = R"(ELEMS HELP:
ID:show ENDPOINT: CHILD:YES HELP:

---- CHILDREN OF show ----
ELEMS HELP:
ID:calls ENDPOINT:SHOW_CALLS CHILD:NO HELP:
ID:users ENDPOINT:SHOW_USERS CHILD:NO HELP:
ID:info ENDPOINT:SHOW_INFO CHILD:NO HELP:This is endpoint help
---- END OF CHILDREN ----

ID:drop ENDPOINT: CHILD:YES HELP:

---- CHILDREN OF drop ----
ELEMS HELP:
ID:calls ENDPOINT:DELETE_CALLS CHILD:NO HELP:
---- END OF CHILDREN ----

ID:set ENDPOINT: CHILD:YES HELP:

---- CHILDREN OF set ----
ELEMS HELP:This is child help
ID:loglevel ENDPOINT: CHILD:YES HELP:

---- CHILDREN OF loglevel ----
ELEMS HELP:
ID:error ENDPOINT:SET_LOG_ERROR CHILD:NO HELP:
ID:warn ENDPOINT:SET_LOG_WARN CHILD:NO HELP:
ID:info ENDPOINT:SET_LOG_INFO CHILD:NO HELP:
ID:debug ENDPOINT:SET_LOG_DEBUG CHILD:NO HELP:
ID:trace ENDPOINT:SET_LOG_TRACE CHILD:NO HELP:
---- END OF CHILDREN ----

---- END OF CHILDREN ----

ID:exit ENDPOINT:EXIT CHILD:NO HELP:This is SPARTA!!!!!!!!!!!!!!!
)";

    elems_t l_elems( l_cli );

    ASSERT( l_elems.str() == l_res );
}

static void test_config_parse( void )
{
    config_t      l_config;
    std::ofstream l_config_file( "test_config_parse", std::ios::binary );
    std::string   l_config_str = R"(
# Having a config file is optional
# If not present, the server tries to start with the default values
# Hint: opening as powershell script gives nice syntax highlight

[GENERAL]
    # The interface on which the server communicates
    # Must be IPv4 capable
    # Default: lo
    interface = "eth0"

    # The port on which the server listens
    # Default: 5060
    port = 5060

    # List of enabled protocols for communication
    # Default: tcp,udp
    protocols = "tcp,udp"

    # If set, then only the configured users can register
    # Default: false
    force_auth = "false"

    # Defines the default loglevel
    # Possible options are: error, warn, info, debug, trace
    # Default: error
    loglevel = "warn"

    # Defines the location of the logs
    # Possible options are: stderr, stdout, path to a file
    # Default: stderr
    log_path = "stderr"

    # Defines the path and name of the CLI socket file
    # Default: /var/run/ssss.sock
    cli_sock_path = "/var/run/ssss.sock"

    # Defines the path and name of the users DB save file
    # If the server crashes, it tries to restore the users from this file on start
    # If the path is left empty, the functionality is turned off
    # Default: /var/run/ssss_users.bin
    users_db_save_path = ""

    # Defines the path and name of the user event log
    # If the path is left empty, the functionality is turned off
    # Default: /var/log/ssss_user.log
    user_event_log_path = ""

    # Defines the path and name of the call event log
    # If the path is left empty, the functionality is turned off
    # Default: /var/log/ssss_call.log
    call_event_log_path = ""

    # Defines the maximum call duration in seconds
    # Default: 3600
    max_call_duration = 1800
[ENDGENERAL]
[USERS]
# These are the configured users
# If the force_auth general parameter is set, then only these users can register
# Default: empty list
    [USER]
        # Username is what appears in the R-URI and used as a key
        # Mandatory
        username = "5000"

        # The login for authentication
        # Mandatory
        auth = "5000_auth"

        # The password for authentication
        # Mandatory
        password = "5000_pass"

        # Specify the name, what you wish to display on the callee side
        # Default: empty string
        display_name = "Mekkelek"
    [ENDUSER]
#    [USER]
#        username = "6000"
#        auth = "6000_auth"
#        password = "6000_pass"
#        display_name = "Krtek"
#    [ENDUSER]
[ENDUSERS]
)";

    /*
     * Note that the loglevel is the actual loglevel
     * because config_t.str() returns the actual loglevel
     * not the one present in the config file.
     */
    std::string l_config_out_str_chk =
R"(  Server port ................. 5060
  Enabled protocols ........... UDP, TCP
  Network interface ........... eth0
  Force authorization ......... NO
  Loglevel .................... TRACE
  Log path .................... stderr
  CLI socket path ............. /var/run/ssss.sock
  Users DB save path .......... -
  User event log path ......... -
  Call event log path ......... -
  Maximum call duration ....... 1800)";

    l_config_file << l_config_str;
    l_config_file.close();

    ASSERT( l_config.parse( "test_config_parse" ) );

    std::string l_config_out_str = l_config.str();

    ASSERT( std::string::npos != l_config_out_str.find( l_config_out_str_chk ) );

    unlink( "test_config_parse" );
}

static void test_sip( void )
{
    REGISTER_MSG_THREAD( UDP );
    CLEAR_MSG_Q();

    /* set config */
    user_t   l_user;
    config_t l_config;

    l_user.name = "1000";
    l_user.auth_name = "1000";
    l_user.password = "1000_pass";

    l_config.users_db_save_path = "";
    l_config.user_event_log_path = "";
    l_config.call_event_log_path = "";
    l_config.configured_users.push_back( l_user );

    DECL_MSG_INPUT_DATA();
    std::thread l_sip_thread = std::thread( sip, &l_config );

    ASSERT( msg_type_t::ENABLE_INBOUND_TRAFFIC == MSG_INPUT_OTHER( UDP ) );

    /* register user 1000 */
    packet_t l_reg_1000_1;
    packet_t l_reg_1000_2;

    l_reg_1000_1.nw_data.protocol = protocol_t::UDP;
    l_reg_1000_1.len = snprintf( static_cast<char*>( l_reg_1000_1.data ), BUFF_SIZE,
"REGISTER sip:192.168.0.1 SIP/2.0\r\n\
Via: SIP/2.0/UDP 192.168.0.1:5060;branch=branch_1000\r\n\
From: <sip:1000@192.168.0.1>;tag=from_1000\r\n\
To: <sip:1000@192.168.0.1>\r\n\
Call-ID: callid_1000\r\n\
Max-Forwards: 70\r\n\
CSeq: 1 REGISTER\r\n\
Allow: INVITE, ACK, BYE, CANCEL\r\n\
Contact: <sip:1000@192.168.0.1:5060>\r\n\
Expires: 600\r\n\
Content-Length: 0\r\n\r\n" );

    l_reg_1000_2.nw_data.protocol = protocol_t::UDP;
    l_reg_1000_2.len = snprintf( static_cast<char*>( l_reg_1000_2.data ), BUFF_SIZE,
"REGISTER sip:192.168.0.1 SIP/2.0\r\n\
Via: SIP/2.0/UDP 192.168.0.1:5060;branch=branch_1000\r\n\
From: <sip:1000@192.168.0.1>;tag=from_1000\r\n\
To: <sip:1000@192.168.0.1>\r\n\
Call-ID: callid_1000\r\n\
Max-Forwards: 70\r\n\
CSeq: 1 REGISTER\r\n\
Allow: INVITE, ACK, BYE, CANCEL\r\n\
Contact: <sip:1000@192.168.0.1:5060>\r\n\
Authorization: nonce=\"42\", uri=\"192.168.0.1\", nc=auth, cnonce=\"123456\", response=\"575f33fee220fc503ab0526f153513e8\", opaque=\"42\"\r\n\
Expires: 600\r\n\
Content-Length: 0\r\n\r\n" );

    char l_1000_401[] =
"SIP/2.0 401 Unauthorized\r\n\
Via: SIP/2.0/UDP 192.168.0.1:5060;branch=branch_1000\r\n\
From: <sip:1000@127.0.0.1>;tag=from_1000\r\n\
To: <sip:1000@127.0.0.1>;tag=42\r\n\
Call-ID: callid_1000\r\n\
CSeq: 1 REGISTER\r\n\
WWW-Authenticate: Digest realm=\"127.0.0.1\", qop=\"auth\", algorithm=MD5, nonce=\"42\", opaque=\"42\"\r\n\
Content-Length: 0\r\n\r\n";

    char l_1000_200[] =
"SIP/2.0 200 OK\r\n\
Via: SIP/2.0/UDP 192.168.0.1:5060;branch=branch_1000\r\n\
From: <sip:1000@127.0.0.1>;tag=from_1000\r\n\
To: <sip:1000@127.0.0.1>;tag=42\r\n\
Call-ID: callid_1000\r\n\
CSeq: 1 REGISTER\r\n\
Contact: <sip:1000@127.0.0.1:5060>\r\n\
Expires: 900\r\n\
Content-Length: 0\r\n\r\n";

    MSG_OUTPUT( PACKET_RECEIVED, SIP, l_reg_1000_1 );
    ASSERT( msg_type_t::SEND_PACKET == MSG_INPUT_OTHER( UDP ) );
    ASSERT( 0 == memcmp( l_1000_401, reinterpret_cast<packet_t*>( MSG_INPUT_DATA )->data, strlen( l_1000_401 ) ) );
    ASSERT( static_cast<int>( strlen( l_1000_401 ) ) == reinterpret_cast<packet_t*>(MSG_INPUT_DATA)->len );

    MSG_OUTPUT( PACKET_RECEIVED, SIP, l_reg_1000_2 );
    ASSERT( msg_type_t::SEND_PACKET == MSG_INPUT_OTHER( UDP ) );
    ASSERT( 0 == memcmp( l_1000_200, reinterpret_cast<packet_t*>(MSG_INPUT_DATA)->data, strlen( l_1000_200 ) ) );
    ASSERT( static_cast<int>( strlen( l_1000_200 ) ) == reinterpret_cast<packet_t*>(MSG_INPUT_DATA)->len );

    ASSERT( msg_type_t::SET_TIMER_MSG == MSG_INPUT_OTHER( TMR ) );
    ASSERT( timer_msg_t::type_t::USER_DEREG == reinterpret_cast<timer_msg_t*>( MSG_INPUT_DATA )->type );

    /* register user 2000 */
    packet_t l_reg_2000;

    l_reg_2000.nw_data.protocol = protocol_t::UDP;
    l_reg_2000.len = snprintf( static_cast<char*>( l_reg_2000.data ), BUFF_SIZE,
"REGISTER sip:192.168.0.1 SIP/2.0\r\n\
Via: SIP/2.0/UDP 192.168.0.1:5060;branch=branch_2000\r\n\
From: <sip:2000@192.168.0.1>;tag=from_2000\r\n\
To: <sip:2000@192.168.0.1>\r\n\
Call-ID: callid_2000\r\n\
Max-Forwards: 70\r\n\
CSeq: 1 REGISTER\r\n\
Allow: INVITE, ACK, BYE, CANCEL\r\n\
Contact: <sip:2000@192.168.0.1:5060>\r\n\
Expires: 600\r\n\
Content-Length: 0\r\n\r\n" );

    char l_2000_200[] =
"SIP/2.0 200 OK\r\n\
Via: SIP/2.0/UDP 192.168.0.1:5060;branch=branch_2000\r\n\
From: <sip:2000@127.0.0.1>;tag=from_2000\r\n\
To: <sip:2000@127.0.0.1>;tag=42\r\n\
Call-ID: callid_2000\r\n\
CSeq: 1 REGISTER\r\n\
Contact: <sip:2000@127.0.0.1:5060>\r\n\
Expires: 900\r\n\
Content-Length: 0\r\n\r\n";

    MSG_OUTPUT( PACKET_RECEIVED, SIP, l_reg_2000 );
    ASSERT( msg_type_t::SEND_PACKET == MSG_INPUT_OTHER( UDP ) );
    ASSERT( 0 == memcmp( l_2000_200, reinterpret_cast<packet_t*>( MSG_INPUT_DATA )->data, strlen( l_2000_200 ) ) );
    ASSERT( static_cast<int>( strlen( l_2000_200 ) ) == reinterpret_cast<packet_t*>( MSG_INPUT_DATA )->len );

    ASSERT( msg_type_t::SET_TIMER_MSG == MSG_INPUT_OTHER( TMR ) );
    ASSERT( timer_msg_t::type_t::USER_DEREG == reinterpret_cast<timer_msg_t*>( MSG_INPUT_DATA )->type );

    /* validate users */
    MSG_OUTPUT_EMPTY( GET_USERS, SIP );
    ASSERT( msg_type_t::GET_USERS_RESP == MSG_INPUT_OTHER( CLI ) );

    std::stringstream l_ss;
    users_t l_users;
    l_ss.write( reinterpret_cast<gen_buffer_t*>( MSG_INPUT_DATA )->data, reinterpret_cast<gen_buffer_t*>( MSG_INPUT_DATA )->len );
    l_users.deserialize( &l_ss );
    ASSERT( 2 == l_users.size() );

    ASSERT( "1000" == l_users["1000"].name );
    ASSERT( "1000" == l_users["1000"].auth_name );
    ASSERT( "1000_pass" == l_users["1000"].password );
    ASSERT( l_users["1000"].registered );
    ASSERT( !l_users["1000"].time_re_reg.valid );
    ASSERT( !l_users["1000"].time_de_reg.valid );
    ASSERT( "" == l_users["1000"].call_id );

    ASSERT( "2000" == l_users["2000"].name );
    ASSERT( "" == l_users["2000"].auth_name );
    ASSERT( "" == l_users["2000"].password );
    ASSERT( l_users["2000"].registered );
    ASSERT( !l_users["2000"].time_re_reg.valid );
    ASSERT( !l_users["2000"].time_de_reg.valid );
    ASSERT( "" == l_users["2000"].call_id );

    /* call to non registered user */
    packet_t l_inv_to_non_reg;

    l_inv_to_non_reg.nw_data.protocol = protocol_t::UDP;
    l_inv_to_non_reg.len = snprintf( static_cast<char*>( l_inv_to_non_reg.data ), BUFF_SIZE,
"INVITE sip:3000@127.0.0.1 SIP/2.0\r\n\
Via: SIP/2.0/UDP 127.0.0.1:5060;branch=z9hG4bK42\r\n\
From: 1000 <sip:1000@127.0.0.1>;tag=9fxced76sl\r\n\
To: <sip:3000@127.0.0.1>\r\n\
Call-ID: 3848276298220188511@atlanta.example.com\r\n\
Max-Forwards: 70\r\n\
CSeq: 1 INVITE\r\n\
Contact: <sip:1000@127.0.0.1:5060>\r\n\
Supported: 100rel\r\n\
Require: 100rel\r\n\
Allow: INVITE, ACK, BYE, CANCEL, PRACK\r\n\
Record-Route: <sip:127.0.0.1:5060;transport=UDP;lr>\r\n\
Content-Type: application/sdp\r\n\
Content-Length: 0\r\n\r\n" );

    char l_inv_to_non_reg_404[] =
"SIP/2.0 404 Not Found\r\n\
Via: SIP/2.0/UDP 127.0.0.1:5060;branch=z9hG4bK42\r\n\
From: <sip:1000@127.0.0.1>;tag=9fxced76sl\r\n\
To: <sip:3000@127.0.0.1>;tag=42\r\n\
Call-ID: 3848276298220188511@atlanta.example.com\r\n\
CSeq: 1 INVITE\r\n\
Record-Route: <sip:127.0.0.1:5060;transport=UDP;lr>\r\n\
Content-Length: 0\r\n\r\n";

    MSG_OUTPUT( PACKET_RECEIVED, SIP, l_inv_to_non_reg );
    ASSERT( msg_type_t::SEND_PACKET == MSG_INPUT_OTHER( UDP ) );
    ASSERT( 0 == memcmp( l_inv_to_non_reg_404, reinterpret_cast<packet_t*>( MSG_INPUT_DATA )->data, strlen( l_inv_to_non_reg_404 ) ) );
    ASSERT( static_cast<int>( strlen( l_inv_to_non_reg_404 ) ) == reinterpret_cast<packet_t*>( MSG_INPUT_DATA )->len );

    /* call from not registered user */
    packet_t l_inv_from_non_reg;

    l_inv_from_non_reg.nw_data.protocol = protocol_t::UDP;
    l_inv_from_non_reg.len = snprintf( static_cast<char*>( l_inv_from_non_reg.data ), BUFF_SIZE,
"INVITE sip:1000@127.0.0.1 SIP/2.0\r\n\
Via: SIP/2.0/UDP 127.0.0.1:5060;branch=z9hG4bK42\r\n\
From: 3000 <sip:3000@127.0.0.1>;tag=9fxced76sl\r\n\
To: <sip:1000@127.0.0.1>\r\n\
Call-ID: 3848276298220188511@atlanta.example.com\r\n\
Max-Forwards: 70\r\n\
CSeq: 1 INVITE\r\n\
Contact: <sip:3000@127.0.0.1:5060>\r\n\
Supported: 100rel\r\n\
Require: 100rel\r\n\
Allow: INVITE, ACK, BYE, CANCEL, PRACK\r\n\
Record-Route: <sip:127.0.0.1:5060;transport=UDP;lr>\r\n\
Content-Type: application/sdp\r\n\
Content-Length: 0\r\n\r\n" );

    char l_inv_from_non_reg_403[] =
"SIP/2.0 403 Forbidden\r\n\
Via: SIP/2.0/UDP 127.0.0.1:5060;branch=z9hG4bK42\r\n\
From: <sip:3000@127.0.0.1>;tag=9fxced76sl\r\n\
To: <sip:1000@127.0.0.1>;tag=42\r\n\
Call-ID: 3848276298220188511@atlanta.example.com\r\n\
CSeq: 1 INVITE\r\n\
Record-Route: <sip:127.0.0.1:5060;transport=UDP;lr>\r\n\
Content-Length: 0\r\n\r\n";

    MSG_OUTPUT( PACKET_RECEIVED, SIP, l_inv_from_non_reg );
    ASSERT( msg_type_t::SEND_PACKET == MSG_INPUT_OTHER( UDP ) );
    ASSERT( 0 == memcmp( l_inv_from_non_reg_403, reinterpret_cast<packet_t*>( MSG_INPUT_DATA )->data, strlen( l_inv_from_non_reg_403 ) ) );
    ASSERT( static_cast<int>( strlen( l_inv_from_non_reg_403 ) ) == reinterpret_cast<packet_t*>( MSG_INPUT_DATA )->len );

    /* call 1000 -> 2000 */
    packet_t l_inv_1000_2000;

    l_inv_1000_2000.nw_data.protocol = protocol_t::UDP;
    l_inv_1000_2000.len = snprintf( static_cast<char*>( l_inv_1000_2000.data ), BUFF_SIZE,
"INVITE sip:2000@127.0.0.1 SIP/2.0\r\n\
Via: SIP/2.0/UDP 127.0.0.1:5060;branch=z9hG4bK42\r\n\
From: 1000 <sip:1000@127.0.0.1>;tag=9fxced76sl\r\n\
To: <sip:2000@127.0.0.1>\r\n\
Call-ID: 3848276298220188511@atlanta.example.com\r\n\
Max-Forwards: 70\r\n\
CSeq: 1 INVITE\r\n\
Contact: <sip:1000@127.0.0.1:5060>\r\n\
Supported: 100rel\r\n\
Require: 100rel\r\n\
Allow: INVITE, ACK, BYE, CANCEL, PRACK\r\n\
Record-Route: <sip:127.0.0.1:5060;transport=UDP;lr>\r\n\
Content-Type: application/sdp\r\n\
Content-Length: 0\r\n\r\n" );

    char l_inv_1000_2000_out[] =
"INVITE sip:2000@127.0.0.1 SIP/2.0\r\n\
Via: SIP/2.0/UDP 127.0.0.1:5060;branch=z9hG4bK42\r\n\
Via: SIP/2.0/UDP 127.0.0.1:5060;branch=z9hG4bK42\r\n\
From: <sip:1000@127.0.0.1>;tag=9fxced76sl\r\n\
To: <sip:2000@127.0.0.1>\r\n\
Call-ID: 3848276298220188511@atlanta.example.com\r\n\
Max-Forwards: 69\r\n\
CSeq: 1 INVITE\r\n\
Contact: <sip:1000@127.0.0.1:5060>\r\n\
Supported: 100rel\r\n\
Require: 100rel\r\n\
Allow: INVITE, ACK, BYE, CANCEL, PRACK\r\n\
Record-Route: <sip:127.0.0.1:5060;transport=UDP;lr>\r\n\
Record-Route: <sip:127.0.0.1:5060;transport=UDP;lr>\r\n\
Content-Length: 0\r\n\r\n";

    char l_inv_100[] =
"SIP/2.0 100 Trying\r\n\
Via: SIP/2.0/UDP 127.0.0.1:5060;branch=z9hG4bK42\r\n\
From: <sip:1000@127.0.0.1>;tag=9fxced76sl\r\n\
To: <sip:2000@127.0.0.1>\r\n\
Call-ID: 3848276298220188511@atlanta.example.com\r\n\
CSeq: 1 INVITE\r\n\
Record-Route: <sip:127.0.0.1:5060;transport=UDP;lr>\r\n\
Content-Length: 0\r\n\r\n";

    MSG_OUTPUT( PACKET_RECEIVED, SIP, l_inv_1000_2000 );
    ASSERT( msg_type_t::SEND_PACKET == MSG_INPUT_OTHER( UDP ) );
    ASSERT( 0 == memcmp( l_inv_100, reinterpret_cast<packet_t*>( MSG_INPUT_DATA )->data, strlen( l_inv_100 ) ) );
    ASSERT( static_cast<int>( strlen( l_inv_100 ) ) == reinterpret_cast<packet_t*>( MSG_INPUT_DATA )->len );
    ASSERT( msg_type_t::SEND_PACKET == MSG_INPUT_OTHER( UDP ) );
    ASSERT( 0 == memcmp( l_inv_1000_2000_out, reinterpret_cast<packet_t*>( MSG_INPUT_DATA )->data, strlen( l_inv_1000_2000_out ) ) );
    ASSERT( static_cast<int>( strlen( l_inv_1000_2000_out ) ) == reinterpret_cast<packet_t*>( MSG_INPUT_DATA )->len );

    packet_t l_inv_200;

    l_inv_200.nw_data.protocol = protocol_t::UDP;
    l_inv_200.len = snprintf( static_cast<char*>( l_inv_200.data ), BUFF_SIZE,
"SIP/2.0 200 OK\r\n\
Via: SIP/2.0/UDP 127.0.0.1:5060;branch=z9hG4bK42\r\n\
From: <sip:1000@127.0.0.1>;tag=9fxced76sl\r\n\
To: <sip:2000@127.0.0.1>;tag=42\r\n\
Call-ID: 3848276298220188511@atlanta.example.com\r\n\
CSeq: 1 INVITE\r\n\
Record-Route: <sip:127.0.0.1:5060;transport=UDP;lr>\r\n\
Content-Length: 0\r\n\r\n" );

    char l_inv_200_out[] =
"SIP/2.0 200 OK\r\n\
From: <sip:1000@127.0.0.1>;tag=9fxced76sl\r\n\
To: <sip:2000@127.0.0.1>;tag=42\r\n\
Call-ID: 3848276298220188511@atlanta.example.com\r\n\
Max-Forwards: 69\r\n\
CSeq: 1 INVITE\r\n\
Contact: <sip:127.0.0.1:5060>\r\n\
Allow: INVITE, ACK, BYE, CANCEL\r\n\
Record-Route: <sip:127.0.0.1:5060;transport=UDP;lr>\r\n\
Content-Length: 0\r\n\r\n";

    MSG_OUTPUT( PACKET_RECEIVED, SIP, l_inv_200 );
    ASSERT( msg_type_t::SEND_PACKET == MSG_INPUT_OTHER( UDP ) );
    ASSERT( 0 == memcmp( l_inv_200_out, reinterpret_cast<packet_t*>( MSG_INPUT_DATA )->data, strlen( l_inv_200_out ) ) );
    ASSERT( static_cast<int>( strlen( l_inv_200_out ) ) == reinterpret_cast<packet_t*>( MSG_INPUT_DATA )->len );

    packet_t l_ack;

    l_ack.nw_data.protocol = protocol_t::UDP;
    l_ack.len = snprintf( static_cast<char*>( l_ack.data ), BUFF_SIZE,
"ACK sip:2000@127.0.0.1 SIP/2.0\r\n\
Via: SIP/2.0/UDP 127.0.0.1:5060;branch=z9hG4bK42\r\n\
From: <sip:1000@127.0.0.1>;tag=9fxced76sl\r\n\
To: <sip:2000@127.0.0.1>;tag=42\r\n\
Call-ID: 3848276298220188511@atlanta.example.com\r\n\
CSeq: 2 ACK\r\n\
Record-Route: <sip:127.0.0.1:5060;transport=UDP;lr>\r\n\
Content-Length: 0\r\n\r\n" );

    char l_ack_out[] =
"ACK sip:2000@127.0.0.1 SIP/2.0\r\n\
Via: SIP/2.0/UDP 127.0.0.1:5060;branch=z9hG4bK42\r\n\
Via: SIP/2.0/UDP 127.0.0.1:5060;branch=z9hG4bK42\r\n\
From: <sip:1000@127.0.0.1>;tag=9fxced76sl\r\n\
To: <sip:2000@127.0.0.1>;tag=42\r\n\
Call-ID: 3848276298220188511@atlanta.example.com\r\n\
Max-Forwards: 69\r\n\
CSeq: 2 ACK\r\n\
Contact: <sip:127.0.0.1:5060>\r\n\
Allow: INVITE, ACK, BYE, CANCEL\r\n\
Record-Route: <sip:127.0.0.1:5060;transport=UDP;lr>\r\n\
Record-Route: <sip:127.0.0.1:5060;transport=UDP;lr>\r\n\
Content-Length: 0\r\n\r\n";

    MSG_OUTPUT( PACKET_RECEIVED, SIP, l_ack );
    ASSERT( msg_type_t::SEND_PACKET == MSG_INPUT_OTHER( UDP ) );
    ASSERT( 0 == memcmp( l_ack_out, reinterpret_cast<packet_t*>( MSG_INPUT_DATA )->data, strlen( l_ack_out ) ) );
    ASSERT( static_cast<int>( strlen( l_ack_out ) ) == reinterpret_cast<packet_t*>( MSG_INPUT_DATA )->len );

    ASSERT( msg_type_t::SET_TIMER_MSG == MSG_INPUT_OTHER( TMR ) );
    ASSERT( timer_msg_t::type_t::CALL_RELEASE == reinterpret_cast<timer_msg_t*>( MSG_INPUT_DATA )->type );

    packet_t l_bye;

    l_bye.nw_data.protocol = protocol_t::UDP;
    l_bye.len = snprintf( static_cast<char*>( l_bye.data ), BUFF_SIZE,
"BYE sip:2000@127.0.0.1 SIP/2.0\r\n\
Via: SIP/2.0/UDP 127.0.0.1:5060;branch=z9hG4bK42\r\n\
From: <sip:1000@127.0.0.1>;tag=9fxced76sl\r\n\
To: <sip:2000@127.0.0.1>;tag=42\r\n\
Call-ID: 3848276298220188511@atlanta.example.com\r\n\
CSeq: 3 BYE\r\n\
Record-Route: <sip:127.0.0.1:5060;transport=UDP;lr>\r\n\
Content-Length: 0\r\n\r\n" );

    char l_bye_200_out[] =
"SIP/2.0 200 OK\r\n\
Via: SIP/2.0/UDP 127.0.0.1:5060;branch=z9hG4bK42\r\n\
From: <sip:1000@127.0.0.1>;tag=9fxced76sl\r\n\
To: <sip:2000@127.0.0.1>;tag=42\r\n\
Call-ID: 3848276298220188511@atlanta.example.com\r\n\
CSeq: 3 BYE\r\n\
Record-Route: <sip:127.0.0.1:5060;transport=UDP;lr>\r\n\
Content-Length: 0\r\n\r\n";

    char l_bye_out[] =
"BYE sip:2000@127.0.0.1 SIP/2.0\r\n\
Via: SIP/2.0/UDP 127.0.0.1:5060;branch=z9hG4bK42\r\n\
Via: SIP/2.0/UDP 127.0.0.1:5060;branch=z9hG4bK42\r\n\
From: <sip:1000@127.0.0.1>;tag=9fxced76sl\r\n\
To: <sip:2000@127.0.0.1>;tag=42\r\n\
Call-ID: 3848276298220188511@atlanta.example.com\r\n\
Max-Forwards: 69\r\n\
CSeq: 3 BYE\r\n\
Contact: <sip:127.0.0.1:5060>\r\n\
Allow: INVITE, ACK, BYE, CANCEL\r\n\
Record-Route: <sip:127.0.0.1:5060;transport=UDP;lr>\r\n\
Record-Route: <sip:127.0.0.1:5060;transport=UDP;lr>\r\n\
Content-Length: 0\r\n\r\n";

    MSG_OUTPUT( PACKET_RECEIVED, SIP, l_bye );
    ASSERT( msg_type_t::SEND_PACKET == MSG_INPUT_OTHER( UDP ) );
    ASSERT( 0 == memcmp( l_bye_200_out, reinterpret_cast<packet_t*>( MSG_INPUT_DATA )->data, strlen( l_bye_200_out ) ) );
    ASSERT( static_cast<int>( strlen( l_bye_200_out ) ) == reinterpret_cast<packet_t*>( MSG_INPUT_DATA )->len );
    ASSERT( msg_type_t::SEND_PACKET == MSG_INPUT_OTHER( UDP ) );
    ASSERT( 0 == memcmp( l_bye_out, reinterpret_cast<packet_t*>( MSG_INPUT_DATA )->data, strlen( l_bye_out ) ) );
    ASSERT( static_cast<int>( strlen( l_bye_out ) ) == reinterpret_cast<packet_t*>( MSG_INPUT_DATA )->len );

    ASSERT( msg_type_t::RESET_TIMER_MSG == MSG_INPUT_OTHER( TMR ) );
    ASSERT( timer_msg_t::type_t::CALL_RELEASE == reinterpret_cast<timer_msg_t*>( MSG_INPUT_DATA )->type );

    packet_t l_bye_200;

    l_bye_200.nw_data.protocol = protocol_t::UDP;
    l_bye_200.len = snprintf( static_cast<char*>( l_bye_200.data ), BUFF_SIZE,
"SIP/2.0 200 OK\r\n\
Via: SIP/2.0/UDP 127.0.0.1:5060;branch=z9hG4bK42\r\n\
From: <sip:1000@127.0.0.1>;tag=9fxced76sl\r\n\
To: <sip:2000@127.0.0.1>;tag=42\r\n\
Call-ID: 3848276298220188511@atlanta.example.com\r\n\
CSeq: 3 BYE\r\n\
Record-Route: <sip:127.0.0.1:5060;transport=UDP;lr>\r\n\
Content-Length: 0\r\n\r\n" );

    MSG_OUTPUT( PACKET_RECEIVED, SIP, l_bye_200 );

    /* kill SIP */
    MSG_OUTPUT_EMPTY( DIE, SIP );
    ASSERT( msg_type_t::DISABLE_INBOUND_TRAFFIC == MSG_INPUT_OTHER( UDP ) );
    ASSERT( msg_type_t::DIE == MSG_INPUT_OTHER( UDP ) );

    l_sip_thread.join();
}

static void test_sip_die_call_terminated( void )
{
    REGISTER_MSG_THREAD( UDP );
    CLEAR_MSG_Q();

    /* set config */
    config_t l_config;

    l_config.users_db_save_path = "";
    l_config.user_event_log_path = "";
    l_config.call_event_log_path = "";

    DECL_MSG_INPUT_DATA();
    std::thread l_sip_thread = std::thread( sip, &l_config );

    ASSERT( msg_type_t::ENABLE_INBOUND_TRAFFIC == MSG_INPUT_OTHER( UDP ) );

    /* register user 1000 */
    packet_t l_reg_1000;

    l_reg_1000.nw_data.protocol = protocol_t::UDP;
    l_reg_1000.len = snprintf( static_cast<char*>( l_reg_1000.data ), BUFF_SIZE,
"REGISTER sip:192.168.0.1 SIP/2.0\r\n\
Via: SIP/2.0/UDP 192.168.0.1:5060;branch=branch_1000\r\n\
From: <sip:1000@192.168.0.1>;tag=from_1000\r\n\
To: <sip:1000@192.168.0.1>\r\n\
Call-ID: callid_1000\r\n\
Max-Forwards: 70\r\n\
CSeq: 1 REGISTER\r\n\
Allow: INVITE, ACK, BYE, CANCEL\r\n\
Contact: <sip:1000@192.168.0.1:5060>\r\n\
Expires: 600\r\n\
Content-Length: 0\r\n\r\n" );

    char l_1000_200[] =
"SIP/2.0 200 OK\r\n\
Via: SIP/2.0/UDP 192.168.0.1:5060;branch=branch_1000\r\n\
From: <sip:1000@127.0.0.1>;tag=from_1000\r\n\
To: <sip:1000@127.0.0.1>;tag=42\r\n\
Call-ID: callid_1000\r\n\
CSeq: 1 REGISTER\r\n\
Contact: <sip:1000@127.0.0.1:5060>\r\n\
Expires: 900\r\n\
Content-Length: 0\r\n\r\n";

    MSG_OUTPUT( PACKET_RECEIVED, SIP, l_reg_1000 );
    ASSERT( msg_type_t::SEND_PACKET == MSG_INPUT_OTHER( UDP ) );
    ASSERT( 0 == memcmp( l_1000_200, reinterpret_cast<packet_t*>(MSG_INPUT_DATA)->data, strlen( l_1000_200 ) ) );
    ASSERT( static_cast<int>( strlen( l_1000_200 ) ) == reinterpret_cast<packet_t*>(MSG_INPUT_DATA)->len );

    /* register user 2000 */
    packet_t l_reg_2000;

    l_reg_2000.nw_data.protocol = protocol_t::UDP;
    l_reg_2000.len = snprintf( static_cast<char*>( l_reg_2000.data ), BUFF_SIZE,
"REGISTER sip:192.168.0.1 SIP/2.0\r\n\
Via: SIP/2.0/UDP 192.168.0.1:5060;branch=branch_2000\r\n\
From: <sip:2000@192.168.0.1>;tag=from_2000\r\n\
To: <sip:2000@192.168.0.1>\r\n\
Call-ID: callid_2000\r\n\
Max-Forwards: 70\r\n\
CSeq: 1 REGISTER\r\n\
Allow: INVITE, ACK, BYE, CANCEL\r\n\
Contact: <sip:2000@192.168.0.1:5060>\r\n\
Expires: 600\r\n\
Content-Length: 0\r\n\r\n" );

    char l_2000_200[] =
"SIP/2.0 200 OK\r\n\
Via: SIP/2.0/UDP 192.168.0.1:5060;branch=branch_2000\r\n\
From: <sip:2000@127.0.0.1>;tag=from_2000\r\n\
To: <sip:2000@127.0.0.1>;tag=42\r\n\
Call-ID: callid_2000\r\n\
CSeq: 1 REGISTER\r\n\
Contact: <sip:2000@127.0.0.1:5060>\r\n\
Expires: 900\r\n\
Content-Length: 0\r\n\r\n";

    MSG_OUTPUT( PACKET_RECEIVED, SIP, l_reg_2000 );
    ASSERT( msg_type_t::SEND_PACKET == MSG_INPUT_OTHER( UDP ) );
    ASSERT( 0 == memcmp( l_2000_200, reinterpret_cast<packet_t*>( MSG_INPUT_DATA )->data, strlen( l_2000_200 ) ) );
    ASSERT( static_cast<int>( strlen( l_2000_200 ) ) == reinterpret_cast<packet_t*>( MSG_INPUT_DATA )->len );

    /* call 1000 -> 2000 */
    packet_t l_inv_1000_2000;

    l_inv_1000_2000.nw_data.protocol = protocol_t::UDP;
    l_inv_1000_2000.len = snprintf( static_cast<char*>( l_inv_1000_2000.data ), BUFF_SIZE,
"INVITE sip:2000@127.0.0.1 SIP/2.0\r\n\
Via: SIP/2.0/UDP 127.0.0.1:5060;branch=z9hG4bK42\r\n\
From: 1000 <sip:1000@127.0.0.1>;tag=9fxced76sl\r\n\
To: <sip:2000@127.0.0.1>\r\n\
Call-ID: 3848276298220188511@atlanta.example.com\r\n\
Max-Forwards: 70\r\n\
CSeq: 1 INVITE\r\n\
Contact: <sip:1000@127.0.0.1:5060>\r\n\
Supported: 100rel\r\n\
Require: 100rel\r\n\
Allow: INVITE, ACK, BYE, CANCEL, PRACK\r\n\
Record-Route: <sip:127.0.0.1:5060;transport=UDP;lr>\r\n\
Content-Type: application/sdp\r\n\
Content-Length: 0\r\n\r\n" );

    char l_inv_1000_2000_out[] =
"INVITE sip:2000@127.0.0.1 SIP/2.0\r\n\
Via: SIP/2.0/UDP 127.0.0.1:5060;branch=z9hG4bK42\r\n\
Via: SIP/2.0/UDP 127.0.0.1:5060;branch=z9hG4bK42\r\n\
From: <sip:1000@127.0.0.1>;tag=9fxced76sl\r\n\
To: <sip:2000@127.0.0.1>\r\n\
Call-ID: 3848276298220188511@atlanta.example.com\r\n\
Max-Forwards: 69\r\n\
CSeq: 1 INVITE\r\n\
Contact: <sip:1000@127.0.0.1:5060>\r\n\
Supported: 100rel\r\n\
Require: 100rel\r\n\
Allow: INVITE, ACK, BYE, CANCEL, PRACK\r\n\
Record-Route: <sip:127.0.0.1:5060;transport=UDP;lr>\r\n\
Record-Route: <sip:127.0.0.1:5060;transport=UDP;lr>\r\n\
Content-Length: 0\r\n\r\n";

    char l_inv_100[] =
"SIP/2.0 100 Trying\r\n\
Via: SIP/2.0/UDP 127.0.0.1:5060;branch=z9hG4bK42\r\n\
From: <sip:1000@127.0.0.1>;tag=9fxced76sl\r\n\
To: <sip:2000@127.0.0.1>\r\n\
Call-ID: 3848276298220188511@atlanta.example.com\r\n\
CSeq: 1 INVITE\r\n\
Record-Route: <sip:127.0.0.1:5060;transport=UDP;lr>\r\n\
Content-Length: 0\r\n\r\n";

    MSG_OUTPUT( PACKET_RECEIVED, SIP, l_inv_1000_2000 );
    ASSERT( msg_type_t::SEND_PACKET == MSG_INPUT_OTHER( UDP ) );
    ASSERT( 0 == memcmp( l_inv_100, reinterpret_cast<packet_t*>( MSG_INPUT_DATA )->data, strlen( l_inv_100 ) ) );
    ASSERT( static_cast<int>( strlen( l_inv_100 ) ) == reinterpret_cast<packet_t*>( MSG_INPUT_DATA )->len );
    ASSERT( msg_type_t::SEND_PACKET == MSG_INPUT_OTHER( UDP ) );
    ASSERT( 0 == memcmp( l_inv_1000_2000_out, reinterpret_cast<packet_t*>( MSG_INPUT_DATA )->data, strlen( l_inv_1000_2000_out ) ) );
    ASSERT( static_cast<int>( strlen( l_inv_1000_2000_out ) ) == reinterpret_cast<packet_t*>( MSG_INPUT_DATA )->len );

    packet_t l_inv_180;

    l_inv_180.nw_data.protocol = protocol_t::UDP;
    l_inv_180.len = snprintf( static_cast<char*>( l_inv_180.data ), BUFF_SIZE,
"SIP/2.0 180 Ringing\r\n\
Via: SIP/2.0/UDP 127.0.0.1:5060;branch=z9hG4bK42\r\n\
From: <sip:1000@127.0.0.1>;tag=9fxced76sl\r\n\
To: <sip:2000@127.0.0.1>;tag=42\r\n\
Call-ID: 3848276298220188511@atlanta.example.com\r\n\
CSeq: 1 INVITE\r\n\
Record-Route: <sip:127.0.0.1:5060;transport=UDP;lr>\r\n\
Content-Length: 0\r\n\r\n" );

    char l_inv_200_out[] =
"SIP/2.0 180 Ringing\r\n\
From: <sip:1000@127.0.0.1>;tag=9fxced76sl\r\n\
To: <sip:2000@127.0.0.1>;tag=42\r\n\
Call-ID: 3848276298220188511@atlanta.example.com\r\n\
Max-Forwards: 69\r\n\
CSeq: 1 INVITE\r\n\
Contact: <sip:127.0.0.1:5060>\r\n\
Allow: INVITE, ACK, BYE, CANCEL\r\n\
Record-Route: <sip:127.0.0.1:5060;transport=UDP;lr>\r\n\
Content-Length: 0\r\n\r\n";

    MSG_OUTPUT( PACKET_RECEIVED, SIP, l_inv_180 );
    ASSERT( msg_type_t::SEND_PACKET == MSG_INPUT_OTHER( UDP ) );
    ASSERT( 0 == memcmp( l_inv_200_out, reinterpret_cast<packet_t*>( MSG_INPUT_DATA )->data, strlen( l_inv_200_out ) ) );
    ASSERT( static_cast<int>( strlen( l_inv_200_out ) ) == reinterpret_cast<packet_t*>( MSG_INPUT_DATA )->len );

    /* kill SIP */
    MSG_OUTPUT_EMPTY( DIE, SIP );
    ASSERT( msg_type_t::DISABLE_INBOUND_TRAFFIC == MSG_INPUT_OTHER( UDP ) );

    char l_cancel_out[] =
"CANCEL sip:2000@127.0.0.1 SIP/2.0\r\n\
Via: SIP/2.0/UDP 127.0.0.1:5060;branch=z9hG4bK42\r\n\
From: <sip:1000@127.0.0.1>;tag=9fxced76sl\r\n\
To: <sip:2000@127.0.0.1>\r\n\
Call-ID: 3848276298220188511@atlanta.example.com\r\n\
Max-Forwards: 69\r\n\
CSeq: 10001 CANCEL\r\n\
Contact: <sip:1000@127.0.0.1:5060>\r\n\
Allow: INVITE, ACK, BYE, CANCEL\r\n\
Record-Route: <sip:127.0.0.1:5060;transport=UDP;lr>\r\n\
Content-Length: 0\r\n\r\n";

    char l_inv_487_out[] =
"SIP/2.0 487 Request Terminated\r\n\
Via: SIP/2.0/UDP 127.0.0.1:5060;branch=z9hG4bK42\r\n\
From: <sip:1000@127.0.0.1>;tag=9fxced76sl\r\n\
To: <sip:2000@127.0.0.1>;tag=42\r\n\
Call-ID: 3848276298220188511@atlanta.example.com\r\n\
CSeq: 1 INVITE\r\n\
Content-Length: 0\r\n\r\n";

    ASSERT( msg_type_t::SEND_PACKET == MSG_INPUT_OTHER( UDP ) );
    ASSERT( 0 == memcmp( l_cancel_out, reinterpret_cast<packet_t*>( MSG_INPUT_DATA )->data, strlen( l_cancel_out ) ) );
    ASSERT( static_cast<int>( strlen( l_cancel_out ) ) == reinterpret_cast<packet_t*>( MSG_INPUT_DATA )->len );
    ASSERT( msg_type_t::SEND_PACKET == MSG_INPUT_OTHER( UDP ) );
    ASSERT( 0 == memcmp( l_inv_487_out, reinterpret_cast<packet_t*>( MSG_INPUT_DATA )->data, strlen( l_inv_487_out ) ) );
    ASSERT( static_cast<int>( strlen( l_inv_487_out ) ) == reinterpret_cast<packet_t*>( MSG_INPUT_DATA )->len );

    ASSERT( msg_type_t::DIE == MSG_INPUT_OTHER( UDP ) );

    l_sip_thread.join();
}

static void test_msg_q( void )
{
    REGISTER_MSG_THREAD( SIP );
    DECL_MSG_INPUT_DATA();
    CLEAR_MSG_Q();

    MSG_OUTPUT( PACKET_RECEIVED, SIP, '1' );
    MSG_OUTPUT( SEND_PACKET, SIP, '2' );
    MSG_OUTPUT( GET_USERS, SIP, '3' );
    MSG_OUTPUT( GET_CALLS_RESP, SIP, '4' );
    MSG_OUTPUT( DIE, SIP, '5' );

    ASSERT( msg_type_t::PACKET_RECEIVED == MSG_INPUT() );
    ASSERT( '1' == *reinterpret_cast<char*>( MSG_INPUT_DATA ) );
    ASSERT( msg_type_t::SEND_PACKET == MSG_INPUT() );
    ASSERT( '2' == *reinterpret_cast<char*>( MSG_INPUT_DATA ) );
    ASSERT( msg_type_t::GET_USERS == MSG_INPUT() );
    ASSERT( '3' == *reinterpret_cast<char*>( MSG_INPUT_DATA ) );
    ASSERT( msg_type_t::GET_CALLS_RESP == MSG_INPUT() );
    ASSERT( '4' == *reinterpret_cast<char*>( MSG_INPUT_DATA ) );
    ASSERT( msg_type_t::DIE == MSG_INPUT() );
    ASSERT( '5' == *reinterpret_cast<char*>( MSG_INPUT_DATA ) );
}

static void test_msg_q_eventfd( void )
{
    REGISTER_MSG_THREAD( CLI );
    DECL_MSG_INPUT_DATA();
    CLEAR_MSG_Q();

    MSG_OUTPUT( PACKET_RECEIVED, CLI, '1' );
    MSG_OUTPUT( SEND_PACKET, CLI, '2' );
    MSG_OUTPUT( GET_USERS, CLI, '3' );
    MSG_OUTPUT( GET_CALLS_RESP, CLI, '4' );
    MSG_OUTPUT( DIE, CLI, '5' );

    ASSERT( msg_type_t::PACKET_RECEIVED == MSG_INPUT() );
    ASSERT( '1' == *reinterpret_cast<char*>( MSG_INPUT_DATA ) );
    ASSERT( msg_type_t::SEND_PACKET == MSG_INPUT() );
    ASSERT( '2' == *reinterpret_cast<char*>( MSG_INPUT_DATA ) );
    ASSERT( msg_type_t::GET_USERS == MSG_INPUT() );
    ASSERT( '3' == *reinterpret_cast<char*>( MSG_INPUT_DATA ) );
    ASSERT( msg_type_t::GET_CALLS_RESP == MSG_INPUT() );
    ASSERT( '4' == *reinterpret_cast<char*>( MSG_INPUT_DATA ) );
    ASSERT( msg_type_t::DIE == MSG_INPUT() );
    ASSERT( '5' == *reinterpret_cast<char*>( MSG_INPUT_DATA ) );
}

static void test_cli_main( void )
{
    REGISTER_MSG_THREAD( APP );
    DECL_MSG_INPUT_DATA();
    CLEAR_MSG_Q();

    /* set config */
    config_t l_config;

    l_config.cli_sock_path = "/tmp/cli.sock";

    std::thread l_cli_thread = std::thread( cli, &l_config );

    /* wait until the cli thread starts */
    std::this_thread::sleep_for( std::chrono::milliseconds( 100 ) );

    /* set local socket */
    struct sockaddr_un test_addr = {};
    test_addr.sun_family = AF_UNIX;
    memcpy( test_addr.sun_path, "/tmp/test.sock", strlen( "/tmp/test.sock" ) );
    int l_sock = socket( PF_UNIX, SOCK_DGRAM, 0 );
    ASSERT( -1 != l_sock );
    ASSERT( -1 != bind( l_sock, reinterpret_cast<struct sockaddr*>( &test_addr ), sizeof( test_addr ) ) );

    /* set destination */
    struct sockaddr_un cli_addr = {};
    cli_addr.sun_family = AF_UNIX;
    memcpy( cli_addr.sun_path, "/tmp/cli.sock", strlen( "/tmp/cli.sock" ) );

    /* send data */
    char l_data[] = "asdf\n\x08\x08\x08\x08iddqd\n\x08\x08\x08\x08\x08\x1B[A\x1B[Bexit\n";
    ASSERT( 0 < sendto( l_sock, &l_data, sizeof( l_data ), 0, reinterpret_cast<struct sockaddr*>( &cli_addr ), sizeof( cli_addr ) ) );

    /* test */
    ASSERT( msg_type_t::GET_USERS == MSG_INPUT_OTHER( SIP ) );
    ASSERT( msg_type_t::GET_CALLS == MSG_INPUT_OTHER( SIP ) );
    ASSERT( msg_type_t::GET_TCP_NETWORK_DATA == MSG_INPUT_OTHER( TCP ) );
    ASSERT( msg_type_t::SET_TIMER_MSG == MSG_INPUT_OTHER( TMR ) );

    MSG_OUTPUT_EMPTY( DIE, CLI );
    MSG_OUTPUT_EMPTY( GET_USERS_RESP, CLI );
    MSG_OUTPUT_EMPTY( GET_CALLS_RESP, CLI );
    MSG_OUTPUT_EMPTY( GET_TCP_NETWORK_DATA_RESP, CLI );

    ASSERT( msg_type_t::EXIT_REQUESTED == MSG_INPUT_OTHER( APP ) );

    l_cli_thread.join();
    close( l_sock );
}

static void test_cli_autocomplete( void )
{
    REGISTER_MSG_THREAD( APP );
    DECL_MSG_INPUT_DATA();
    CLEAR_MSG_Q();

    /* set config */
    config_t l_config;

    l_config.cli_sock_path = "/tmp/cli.sock";

    std::thread l_cli_thread = std::thread( cli, &l_config );

    /* wait until the cli thread starts */
    std::this_thread::sleep_for( std::chrono::milliseconds( 100 ) );

    /* set local socket */
    struct sockaddr_un test_addr = {};
    test_addr.sun_family = AF_UNIX;
    memcpy( test_addr.sun_path, "/tmp/test.sock", strlen( "/tmp/test.sock" ) );
    int l_sock = socket( PF_UNIX, SOCK_DGRAM, 0 );
    ASSERT( -1 != l_sock );
    ASSERT( -1 != bind( l_sock, reinterpret_cast<struct sockaddr*>( &test_addr ), sizeof( test_addr ) ) );

    /* set destination */
    struct sockaddr_un cli_addr = {};
    cli_addr.sun_family = AF_UNIX;
    memcpy( cli_addr.sun_path, "/tmp/cli.sock", strlen( "/tmp/cli.sock" ) );

    /* send data */
    char l_data[] = "se\x09logl\x09tr\x09\n";
    ASSERT( 0 < sendto( l_sock, &l_data, sizeof( l_data ), 0, reinterpret_cast<struct sockaddr*>( &cli_addr ), sizeof( cli_addr ) ) );

    /* test */
    ASSERT( msg_type_t::GET_USERS == MSG_INPUT_OTHER( SIP ) );
    ASSERT( msg_type_t::GET_CALLS == MSG_INPUT_OTHER( SIP ) );
    ASSERT( msg_type_t::GET_TCP_NETWORK_DATA == MSG_INPUT_OTHER( TCP ) );
    ASSERT( msg_type_t::SET_TIMER_MSG == MSG_INPUT_OTHER( TMR ) );

    MSG_OUTPUT_EMPTY( GET_USERS_RESP, CLI );
    MSG_OUTPUT_EMPTY( GET_CALLS_RESP, CLI );
    MSG_OUTPUT_EMPTY( GET_TCP_NETWORK_DATA_RESP, CLI );

    MSG_OUTPUT_EMPTY( DIE, CLI );

    l_cli_thread.join();
    close( l_sock );
}

static void test_timer( void )
{
    REGISTER_MSG_THREAD( CLI );
    DECL_MSG_INPUT_DATA();
    CLEAR_MSG_Q();

    std::thread l_tmr_thread = std::thread( tmr );

    SET_TIMER_EMPTY( CLI_TIMEOUT, 1 );
    ASSERT( msg_type_t::TIMER_EXPIRED == MSG_INPUT() );
    ASSERT( timer_msg_t::type_t::CLI_TIMEOUT == reinterpret_cast<timer_msg_t*>( MSG_INPUT_DATA )->type );

    MSG_OUTPUT_EMPTY( DIE, TMR );
    l_tmr_thread.join();
}

static void test_timer_reset( void )
{
    REGISTER_MSG_THREAD( CLI );
    CLEAR_MSG_Q();

    std::thread l_tmr_thread = std::thread( tmr );

    RESET_TIMER_EMPTY( CLI_TIMEOUT );
    RESET_TIMER_EMPTY( CLI_TIMEOUT );
    RESET_TIMER_EMPTY( CLI_TIMEOUT );

    SET_TIMER_EMPTY( CLI_TIMEOUT, 1000 );

    RESET_TIMER_EMPTY( CLI_TIMEOUT );
    RESET_TIMER_EMPTY( CLI_TIMEOUT );
    RESET_TIMER_EMPTY( CLI_TIMEOUT );

    MSG_OUTPUT_EMPTY( DIE, TMR );
    l_tmr_thread.join();
}

static void test_base64_encode( void )
{
    ASSERT( "Cg==" == base64_encode( "\n", 1 ) );
    ASSERT( "Cgo=" == base64_encode( "\n\n", 2 ) );
    ASSERT( "CgoK" == base64_encode( "\n\n\n", 3 ) );
    ASSERT( "CgoKNA==" == base64_encode( "\n\n\n4", 4 ) );
    ASSERT( "CgoKNDU=" == base64_encode( "\n\n\n45", 5 ) );
    ASSERT( "CgoKNDU2" == base64_encode( "\n\n\n456", 6 ) );
}

/***************************************/
/***************** MAIN ****************/
/***************************************/
int main( void )
{
    Logger::set_loglevel( loglevel_t::TRACE );
    Logger::set_log_path( "testlog.txt" );

    if( !MsgBus::init() )
    {
        return EXIT_FAILURE;
    }

    TC( test_parse_packet_request );
    TC( test_parse_uri_sip );
    TC( test_parse_uri_sip_wo_userpart );
    TC( test_parse_uri_sip_only_uri );
    TC( test_parse_uri_siptel );
    TC( test_parse_uri_tel );
    TC( test_parse_uri_invalid );
    TC( test_deparse_response );
    TC( test_deparse_relay_request );
    TC( test_deparse_relay_response );
    TC( test_user_printout );
    TC( test_users_printout );
    TC( test_call_printout );
    TC( test_calls_printout );
    TC( test_users_save_restore );
    TC( test_calls_ser_deser );
    TC( test_calls_ser_deser_empty );
    TC( test_cli_flatten );
    TC( test_cli_nohelp );
    TC( test_cli_ep_help );
    TC( test_cli_child_help );
    TC( test_cli_mix_help );
    TC( test_config_parse );
    TC( test_sip );
    TC( test_sip_die_call_terminated );
    TC( test_msg_q );
    TC( test_msg_q_eventfd );
    TC( test_cli_main );
    TC( test_cli_autocomplete );
    TC( test_timer );
    TC( test_timer_reset );
    TC( test_base64_encode );

    SUMMARY();

    return g_status;
}
