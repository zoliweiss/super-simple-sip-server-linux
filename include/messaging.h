#pragma once

#include "logger.h"

#include <string.h>
#include <sys/eventfd.h>
#include <unistd.h>
#include <condition_variable>
#include <queue>
#include <thread>

/***************************************/
/**************** MACROS ***************/
/***************************************/
#define MSG_SIZE 16384

#define DECL_MSG_INPUT_DATA()                       msg_t __l_msg = {};
#define MSG_INPUT_DATA                              &__l_msg.msg
#define MSG_INPUT()                                 MsgBus::get_msg( msg_t::endpoint_t::SELF, 0, false, &__l_msg )
#define MSG_INPUT_W_WAIT( _duration_milli_ )        MsgBus::get_msg( msg_t::endpoint_t::SELF, _duration_milli_, true, &__l_msg )
#define MSG_OUTPUT( _msg_type_, _dest_, _what_ )    MsgBus::send_msg( msg_type_t::_msg_type_, msg_t::endpoint_t::SELF, msg_t::endpoint_t::_dest_, _what_ )
#define MSG_OUTPUT_EMPTY( _msg_type_, _dest_ )      MsgBus::send_msg( msg_type_t::_msg_type_, msg_t::endpoint_t::SELF, msg_t::endpoint_t::_dest_, '\0' )
#define MSG_SRC                                     __l_msg.msg_src
#define SAVE_MSG()                                  MsgBus::send_msg( __l_msg.msg_type, __l_msg.msg_src, msg_t::endpoint_t::SELF, __l_msg.msg )

/***************************************/
/******** DATA TYPE DEFINITIONS ********/
/***************************************/
enum class msg_type_t
{
    NOT_SET,

    PACKET_RECEIVED,            /* UDP, TCP -> SIP           */ /* packet_t              */ /* report the received packet from the network to SIP */
    SEND_PACKET,                /* SIP      -> UDP, TCP      */ /* packet_t              */ /* SIP sends request or response back to the network  */
    REPORT_CLOSED_CONNECTIONS,  /* TCP      -> SIP           */ /* network_t             */ /* TCP reports the closed TCP connection to SIP       */
    GET_USERS,                  /* CLI      -> SIP           */ /* EMPTY                 */ /* CLI asking for users_t                             */
    GET_CALLS,                  /* CLI      -> SIP           */ /* EMPTY                 */ /* CLI asking for calls_t                             */
    GET_TCP_NETWORK_DATA,       /* CLI      -> TCP           */ /* EMPTY                 */ /* CLI asking for TCP network data                    */
    TERMINATE_CALL,             /* CLI, APP -> SIP           */ /* terminate_call_t      */ /* CLI asking for call termination                    */
    DELETE_USER,                /* CLI      -> SIP           */ /* delete_user_t         */ /* CLI asking for user deletion                       */
    GET_USERS_RESP,             /* SIP      -> CLI           */ /* gen_buffer_t          */ /* serialized users_t data                            */
    GET_CALLS_RESP,             /* SIP      -> CLI           */ /* gen_buffer_t          */ /* serialized calls_t data                            */
    GET_TCP_NETWORK_DATA_RESP,  /* TCP      -> CLI           */ /* cli_tcp_nw_data_t     */ /* TCP network data for CLI reporing                  */
    TERMINATE_CALL_RESP,        /* SIP      -> CLI           */ /* terminate_call_resp_t */ /* call termination result                            */
    DELETE_USER_RESP,           /* SIP      -> CLI           */ /* delete_user_resp_t    */ /* user deletion result                               */
    EXIT_REQUESTED,             /* SGN, CLI -> APP           */ /* EMPTY                 */ /* process exit request                               */
    ENABLE_INBOUND_TRAFFIC,     /* SIP      -> UDP, TCP      */ /* EMPTY                 */ /* enables the processing of inbound network packets  */
    DISABLE_INBOUND_TRAFFIC,    /* SIP      -> UDP, TCP      */ /* EMPTY                 */ /* disables the processing of inbound network packets */
    SET_TIMER_MSG,              /* CLI, SIP -> TMR           */ /* timer_msg_t           */ /* sets the timer                                     */
    RESET_TIMER_MSG,            /* CLI, SIP -> TMR           */ /* timer_msg_t           */ /* resets the timer                                   */
    TIMER_EXPIRED,              /* TMR      -> CLI, SIP      */ /* timer_msg_t           */ /* TMR returns the timer data                         */
    CONFIG_CHANGED,             /* CLI      -> SIP           */ /* EMPTY                 */ /* CLI signals that some configuration has changed    */
    RELOAD_CONF_USERS,          /* CLI      -> SIP           */ /* EMPTY                 */ /* CLI commands SIP to reload the users from config   */
    CLOSE_TCP_SOCKET,           /* SIP      -> TCP           */ /* int                   */ /* SIP signals that TCP client is deleted             */
    DIE,                        /* APP      -> SIP, CLI, TMR */ /* EMPTY                 */ /* thread termination request                         */
                                /* SIP      -> UDP, TCP      */
};

struct msg_t
{
    enum endpoint_t
    {
        SELF = -2,
        NOT_SET = -1,

        SIP = 0,
        UDP,
        TCP,
        CLI,
        SGN,
        TMR,
        APP,

        SIZE
    };

    int           msg_size = 0;
    msg_type_t    msg_type = msg_type_t::NOT_SET;
    endpoint_t    msg_src = endpoint_t::NOT_SET;
    endpoint_t    msg_dest = endpoint_t::NOT_SET;
    unsigned char msg[MSG_SIZE] = {};

    inline void init( void )
    {
        msg_size = 0;
        msg_type = msg_type_t::NOT_SET;
    }
};

class MsgBus
{
public:
    static bool init( void )
    {
        if( 0 > ( g_udp_eventfd = eventfd( 0, EFD_SEMAPHORE ) ) )
        {
            ERROR( "UDP eventfd error: %s", strerror( errno ) );
            return false;
        }

        if( 0 > ( g_tcp_eventfd = eventfd( 0, EFD_SEMAPHORE ) ) )
        {
            ERROR( "TCP eventfd error: %s", strerror( errno ) );
            return false;
        }

        if( 0 > ( g_cli_eventfd = eventfd( 0, EFD_SEMAPHORE ) ) )
        {
            ERROR( "CLI eventfd error: %s", strerror( errno ) );
            return false;
        }

        return true;
    }

    static void clear( void )
    {
        for( auto &i_q : g_q )
        {
            i_q = {};
        }
    }

    static void register_thread( const msg_t::endpoint_t self )
    {
        g_self = self;
    }

    template<typename T>
    static void send_msg( const msg_type_t msg_type, const msg_t::endpoint_t src, const msg_t::endpoint_t dest, const T &msg )
    {
        static_assert( sizeof( T ) <= MSG_SIZE, "Type doesn't fit into buffer!" );

        msg_t    l_msg = {};
        uint64_t l_data = 1;

        l_msg.msg_size = sizeof( msg );
        l_msg.msg_type = msg_type;
        memcpy( &l_msg.msg, &msg, sizeof( msg ) );

        if( msg_t::endpoint_t::SELF == src )
        {
            l_msg.msg_src = g_self;
        }
        else
        {
            l_msg.msg_src = src;
        }

        if( msg_t::endpoint_t::SELF == dest )
        {
            l_msg.msg_dest = g_self;
        }
        else
        {
            l_msg.msg_dest = dest;
        }

        std::unique_lock<std::mutex> l_lock( MsgBus::getMtx( l_msg.msg_dest ) );
        g_q[l_msg.msg_dest].push( l_msg );

        /* these threads are waiting for network socket data, so we must notify them via eventfd socket */
        if( msg_t::endpoint_t::UDP == l_msg.msg_dest )
        {
            if( sizeof( uint64_t ) != write( g_udp_eventfd, &l_data, sizeof( uint64_t ) ) )
            {
                WARN( "UDP eventfd write error: %s", strerror( errno ) );
            }
        }
        else if( msg_t::endpoint_t::TCP == l_msg.msg_dest )
        {
            if( sizeof( uint64_t ) != write( g_tcp_eventfd, &l_data, sizeof( uint64_t ) ) )
            {
                WARN( "TCP eventfd write error: %s", strerror( errno ) );
            }
        }
        else if( msg_t::endpoint_t::CLI == l_msg.msg_dest )
        {
            if( sizeof( uint64_t ) != write( g_cli_eventfd, &l_data, sizeof( uint64_t ) ) )
            {
                WARN( "CLI eventfd write error: %s", strerror( errno ) );
            }
        }

        g_cv[l_msg.msg_dest].notify_all();
    }

    static msg_type_t get_msg( const msg_t::endpoint_t dest, const int duration_milli, const bool timed, msg_t *msg )
    {
        uint64_t          l_data = 0;
        msg_t::endpoint_t l_dest = g_self;

        if( msg_t::endpoint_t::SELF != dest )
        {
            l_dest = dest;
        }

        msg->init();
        std::unique_lock<std::mutex> l_lock( MsgBus::getMtx( l_dest ) );

        if( g_q[l_dest].empty() )
        {
            if( timed )
            {
                g_cv[l_dest].wait_for( l_lock, std::chrono::milliseconds( duration_milli ) );
            }
            else
            {
                g_cv[l_dest].wait( l_lock );
            }
        }

        if( !g_q[l_dest].empty() )
        {
            *msg = g_q[l_dest].front();
            g_q[l_dest].pop();
        }

        if( msg_t::endpoint_t::UDP == l_dest )
        {
            if( sizeof( uint64_t ) != read( g_udp_eventfd, &l_data, sizeof( uint64_t ) ) )
            {
                WARN( "UDP eventfd read error: %s", strerror( errno ) );
            }
        }
        else if( msg_t::endpoint_t::TCP == l_dest )
        {
            if( sizeof( uint64_t ) != read( g_tcp_eventfd, &l_data, sizeof( uint64_t ) ) )
            {
                WARN( "TCP eventfd read error: %s", strerror( errno ) );
            }
        }
        else if( msg_t::endpoint_t::CLI == l_dest )
        {
            if( sizeof( uint64_t ) != read( g_cli_eventfd, &l_data, sizeof( uint64_t ) ) )
            {
                WARN( "CLI eventfd read error: %s", strerror( errno ) );
            }
        }

        return msg->msg_type;
    }

    inline static int get_tcp_eventfd( void )
    {
        return g_tcp_eventfd;
    }

    inline static int get_udp_eventfd( void )
    {
        return g_udp_eventfd;
    }

    inline static int get_cli_eventfd( void )
    {
        return g_cli_eventfd;
    }

private:
    static thread_local msg_t::endpoint_t g_self;
    static std::condition_variable        g_cv[msg_t::endpoint_t::SIZE];
    static std::queue<msg_t>              g_q[msg_t::endpoint_t::SIZE];
    static std::thread::id                g_tid_mapping[msg_t::endpoint_t::SIZE];
    static int                            g_udp_eventfd;
    static int                            g_tcp_eventfd;
    static int                            g_cli_eventfd;

    MsgBus( void ) = delete;

    static std::mutex& getMtx( const msg_t::endpoint_t dest );
};
