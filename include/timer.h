#pragma once

#include "messaging.h"
#include "common.h"

#include <chrono>

/***************************************/
/**************** MACROS ***************/
/***************************************/
#define MAX_WAIT_TIME_MILLI 1000

#define SET_TIMER( _type_, _timeout_, _what_ )      { timer_msg_t l_timer = {}; l_timer.type = timer_msg_t::type_t::_type_; l_timer.timeout = _timeout_; string_to_char_arr( _what_, sizeof( l_timer.data ), l_timer.data );\
                                                        MsgBus::send_msg( msg_type_t::SET_TIMER_MSG, msg_t::endpoint_t::SELF, msg_t::endpoint_t::TMR, l_timer );  }
#define RESET_TIMER( _type_, _what_ )               { timer_msg_t l_timer = {}; l_timer.type = timer_msg_t::type_t::_type_; string_to_char_arr( _what_, sizeof( l_timer.data ), l_timer.data );\
                                                        MsgBus::send_msg( msg_type_t::RESET_TIMER_MSG, msg_t::endpoint_t::SELF, msg_t::endpoint_t::TMR, l_timer ); }

#define SET_TIMER_EMPTY( _type_, _timeout_ )        { timer_msg_t l_timer = {}; l_timer.type = timer_msg_t::type_t::_type_; l_timer.timeout = _timeout_;\
                                                        MsgBus::send_msg( msg_type_t::SET_TIMER_MSG, msg_t::endpoint_t::SELF, msg_t::endpoint_t::TMR, l_timer );  }
#define RESET_TIMER_EMPTY( _type_ )                 { timer_msg_t l_timer = {}; l_timer.type = timer_msg_t::type_t::_type_;\
                                                        MsgBus::send_msg( msg_type_t::RESET_TIMER_MSG, msg_t::endpoint_t::SELF, msg_t::endpoint_t::TMR, l_timer ); }

/***************************************/
/******** DATA TYPE DEFINITIONS ********/
/***************************************/
struct tmr_t
{
    timer_msg_t::type_t type = timer_msg_t::type_t::TIMER_NOT_DEF;
    ChronoSteadyPoint   deadline = {};
    char                data[sizeof( std::declval<timer_msg_t>().data )] = {};
    msg_t::endpoint_t   src = msg_t::endpoint_t::NOT_SET;
};

/***************************************/
/******** FUNCTION DECLARATIONS ********/
/***************************************/
void tmr( void );
