#pragma once

#include <string>
#include <thread>

/***************************************/
/**************** MACROS ***************/
/***************************************/
#define NOCHDIR 1
#define NOCLOSE 1

#ifndef GIT_REV
#define GIT_REV "local"
#endif

/***************************************/
/******** DATA TYPE DEFINITIONS ********/
/***************************************/
struct cmd_args_t
{
    bool        help = false;
    bool        daemon = false;
    bool        version = false;
    std::string cfg_path = "cfg/cfg.txt";

    bool parse( const int argc, const char **argv );
};

struct process_data_t
{
    std::thread sip_thread;
    std::thread cli_thread;
    std::thread udp_thread;
    std::thread tcp_thread;
    std::thread sgn_thread;
    std::thread tmr_thread;
};
