#pragma once

#include "common.h"

/***************************************/
/******** FUNCTION DECLARATIONS ********/
/***************************************/
int deparse_response( const message_t &received_message, const config_t &config, const user_t *user, const sip_status_t response_code, char *response_buffer );
int deparse_relay( const message_t &received_message, const config_t &config, const user_t *user, const protocol_t protocol, const call_t &call, char *relay_buffer );
