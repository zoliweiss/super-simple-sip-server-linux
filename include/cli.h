#pragma once

#include "common.h"

#include <arpa/inet.h>
#include <sys/un.h>
#include <unistd.h>
#include <list>
#include <vector>
#include <sstream>
#include <memory>

/***************************************/
/**************** MACROS ***************/
/***************************************/
#define CLEAR_REST_OF_LINE "\x1B[0K"
#define CLEAR_LINE         "\x1B[2K"
#define START_OF_LINE      "\x1B[0G"

#define PROMPT KGRN "> " KNRM
#define BS_STR "\b"

/* terminal */
#define NUL   0
#define BS    8
#define TAB   9
#define LF   10
#define CR   13
#define ESC  27
#define DEL 127

/* custom codes */
#define ARR_UP   -1
#define ARR_DOWN -2

/***************************************/
/******** DATA TYPE DEFINITIONS ********/
/***************************************/
enum class direction_t
{
    UP,
    DOWN
};

struct elems_t : std::vector<elems_t>
{
    std::string              id;
    std::string              help;
    std::string              endpoint;
    std::unique_ptr<elems_t> child;

    elems_t( void ) = default;
    elems_t( elems_t&& elems ) = default;
    elems_t( std::string &cli_str );
    elems_t( const char *cli_str );
    elems_t( const char **cli_str );
    void parse( const char **cli_str );

    inline void init( void )
    {
        id.clear();
        help.clear();
        endpoint.clear();
        child.release();
    }

    inline bool is_valid_char( const char ch ) const
    {
        return ( ch >= 'a' && ch <= 'z' )
            || ( ch >= 'A' && ch <= 'Z' )
            || ( ch == '_' )
            || ( ch == '?' )
            || ( ch == '-' );
    }

    inline void advance_one_char( const char **cli_str ) const
    {
        (*cli_str)++;
    }

    inline void find_next( const char **cli_str, const char what ) const
    {
        const char *l_idx = nullptr;

        if( nullptr != ( l_idx = strchr( *cli_str, what ) ) )
        {
            *cli_str = l_idx;
        }
    }

    inline bool is_child_start( const char **cli_str ) const
    {
        return '{' == **cli_str;
    }

    inline bool is_endpoint_w_help_start( const char **cli_str ) const
    {
        return '[' == **cli_str;
    }

    inline void skip_invalid( const char **cli_str ) const
    {
        while( !is_valid_char( **cli_str ) )
        {
            advance_one_char( cli_str );
        }
    }

    inline void skip_valid( const char **cli_str ) const
    {
        while( is_valid_char( **cli_str ) )
        {
            advance_one_char( cli_str );
        }
    }

    inline bool is_last_elem( const char **cli_str ) const
    {
        return ',' != **cli_str;
    }

    inline std::string get_help_str( const char **cli_str ) const
    {
        const char  *l_temp_id_start = nullptr;
        std::string  l_temp_str;

        find_next( cli_str, '"' );
        advance_one_char( cli_str );
        l_temp_id_start = *cli_str;
        find_next( cli_str, '"' );
        l_temp_str.assign( l_temp_id_start, *cli_str - l_temp_id_start );
        advance_one_char( cli_str );

        return l_temp_str;
    }

    inline std::string get_endpoint_str( const char **cli_str ) const
    {
        const char  *l_temp_id_start = nullptr;
        std::string  l_temp_str;

        l_temp_id_start = *cli_str;
        skip_valid( cli_str );
        l_temp_str.assign( l_temp_id_start, *cli_str - l_temp_id_start );

        return l_temp_str;
    }

    inline std::string str( void ) const
    {
        std::stringstream l_ss;

        l_ss << *this;

        return l_ss.str();
    }

    friend std::ostream &operator <<( std::ostream& os, const elems_t &elems );
};

class History : public std::vector<std::string>
{
private:
    size_t history_idx = 0;

public:
    History( void );
    void add( const std::string &input );
    std::string move( const direction_t direction );
};

class Terminal
{
private:
    int                sock_desc = -1;
    struct sockaddr_un addr = {};
    struct sockaddr_un from = {};
    socklen_t          fromlen = 0;
    std::stringstream  buf;

public:
    Terminal( const std::string &socket_path );
    ~Terminal( void );
    char get_char( void );
    int get_symbol( void );
    void delete_back_n_chars( const int n );

    inline bool is_socket_open( void ) const
    {
        return -1 != this->sock_desc;
    }

    inline int get_socket( void ) const
    {
        return this->sock_desc;
    }

    inline void close_socket( void )
    {
        close( this->sock_desc );
        this->sock_desc = -1;
    }

    inline bool is_allowed( const char c ) const
    {
        return isgraph( c )
            || ' ' == c;
    }

    inline bool has_data( void ) const
    {
        return buf.good();
    }

    template <class T>
    Terminal &operator <<( const T &t );

    template <class T>
    Terminal &operator >>( T &t );
};

/***************************************/
/******** FUNCTION DECLARATIONS ********/
/***************************************/
std::string &flatten_str( std::string &str );
void cli( config_t *config );
