#pragma once

#include "logger.h"
#include "termcolors.h"

#include <arpa/inet.h>
#include <string.h>
#include <time.h>
#include <poll.h>
#include <string>
#include <vector>
#include <queue>
#include <set>
#include <map>
#include <algorithm>
#include <condition_variable>
#include <mutex>
#include <shared_mutex>
#include <atomic>
#include <thread>

/***************************************/
/********** EXTERNAL VARIABLES *********/
/***************************************/
extern std::atomic<bool> g_exit_requested;

/***************************************/
/**************** MACROS ***************/
/***************************************/
#define BUFF_SIZE  8192
#define MAX_CONNS  1000
#define NO_TIMEOUT   -1

#define NL_STR "\n"

#define SIP_USER_MAX_REG_TIME 900
#define SIP_MAX_FORWARDS       70

/***************************************/
/******** DATA TYPE DEFINITIONS ********/
/***************************************/
using ChronoSteadyPoint = std::chrono::time_point<std::chrono::steady_clock>;

enum is_quoted_t
{
    YES = true,
    NO  = false
};

enum sip_status_t
{
    SIP_STATUS_NOT_DEF  = 0,
    SIP_RESP_MIN        = 100,
    SIP_TRYING          = 100,
    SIP_RINGING         = 180,
    SIP_OK              = 200,
    SIP_UNAUTHORIZED    = 401,
    SIP_FORBIDDEN       = 403,
    SIP_NOT_FOUND       = 404,
    SIP_TEMP_UNAVAIL    = 480,
    SIP_CALL_NOT_EXIST  = 481,
    SIP_BUSY_HERE       = 486,
    SIP_REQUEST_TERM    = 487,
    SIP_NOT_ACC_HERE    = 488,
    SIP_SERVER_ERROR    = 500,
    SIP_NOT_IMPLEMENT   = 501,
    SIP_SERVICE_UNAVAIL = 503,
    SIP_DECLINE         = 603,
    SIP_RESP_MAX        = 603
};

enum sip_method_t
{
    SIP_NOT_DEF = 0,
    SIP_REGISTER,
    SIP_INVITE,
    SIP_CANCEL,
    SIP_BYE,
    SIP_ACK,
    SIP_SUBSCRIBE,
    SIP_INFO,
    SIP_OPTIONS,
    SIP_PRACK,
    SIP_NOTIFY,
    SIP_PUBLISH,
    SIP_REFER,
    SIP_MESSAGE,
    SIP_UPDATE,
    SIP_METHOD_EMPTY
};

constexpr const char *sip_method_t_str[] = {
    "-",
    "REGISTER",
    "INVITE",
    "CANCEL",
    "BYE",
    "ACK",
    "SUBSCRIBE",
    "INFO",
    "OPTIONS",
    "PRACK",
    "NOTIFY",
    "PUBLISH",
    "REFER",
    "MESSAGE",
    "UPDATE",
    ""
};

enum call_state_t
{
    CALL_STATE_NOT_DEF = 0,
    PRE_ALERTING,
    ALERTING,
    ACTIVE,
    CANCEL_PENDING,
    CLEAR_PENDING,
    STATE_EMPTY
};

constexpr const char *call_state_t_str[] = {
    "-",
    "PRE_ALERTING",
    "ALERTING",
    "ACTIVE",
    "CANCEL PENDING",
    "CLEAR PENDING",
    ""
};

enum protocol_t
{
    PROTO_NOT_DEF = 0,
    UDP           = 1,
    TCP           = 2,
    PROTO_EMPTY   = 3
};

constexpr const char *protocol_t_str[] = {
    "-",
    "UDP",
    "TCP",
    ""
};

enum event_t
{
    /* USER */
    USER_ATTEMPT = 0,      /* Registration or de-registration attempt */
    USER_INIT_REG,         /* Successful initial registration */
    USER_INIT_REG_EXIST,   /* Successful initial registration of a configured user */
    USER_RE_REG,           /* Successful re-registration */
    USER_DE_REG,           /* Successful de-registration */
    USER_CHALLENGE,        /* User is challenged */
    USER_NOT_FOUND,        /* De-registration failed because user is not registered */
    USER_INVALID_CRED,     /* Registration failed because user supplied invalid credentials */
    USER_NOT_PERMITTED,    /* Registration failed because user is not configured and authorization is enforced */
    USER_ERROR,            /* Registration failed because of some system error */
    USER_DISCONNECT,       /* De-registration by TCP disconnection */
    USER_DELETE,           /* User is forcefully deleted */

    /* CALL */
    CALL_ATTEMPT,          /* Call attempt */
    CALL_FORBIDDEN,        /* Call is forbidden because user is not registered or the network data is not matching */
    CALL_NOT_FOUND,        /* B party is not registered */
    CALL_BUSY,             /* A or B party is busy */
    CALL_NOT_POSS,         /* Call is forbidden because of unknown reason */
    CALL_RE_INVITE,        /* Re-INVITE is received */
    CALL_CANCEL_PRE_ALERT, /* Call is cancelled in pre-alerting phase */
    CALL_CANCEL,           /* Call is cancelled */
    CALL_DECLINE,          /* Call is declined */
    CALL_ANSWER,           /* Call is answered */
    CALL_RELEASE,          /* Call is released by BYE */
    CALL_TERMINATE         /* Call is terminated at application stop */
};

constexpr const char *event_t_str[] = {
    "REG_DEREG_ATTEMPT",
    "INIT_REG",
    "INIT_REG_EXISTING",
    "RE_REG",
    "DE_REG",
    "CHALLENGING",
    "NOT_FOUND",
    "INVALID_CREDENTIALS",
    "NOT_PERMITTED",
    "ERROR",
    "DISCONNECT",

    "ATTEMPT",
    "FORBIDDEN",
    "NOT_FOUND",
    "BUSY",
    "NOT_POSSIBLE",
    "RE_INVITE",
    "CANCEL_PRE_ALERT",
    "CANCEL",
    "DECLINE",
    "ANSWER",
    "RELEASE",
    "TERMINATE",

    ""
};

struct cseq_t
{
    int          num = 0;
    sip_method_t method = sip_method_t::SIP_NOT_DEF;

    std::string str( void ) const;

    inline void init( void )
    {
        num    = 0;
        method = sip_method_t::SIP_NOT_DEF;
    }
};

enum class uri_scheme_t
{
    NOT_DEF = 0,
    SIP,
    TEL
};

struct uri_t
{
    std::string  display_name;
    uri_scheme_t scheme = uri_scheme_t::NOT_DEF;
    std::string  name;
    std::string  domain;
    int          port = 0;
    std::string  tag;

    inline void init( void )
    {
        display_name.clear();
        scheme = uri_scheme_t::NOT_DEF;
        name.clear();
        domain.clear();
        port = 0;
        tag.clear();
    }
};

struct via_t
{
    std::string full;

    inline void init( void )
    {
        full.clear();
    }
};

struct vias_t : std::vector<via_t>
{
    inline void init( void )
    {
        this->clear();
    }
};

struct authorization_t
{
    std::string nonce;
    std::string uri;
    std::string nc;
    std::string cnonce;
    std::string response;
    std::string opaque;

    inline void init( void )
    {
        nonce.clear();
        uri.clear();
        nc.clear();
        cnonce.clear();
        response.clear();
        opaque.clear();
    }
};

struct content_t
{
    constexpr static int MAX_LEN = 1000;

    std::string type;
    int         len;
    char        payload[MAX_LEN];

    content_t( void );

    inline void init( void )
    {
        type.clear();
        len = 0;
        payload[0] = '\0';
    }
};

struct network_t
{
    struct sockaddr_in ip_data = {};
    int                socket = 0;
    protocol_t         protocol = protocol_t::PROTO_NOT_DEF;

    std::string ip_str( void ) const;
    std::string port_str( void ) const;
    std::string socket_str( void ) const;
    std::string str( void ) const;

    inline void init( void )
    {
        socket = 0;
        protocol = protocol_t::PROTO_NOT_DEF;
        memset( &ip_data, 0, sizeof( ip_data ) );
    }

    inline bool operator==( const network_t& other ) const
    {
        if( this->protocol != other.protocol )
        {
            return false;
        }

        if( protocol_t::TCP == this->protocol )
        {
            return this->socket == other.socket;
        }
        else if( protocol_t::UDP == protocol )
        {
            return this->ip_data.sin_addr.s_addr == other.ip_data.sin_addr.s_addr
                && this->ip_data.sin_port        == other.ip_data.sin_port;
        }

        return false;
    }
};

struct utctime_t
{
    time_t   sec = 0;
    long int nsec = 0;
    bool     valid = false;

    std::string get_iso8601_str( void ) const;
    std::string get_elapsed_str( void ) const;
    void set( const time_t sec );
    void set( const time_t sec, const int nsec );
    void set_actual( void );

    inline bool operator==( const utctime_t& other ) const
    {
        return this->sec   == other.sec
            && this->nsec  == other.nsec
            && this->valid == other.valid;
    }
};

struct call_t
{
    call_state_t state = call_state_t::CALL_STATE_NOT_DEF;
    std::string  call_id;
    vias_t       invite_vias;
    cseq_t       invite_cseq;
    std::string  invite_from_tag;
    std::string  invite_to_tag;
    std::string  a_username;
    std::string  b_username;
    std::string  last_request_from_tag;
    std::string  last_request_to_tag;
    utctime_t    time_call_started;

    std::string str( void ) const;
    std::string get_csv_line( void ) const;
};

struct supp_req_t
{
    bool rel100 = false;

    inline void init( void )
    {
        rel100 = false;
    }

    inline bool is_set( void ) const
    {
        return rel100;
    }
};

struct allow_t
{
    bool prack = false;
    bool update = false;

    inline void init( void )
    {
        prack = false;
        update = false;
    }
};

struct route_t
{
    std::string full;

    inline void init( void )
    {
        full.clear();
    }
};

struct routes_t : std::vector<route_t>
{
    inline void init( void )
    {
        this->clear();
    }
};

struct message_t
{
    sip_method_t    method = sip_method_t::SIP_NOT_DEF;
    uri_t           ruri;
    sip_status_t    response_code = sip_status_t::SIP_STATUS_NOT_DEF;
    int             expires = SIP_USER_MAX_REG_TIME;
    uri_t           from;
    uri_t           to;
    std::string     call_id;
    vias_t          vias;
    cseq_t          cseq;
    std::string     rseq;
    std::string     rack;
    supp_req_t      supported;
    supp_req_t      require;
    allow_t         allow;
    uri_t           contact;
    network_t       nw_data;
    authorization_t authorization;
    int             max_forwards = SIP_MAX_FORWARDS;
    routes_t        routes;
    routes_t        record_routes;
    content_t       content;

    bool is_in_dialog( const call_t &call ) const;
    std::string get_csv_line( void ) const;

    inline void init( void )
    {
        method = sip_method_t::SIP_NOT_DEF;
        response_code = sip_status_t::SIP_STATUS_NOT_DEF;
        expires = SIP_USER_MAX_REG_TIME;
        max_forwards = SIP_MAX_FORWARDS;

        ruri.init();
        from.init();
        to.init();
        call_id.clear();
        vias.init();
        cseq.init();
        rseq.clear();
        rack.clear();
        supported.init();
        require.init();
        allow.init();
        contact.init();
        nw_data.init();
        authorization.init();
        routes.init();
        record_routes.init();
        content.init();
    }

    inline bool is_request( void ) const
    {
        return sip_method_t::SIP_NOT_DEF != method
            && sip_status_t::SIP_STATUS_NOT_DEF == response_code;
    }

    inline bool is_valid_response( void ) const
    {
        return sip_status_t::SIP_RESP_MIN < response_code
            && sip_status_t::SIP_RESP_MAX >= response_code;
    }
};

struct user_t
{
    std::string  name;
    std::string  auth_name;
    std::string  display_name;
    network_t    nw_data;
    std::string  call_id;
    std::string  password;
    std::string  nonce;
    std::string  opaque;
    bool         registered = false;
    utctime_t    time_initial_reg;
    utctime_t    time_re_reg;
    utctime_t    time_de_reg;

    std::string str( void ) const;
    std::string get_csv_line( void ) const;

    inline void clear_auth( void )
    {
        nonce.clear();
        opaque.clear();
    }

    inline void deregister( void )
    {
        registered = false;
        time_de_reg.set_actual();
    }

    inline bool is_user_busy( void ) const
    {
        return !this->call_id.empty();
    }
};

struct packet_t
{
    network_t nw_data;
    int       len = 0;
    char      data[BUFF_SIZE];

    packet_t( void );

    inline void init( void )
    {
        len = 0;
        terminate_buffer();
        nw_data.init();
    }

    inline void terminate_buffer( void )
    {
        if( BUFF_SIZE - 1 > len )
        {
            data[len] = '\0';
        }
    }
};

struct event_log_t
{
    FILE *fp = nullptr;

    void open( const std::string &event_log_path );

    template<typename T>
    void add_event( const event_t log_event, const T &t ) const
    {
        if( nullptr == fp )
        {
            return;
        }

        utctime_t l_acttime;
        l_acttime.set_actual();

        fprintf( fp, "\"%s\",\"%s\",%s\n", l_acttime.get_iso8601_str().c_str(), event_t_str[log_event], t.get_csv_line().c_str() );
        fflush( fp );
    }
};

struct users_t : std::map<std::string, user_t>
{
    event_log_t event_log;
    std::string users_db_save_path;

    std::string str( void ) const;
    user_t* get_user_by_username( const std::string &username );
    user_t* get_user_by_nw_data( const network_t &nw_data );
    int get_number_of_registered_users( void ) const;
    sip_status_t register_user( const message_t &received_message );
    sip_status_t deregister_user( const message_t &received_message );
    void deregister_user_by_nw_data( const network_t &nw_data );
    user_t* get_other_party( const message_t &received_message );
    bool add_call( const std::string &a_username, const std::string &b_username, const std::string &call_id );
    void remove_call( const call_t &call );
    void remove_user( const std::string &username );
    void serialize( std::stringstream *users ) const;
    void deserialize( std::stringstream *users );
    void save( void ) const;
    bool restore( void );
    bool merge_configured_users( const std::vector<user_t> &configured_users );
    void set_users_db_save_path( const std::string &users_db_save_path );
};

struct calls_t : std::map<std::string, call_t>
{
    event_log_t event_log;

    std::string str( void ) const;
    void remove_call( const std::string &call_id );
    call_t* add_call( const message_t &received_message );
    call_t* get_call_by_call_id( const std::string &call_id );
    call_t* get_call_by_partial_call_id( const std::string &partial_call_id );
    call_t* get_call_by_username( users_t &users, const std::string &username );
    void serialize( std::stringstream *calls ) const;
    void deserialize( std::stringstream *calls );
};

struct master_sockets_t
{
    int tcp = -1;
    int udp = -1;

    inline void init( void )
    {
        tcp = -1;
        udp = -1;
    }
};

struct config_t
{
    struct static_data_t
    {
        std::string      server_ip_address = "127.0.0.1";
        master_sockets_t master_sockets;
    };

    int                 server_port = 5060;
    int                 max_call_duration = 3600;                       /* changed */
    std::string         interface = "lo";
    bool                force_auth = false;                             /* changed */
    bool                udp_enabled = true;
    bool                tcp_enabled = true;
    loglevel_t          loglevel = loglevel_t::INFO;                    /* changed via own mutexed setter */
    std::string         log_path = "stderr";                            /* changed via own mutexed setter */
    std::string         cli_sock_path = "/var/run/ssss.sock";
    std::string         users_db_save_path = "/var/run/ssss_users.bin";
    std::string         user_event_log_path = "/var/log/ssss_user.log";
    std::string         call_event_log_path = "/var/log/ssss_call.log";
    std::vector<user_t> configured_users;

    /* for data not coming from config but initialized only once */
    static_data_t        static_data;

    /* mutex used only for data accessed simultaneously, not all fields are like this, only the marked ones */
    std::shared_mutex    mtx;
    std::string          cfg_path;

    config_t( void ) = default;
    config_t( const config_t& other )
    {
        this->copy( other );
    }

    config_t& operator=( const config_t& other )
    {
        this->copy( other );
        return *this;
    }

    void copy( const config_t& other )
    {
        server_port         = other.server_port;
        max_call_duration   = other.max_call_duration;
        interface           = other.interface;
        force_auth          = other.force_auth;
        udp_enabled         = other.udp_enabled;
        tcp_enabled         = other.tcp_enabled;
        loglevel            = other.loglevel;
        log_path            = other.log_path;
        cli_sock_path       = other.cli_sock_path;
        users_db_save_path  = other.users_db_save_path;
        user_event_log_path = other.user_event_log_path;
        call_event_log_path = other.call_event_log_path;
        configured_users    = other.configured_users;
        static_data         = other.static_data;
        cfg_path            = other.cfg_path;
    }

    std::string clean_line( const std::string &line );
    bool parse_param( const std::string &param, std::string *key, std::string *value );
    void parse_general_params( const std::vector<std::string> &general_params );
    bool parse_user( const std::vector<std::string> &user_params, user_t *user );
    void parse_users( const std::vector<std::string> &users_lines );
    bool parse( void );
    bool parse( const std::string &cfg_path );
    void remove_configured_users( void );
    std::string str( void ) const;
};

struct poll_t
{
    struct pollfd pollfds[MAX_CONNS] = {};
    int           len = 0;

    inline void add_fd( const int fd )
    {
        if( len < MAX_CONNS )
        {
            pollfds[len].fd = fd;
            pollfds[len].events = POLLIN;

            len++;
        }
    }

    inline void remove_fd( const int fd )
    {
        for( int i = 0; i < len; i++ )
        {
            if( fd == pollfds[i].fd )
            {
                len--;

                if( i < len )
                {
                    pollfds[i] = pollfds[len];
                }

                memset( &pollfds[len], 0, sizeof( pollfds[0] ) );

                return;
            }
        }
    }

    inline bool is_fd_set( const int fd ) const
    {
        for( int i = 0; i < len; i++ )
        {
            if( fd == pollfds[i].fd )
            {
                return POLLIN & pollfds[i].revents;
            }
        }

        return false;
    }

    inline int get_len( void ) const
    {
        return len;
    }

    inline struct pollfd* get_fds( void )
    {
        return pollfds;
    }
};

struct gen_buffer_t
{
    int  len = 0;
    char data[BUFF_SIZE];
};

struct cli_tcp_nw_data_t
{
    int num_tcp_clients = 0;
};

struct terminate_call_t
{
    bool force = false;
    char call_id[64] = {};
};

struct terminate_call_resp_t
{
    int num_calls_terminated = 0;
};

struct delete_user_t
{
    char user_id[64] = {};
};

struct delete_user_resp_t
{
    int num_users_deleted = 0;
};

struct timer_msg_t
{
    enum type_t
    {
        TIMER_NOT_DEF,
        CALL_RELEASE,
        USER_DEREG,
        CLI_TIMEOUT,
    };

    int    timeout = 0;
    type_t type = type_t::TIMER_NOT_DEF;
    char   data[128] = {};
};

class ExitException : std::exception
{
};

class TimeoutException : std::exception
{
};

/***************************************/
/******** FUNCTION DECLARATIONS ********/
/***************************************/
void trim_ws( std::string &s );
void trim_dq( std::string &s );
size_t string_to_char_arr( const std::string &in, const size_t out_maxlen, char *out );
bool string_to_int( const std::string &in, int *out );
std::string base64_encode( const char *src, const int len );
