#pragma once

/***************************************/
/**************** MACROS ***************/
/***************************************/
#define KNRM  "\x1B[0m"
#define KGRY  "\x1B[30;1m"
#define KRED  "\x1B[31;1m"
#define KGRN  "\x1B[32;1m"
#define KYEL  "\x1B[33;1m"
#define KMAG  "\x1B[35;1m"
#define KCYN  "\x1B[36;1m"
