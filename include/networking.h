#pragma once

#include "common.h"

/***************************************/
/**************** MACROS ***************/
/***************************************/
#define BACKLOG 128

/***************************************/
/******** FUNCTION DECLARATIONS ********/
/***************************************/
bool bind_master_sockets( config_t *config );
void udp( config_t *config );
void tcp( config_t *config );
