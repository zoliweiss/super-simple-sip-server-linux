#pragma once

#include "termcolors.h"

#include <string.h>
#include <stdio.h>
#include <string>

/***************************************/
/**************** MACROS ***************/
/***************************************/
#define ERROR( _msg_, ... )         if(Logger::is_enabled(loglevel_t::ERROR)){\
                                        Logger::mutex_lock();\
                                        if(Logger::has_color()){\
                                            fprintf(Logger::get_log_file_desc(),KRED "[ERROR] " KNRM "[%s] [%s] " _msg_ "\n",Logger::get_time().c_str(),Logger::get_tlid().c_str(),##__VA_ARGS__);\
                                        }\
                                        else{\
                                            fprintf(Logger::get_log_file_desc(),"[ERROR] [%s] [%s] " _msg_ "\n",Logger::get_time().c_str(),Logger::get_tlid().c_str(),##__VA_ARGS__);\
                                        }\
                                        Logger::mutex_unlock();}

#define WARN( _msg_, ... )          if(Logger::is_enabled(loglevel_t::WARN)){\
                                        Logger::mutex_lock();\
                                        if(Logger::has_color()){\
                                            fprintf(Logger::get_log_file_desc(),KYEL "[WARN]  " KNRM "[%s] [%s] " _msg_ "\n",Logger::get_time().c_str(),Logger::get_tlid().c_str(),##__VA_ARGS__);\
                                        }\
                                        else{\
                                            fprintf(Logger::get_log_file_desc(),"[WARN]  [%s] [%s] " _msg_ "\n",Logger::get_time().c_str(),Logger::get_tlid().c_str(),##__VA_ARGS__);\
                                        }\
                                        Logger::mutex_unlock();}

#define INFO( _msg_, ... )          if(Logger::is_enabled(loglevel_t::INFO)){\
                                        Logger::mutex_lock();\
                                        if(Logger::has_color()){\
                                            fprintf(Logger::get_log_file_desc(),KCYN "[INFO]  " KNRM "[%s] [%s] " _msg_ "\n",Logger::get_time().c_str(),Logger::get_tlid().c_str(),##__VA_ARGS__);\
                                        }\
                                        else{\
                                            fprintf(Logger::get_log_file_desc(),"[INFO]  [%s] [%s] " _msg_ "\n",Logger::get_time().c_str(),Logger::get_tlid().c_str(),##__VA_ARGS__);\
                                        }\
                                        Logger::mutex_unlock();}

#define DEBUG( _msg_, ... )         if(Logger::is_enabled(loglevel_t::DEBUG)){\
                                        Logger::mutex_lock();\
                                        if(Logger::has_color()){\
                                            fprintf(Logger::get_log_file_desc(),KGRY "[DEBUG] " KNRM "[%s] [%s] " _msg_ "\n",Logger::get_time().c_str(),Logger::get_tlid().c_str(),##__VA_ARGS__);\
                                        }\
                                        else{\
                                            fprintf(Logger::get_log_file_desc(),"[DEBUG] [%s] [%s] " _msg_ "\n",Logger::get_time().c_str(),Logger::get_tlid().c_str(),##__VA_ARGS__);\
                                        }\
                                        Logger::mutex_unlock();}

#define TRACE( _color_, ... )       if(Logger::is_enabled(loglevel_t::TRACE)){\
                                        Logger::mutex_lock();\
                                        if(Logger::has_color()){\
                                            fprintf(Logger::get_log_file_desc(),_color_ __VA_ARGS__);\
                                        }\
                                        else{\
                                            fprintf(Logger::get_log_file_desc(),__VA_ARGS__);\
                                        }\
                                        Logger::mutex_unlock();}

#define LOG_VAR( _var_ )            if(Logger::is_enabled(loglevel_t::DEBUG)){\
                                        Logger::mutex_lock();\
                                        fprintf(Logger::get_log_file_desc(),"Line %d ",__LINE__);\
                                        fprintf(Logger::get_log_file_desc(),#_var_);\
                                        for(unsigned int i=0;i<sizeof(_var_);i++)\
                                            fprintf(Logger::get_log_file_desc()," %02x",(reinterpret_cast<const unsigned char*>(&_var_))[i]);\
                                        fprintf(Logger::get_log_file_desc(),"\n");\
                                        Logger::mutex_unlock();}

#define LOG_PTR( _ptr_, _size_ )    if(Logger::is_enabled(loglevel_t::DEBUG)){\
                                        Logger::mutex_lock();\
                                        fprintf(Logger::get_log_file_desc(),"Line %d ",__LINE__);\
                                        fprintf(Logger::get_log_file_desc(),#_ptr_);\
                                        if(nullptr==_ptr_){\
                                            fprintf(Logger::get_log_file_desc()," nullptr");}\
                                        else{\
                                            for(unsigned int i=0;i<_size_;i++)\
                                                fprintf(Logger::get_log_file_desc()," %02x",(reinterpret_cast<const unsigned char*>(_ptr_))[i]);}\
                                        fprintf(Logger::get_log_file_desc(),"\n");\
                                        Logger::mutex_unlock();}

#define DUMP_USERS( _users_ )       if(Logger::is_enabled(loglevel_t::TRACE)){\
                                        Logger::mutex_lock();\
                                        fprintf(Logger::get_log_file_desc(),"%s",(_users_).str().c_str());\
                                        Logger::mutex_unlock();}

#define DUMP_CALLS( _calls_ )       if(Logger::is_enabled(loglevel_t::TRACE)){\
                                        Logger::mutex_lock();\
                                        fprintf(Logger::get_log_file_desc(),"%s",(_calls_).str().c_str());\
                                        Logger::mutex_unlock();}

/***************************************/
/******** DATA TYPE DEFINITIONS ********/
/***************************************/
enum loglevel_t
{
    ERROR = 0,
    WARN,
    INFO,
    DEBUG,
    TRACE
};

constexpr const char *loglevel_t_str[] = {
    "ERROR",
    "WARN",
    "INFO",
    "DEBUG",
    "TRACE",
    ""
};

class Logger
{
public:
    static loglevel_t from_str( const std::string &loglevel_str );
    static const char *get_loglevel_str( void );
    static loglevel_t get_loglevel( void );
    static void set_loglevel( const loglevel_t loglevel );
    static void set_loglevel( const std::string &loglevel_str );
    static void set_log_path( const std::string &log_path_str );
    static void set_tlid( const std::string &tlid );
    static std::string get_tlid( void );
    static void set_no_color( void );
    static void mutex_lock( void );
    static void mutex_unlock( void );
    static std::string get_time( void );

    inline static loglevel_t get_default( void )
    {
        return loglevel_t::INFO;
    }

    inline static bool is_enabled( const loglevel_t loglevel )
    {
        return g_loglevel >= loglevel;
    }

    inline static bool has_color( void )
    {
        return !g_no_color;
    }

    inline static FILE *get_log_file_desc( void )
    {
        return g_log_file_desc;
    }

private:
    static thread_local std::string  g_tlid;
    static loglevel_t                g_loglevel;
    static bool                      g_no_color;
    static FILE                     *g_log_file_desc;

    Logger( void ) = delete;
};
