#pragma once

#include <string>

#define MD5_BYTE_LEN 16

std::string hash_md5( const std::string &input );
