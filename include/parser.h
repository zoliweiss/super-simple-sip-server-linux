#pragma once

#include "common.h"

/***************************************/
/******** DATA TYPE DEFINITIONS ********/
/***************************************/
enum class header_type_t
{
    NOT_DEF = 0,

    STARTLINE,
    VIA,
    FROM,
    TO,
    CALL_ID,
    CONTACT,
    CONTENT_TYPE,
    CSEQ,
    RSEQ,
    RACK,
    SUPPORTED,
    REQUIRE,
    ALLOW,
    CONTENT_LENGTH,
    EXPIRES,
    ROUTE,
    RECORD_ROUTE,
    AUTHORIZATION,
    MAX_FORWARDS,

    SIZE
};

struct header_t
{
    header_type_t type = header_type_t::NOT_DEF;
    int           start = -1;
    int           len = 0;
};

struct header_map_t
{
    header_type_t  type = header_type_t::NOT_DEF;
    const char    *str = nullptr;
    int            len = 0;
};

/***************************************/
/******** FUNCTION DECLARATIONS ********/
/***************************************/
void parse_packet( const packet_t &packet_from_queue, std::vector<message_t> *received_messages );
