#!/usr/bin/env bash

KCYN="\033[1;36m"
KYEL="\033[1;33m"
KNRM="\033[0m"

function restore_tty()
{
    stty icanon echo
}

if ! command -v nc &> /dev/null; then
    echo -e "${KYEL}-- Command 'nc' could not be found!${KNRM}"
    exit
fi

# Possible SSSS config locations
FILES=(
    "cfg/cfg.txt"
    "/etc/ssss/cfg.txt"
)

# Sock file default location
SOCK_FILE="/var/run/ssss.sock"

for FILE in ${FILES[@]}; do
    if [ -f "${FILE}" ]; then
        SOCK_FILE_CFG=$(cat ${FILE} | grep cli_sock_path | grep -v '#' | cut -d '=' -f 2)

        if [ ! -z "${SOCK_FILE_CFG}" ]; then
            SOCK_FILE=$(echo ${SOCK_FILE_CFG} | tr -d '"')
        fi
    fi
done

# Assign the handler function to the SIGINT signal
trap restore_tty SIGINT

echo -e "${KCYN}-- Using sock file ${SOCK_FILE}${KNRM}"
echo -e "${KCYN}-- Press Enter to gain prompt${KNRM}"
echo -e "${KCYN}-- Exit with Ctrl + C${KNRM}"

stty -icanon -echo
nc -uU ${SOCK_FILE}
restore_tty
