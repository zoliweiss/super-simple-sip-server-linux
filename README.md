This is the Linux only version of my other [Super Simple Sip Server](https://gitlab.com/zoliweiss/super-simple-sip-server) which is multiplatformish. Like that, neither this conforms to any RFC, however was tested with PhonerLite and MicroSIP, so use it at your own risk.

It can work as a daemon if you start with the `-d` option. CLI is accessible with the provided `ssss_cli.sh` script. You should definitely read the `cfg/cfg.txt` config file, maybe you will need to tailor the parameters to your needs.
