#include "timer.h"
#include "logger.h"

#include <map>

/***************************************/
/* FORWARD DECLARATION OF MSG HANDLERS */
/***************************************/
static void handle_msg_set_timer_msg( timer_msg_t *timer_msg, const msg_t::endpoint_t src, std::map<std::string, tmr_t> *timers );
static void handle_msg_reset_timer_msg( timer_msg_t *timer_msg, const msg_t::endpoint_t src, std::map<std::string, tmr_t> *timers );
static void handle_msg_die( void );

/***************************************/
/************ FREE FUNCTIONS ***********/
/***************************************/
static const std::string get_timer_key( const timer_msg_t::type_t type, const msg_t::endpoint_t src, const char *data )
{
    return std::to_string( type ) + ":" + std::to_string( src ) + ":" + std::string( data );
}

static void remove_timer( const std::string &key, std::map<std::string, tmr_t> *timers )
{
    timers->erase( key );
}

static ChronoSteadyPoint notify_expired_and_get_nearest_deadline( std::map<std::string, tmr_t> *timers )
{
    std::set<std::string> l_expired_timer_keys;
    ChronoSteadyPoint     l_nearest_deadline = ChronoSteadyPoint::max();

    const ChronoSteadyPoint l_now = std::chrono::steady_clock::now();

    for( const auto &i_timer : *timers )
    {
        if( i_timer.second.deadline < l_now )
        {
            l_expired_timer_keys.insert( i_timer.first );
        }

        l_nearest_deadline = std::min( l_nearest_deadline, i_timer.second.deadline );
    }

    for( const auto &i_expired_timer_key : l_expired_timer_keys )
    {
        timer_msg_t l_out_timer;

        const tmr_t l_expired_timer = timers->at( i_expired_timer_key );

        l_out_timer.type = l_expired_timer.type;
        memcpy( l_out_timer.data, l_expired_timer.data, sizeof( l_out_timer.data ) );

        /* using the function call instead of the macro because it expects constants for the first three positions */
        MsgBus::send_msg( msg_type_t::TIMER_EXPIRED, msg_t::endpoint_t::SELF, l_expired_timer.src, l_out_timer );
        remove_timer( i_expired_timer_key, timers );
    }

    return l_nearest_deadline;
}

/***************************************/
/****** MESSAGE HANDLER FUNCTIONS ******/
/***************************************/
static void handle_msg_set_timer_msg( timer_msg_t *timer_msg, const msg_t::endpoint_t src, std::map<std::string, tmr_t> *timers )
{
    tmr_t l_timer = {};

    l_timer.type     = timer_msg->type;
    l_timer.src      = src;
    l_timer.deadline = std::chrono::steady_clock::now() + std::chrono::milliseconds( timer_msg->timeout );
    memcpy( l_timer.data, timer_msg->data, sizeof( l_timer.data ) );

    (*timers)[get_timer_key( l_timer.type, l_timer.src, l_timer.data )] = l_timer;
}

static void handle_msg_reset_timer_msg( timer_msg_t *timer_msg, const msg_t::endpoint_t src, std::map<std::string, tmr_t> *timers )
{
    remove_timer( get_timer_key( timer_msg->type, src, timer_msg->data ), timers );
}

static void handle_msg_die( void )
{
    throw ExitException();
}

/***************************************/
/*** THREAD MAIN FUNCTION / EVENTLOOP **/
/***************************************/
void tmr()
{
    Logger::set_tlid( "TMR" );
    MsgBus::register_thread( msg_t::endpoint_t::TMR );
    DEBUG( "Starting thread, TID: %u", gettid() );

    std::map<std::string, tmr_t> l_timers;
    int                          l_wait_duration = MAX_WAIT_TIME_MILLI;

    try
    {
        while( true )
        {
            DECL_MSG_INPUT_DATA();

            switch( MSG_INPUT_W_WAIT( l_wait_duration ) )
            {
                case msg_type_t::SET_TIMER_MSG:
                    handle_msg_set_timer_msg( reinterpret_cast<timer_msg_t*>( MSG_INPUT_DATA ), MSG_SRC, &l_timers );
                    break;

                case msg_type_t::RESET_TIMER_MSG:
                    handle_msg_reset_timer_msg( reinterpret_cast<timer_msg_t*>( MSG_INPUT_DATA ), MSG_SRC, &l_timers );
                    break;

                case msg_type_t::DIE:
                    handle_msg_die();
                    break;

                default:
                    /* nothing to do */
                    break;
            }

            const ChronoSteadyPoint l_nearest_deadline = notify_expired_and_get_nearest_deadline( &l_timers );

            l_wait_duration = std::min( std::chrono::duration_cast<std::chrono::milliseconds>( l_nearest_deadline - std::chrono::steady_clock::now() ).count(), static_cast<std::chrono::milliseconds::rep>( MAX_WAIT_TIME_MILLI ) );
        }
    }
    catch( const ExitException &e )
    {
        /* nothing to do */
    }

    DEBUG( "Exiting thread, TID: %u", gettid() );
}
