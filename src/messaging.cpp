#include "messaging.h"

/***************************************/
/***** STATIC AND GLOBAL VARIABLES *****/
/***************************************/
namespace
{
    std::mutex g_messaging_mtx[msg_t::endpoint_t::SIZE];
}

thread_local msg_t::endpoint_t MsgBus::g_self = msg_t::endpoint_t::NOT_SET;
std::condition_variable        MsgBus::g_cv[msg_t::endpoint_t::SIZE];
std::queue<msg_t>              MsgBus::g_q[msg_t::endpoint_t::SIZE];
std::thread::id                MsgBus::g_tid_mapping[msg_t::endpoint_t::SIZE];
int                            MsgBus::g_udp_eventfd = -1;
int                            MsgBus::g_tcp_eventfd = -1;
int                            MsgBus::g_cli_eventfd = -1;

/***************************************/
/*** CLASS AND STRUCT IMPLEMENTATION ***/
/***************************************/
std::mutex& MsgBus::getMtx( const msg_t::endpoint_t dest )
{
    return g_messaging_mtx[dest];
}
