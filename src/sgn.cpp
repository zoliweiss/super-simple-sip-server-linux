#include "sgn.h"
#include "logger.h"
#include "messaging.h"

#include <signal.h>

/***************************************/
/***** STATIC AND GLOBAL VARIABLES *****/
/***************************************/
std::atomic<bool> g_exit_requested = false;

/***************************************/
/************ FREE FUNCTIONS ***********/
/***************************************/
extern "C" void signal_handler( int signal )
{
    if( SIGINT == signal )
    {
       g_exit_requested = true;
    }
}

static void set_sigaction( int signal, const struct sigaction *action )
{
    struct sigaction l_old_action;

    sigaction( signal, nullptr, &l_old_action );

    if( SIG_IGN != l_old_action.sa_handler )
    {
        sigaction( signal, action, nullptr );
    }
}

static void bind_signal_handlers( void )
{
    struct sigaction l_new_action;

    l_new_action.sa_handler = signal_handler;
    sigemptyset( &l_new_action.sa_mask );
    l_new_action.sa_flags = 0;

    set_sigaction( SIGINT, &l_new_action );
}

void sgn()
{
    bind_signal_handlers();

    Logger::set_tlid( "SGN" );
    MsgBus::register_thread( msg_t::endpoint_t::SGN );
    DEBUG( "Starting thread, TID: %u", gettid() );

    /*
     * The main thread needs to receive internal messages, but it is not possible to simultaneously handle
     * interrupts and internal signals, so that is why this signal handler thread was created.
     *
     * You should not set condition variables in the signal handler, so this is the best I was able to come up.
     * Anyway, you would need the while loop because of the spurious wakeups, sooo...
     */
    while( !g_exit_requested )
    {
        std::this_thread::sleep_for( std::chrono::milliseconds( 100 ) );
    }

    MSG_OUTPUT_EMPTY( EXIT_REQUESTED, APP );

    DEBUG( "Exiting thread, TID: %u", gettid() );
}
