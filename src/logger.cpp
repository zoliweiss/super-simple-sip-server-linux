#include "logger.h"

#include <string.h>
#include <mutex>
#include <algorithm>

/***************************************/
/***** STATIC AND GLOBAL VARIABLES *****/
/***************************************/
namespace
{
    std::mutex g_logger_mtx;
}

thread_local std::string  Logger::g_tlid;
loglevel_t                Logger::g_loglevel = Logger::get_default();
bool                      Logger::g_no_color = false;
FILE                     *Logger::g_log_file_desc = stderr;

/***************************************/
/*** CLASS AND STRUCT IMPLEMENTATION ***/
/***************************************/
loglevel_t Logger::from_str( const std::string &loglevel_str )
{
    int         l_loglevel = loglevel_t::ERROR;
    std::string l_loglevel_str = loglevel_str;

    while( '\0' != loglevel_t_str[l_loglevel][0] )
    {
        transform( l_loglevel_str.begin(), l_loglevel_str.end(), l_loglevel_str.begin(), []( char c ){ return std::toupper( c ); } );

        if( loglevel_t_str[l_loglevel] == l_loglevel_str )
        {
            return static_cast<loglevel_t>( l_loglevel );
        }

        l_loglevel++;
    }

    return Logger::get_default();
}

void Logger::set_loglevel( const loglevel_t loglevel )
{
    switch( loglevel )
    {
        case loglevel_t::ERROR:
        case loglevel_t::WARN:
        case loglevel_t::INFO:
        case loglevel_t::DEBUG:
        case loglevel_t::TRACE:
            Logger::mutex_lock();
            g_loglevel = loglevel;
            Logger::mutex_unlock();

            break;
        default:
            break;
    }
}

void Logger::set_loglevel( const std::string &loglevel_str )
{
    Logger::mutex_lock();
    g_loglevel = Logger::from_str( loglevel_str );
    Logger::mutex_unlock();
}

void Logger::set_log_path( const std::string &log_path_str )
{
    FILE *l_fp = nullptr;

    if( log_path_str.empty() )
    {
        return;
    }

    Logger::mutex_lock();

    if( stdout != g_log_file_desc
     && stderr != g_log_file_desc )
    {
        fclose( g_log_file_desc );
    }

    if( "stdout" == log_path_str )
    {
        g_log_file_desc = stdout;
    }
    else if( "stderr" == log_path_str )
    {
        g_log_file_desc = stderr;
    }
    else if( nullptr != ( l_fp = fopen( log_path_str.c_str(), "a" ) ) )
    {
        g_log_file_desc = l_fp;
        Logger::set_no_color();

        fprintf( l_fp, "\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
    }
    else
    {
        /* Nothing to do, by default it is set to stderr */
    }

    Logger::mutex_unlock();
}

void Logger::set_tlid( const std::string &tlid )
{
    g_tlid = tlid;
}

std::string Logger::get_tlid( void )
{
    return g_tlid;
}

void Logger::mutex_lock( void )
{
    g_logger_mtx.lock();
}

void Logger::mutex_unlock( void )
{
    g_logger_mtx.unlock();
}

std::string Logger::get_time( void )
{
    struct timespec l_rawtime = {};
    char            l_timebuf[38] = {};
    struct tm       l_dummy_tm;
    constexpr int   l_date_len = 19; /* 2022-05-19 12:49:03 */

    clock_gettime( CLOCK_REALTIME, &l_rawtime );
    strftime( l_timebuf, sizeof( l_timebuf ), "%F %T", localtime_r( &l_rawtime.tv_sec, &l_dummy_tm ) );
    snprintf( l_timebuf + l_date_len, sizeof( l_timebuf ) - l_date_len, ".%6.6ld", l_rawtime.tv_nsec / 1'000 );

    return std::string( l_timebuf );
}

const char *Logger::get_loglevel_str( void )
{
    return loglevel_t_str[g_loglevel];
}

loglevel_t Logger::get_loglevel( void )
{
    return g_loglevel;
}

void Logger::set_no_color( void )
{
    g_no_color = true;
}
