#include "sip.h"
#include "common.h"
#include "logger.h"
#include "messaging.h"
#include "md5.h"
#include "parser.h"
#include "deparser.h"
#include "timer.h"

#include <sstream>
#include <set>

/***************************************/
/* FORWARD DECLARATION OF MSG HANDLERS */
/***************************************/
static void handle_msg_packet_received( const packet_t *packet_from_queue, const config_t &config, calls_t *calls, users_t *users );
static void handle_msg_report_closed_connections( const network_t *nw_data, users_t *users );
static void handle_msg_get_users( const users_t &users );
static void handle_msg_get_calls( const calls_t &calls );
static void handle_msg_terminate_call( const terminate_call_t *term_call, const config_t &config, calls_t *calls, users_t *users );
static void handle_msg_delete_user( const delete_user_t *del_user, const config_t &config, calls_t *calls, users_t *users );
static void handle_msg_timer_expired( const timer_msg_t *timer_msg, const config_t &config, calls_t *calls, users_t *users );
static void handle_msg_config_changed( config_t &common_cfg, config_t *local_cfg );
static void handle_msg_reload_conf_users( const config_t &config, calls_t *calls, users_t *users );
static void handle_msg_die( const config_t &config, calls_t *calls, users_t *users );

/***************************************/
/************ FREE FUNCTIONS ***********/
/***************************************/
static call_t* add_call( const message_t &message, calls_t *calls, users_t *users )
{
    if( !users->add_call( message.from.name, message.to.name, message.call_id ) )
    {
        return nullptr;
    }

    return calls->add_call( message );
}

static void remove_call( const call_t &call, calls_t *calls, users_t *users )
{
    users->remove_call( call );
    calls->remove_call( call.call_id );
}

static bool validate_credentials( const message_t &received_message, const user_t *user, const std::string &server_ip_address )
{
    const std::string l_a1_str = user->auth_name + ":" + server_ip_address + ":" + user->password;
    const std::string l_a2_str = "REGISTER:" + received_message.authorization.uri;

    const std::string l_a1_md5_hash = hash_md5( l_a1_str );
    const std::string l_a2_md5_hash = hash_md5( l_a2_str );

    const std::string l_final_str = l_a1_md5_hash + ":" + received_message.authorization.nonce + ":" + received_message.authorization.nc + ":" + received_message.authorization.cnonce + ":auth:" + l_a2_md5_hash;

    const std::string l_final_md5_hash = hash_md5( l_final_str );

    return l_final_md5_hash == received_message.authorization.response
        && user->nonce      == received_message.authorization.nonce
        && user->opaque     == received_message.authorization.opaque;
}

static bool is_callers_nw_data_valid( const network_t& nw_data, const std::string &username, users_t *users )
{
    user_t *l_existing_user = nullptr;

    return nullptr != ( l_existing_user = users->get_user_by_username( username ) )
        && l_existing_user->nw_data == nw_data;
}

static sip_status_t handle_saved_users( const message_t &received_message, const config_t &config, users_t *users )
{
    user_t *l_user_ptr = nullptr;

    if( nullptr != ( l_user_ptr = users->get_user_by_username( received_message.from.name ) )
     && !l_user_ptr->auth_name.empty() )
    {
        if( l_user_ptr->nonce.empty() )
        {
            l_user_ptr->nonce = std::to_string( rand() );
            l_user_ptr->opaque = std::to_string( rand() );

            users->event_log.add_event( event_t::USER_CHALLENGE, received_message );
            INFO( "Challenging user %s", received_message.from.name.c_str() );

            return sip_status_t::SIP_UNAUTHORIZED;
        }
        else if( !validate_credentials( received_message, l_user_ptr, config.static_data.server_ip_address ) )
        {
            l_user_ptr->clear_auth();

            users->event_log.add_event( event_t::USER_INVALID_CRED, received_message );
            INFO( "Invalid credentials from user %s", received_message.from.name.c_str() );

            return sip_status_t::SIP_FORBIDDEN;
        }

        l_user_ptr->clear_auth();
    }
    else if( config.force_auth )
    {
        users->event_log.add_event( event_t::USER_NOT_PERMITTED, received_message );
        INFO( "User %s not permitted", received_message.from.name.c_str() );

        return sip_status_t::SIP_FORBIDDEN;
    }

    return sip_status_t::SIP_STATUS_NOT_DEF;
}

static void send_packet( const char *response_buffer, const int response_len, const network_t &nw_data )
{
    packet_t l_packet;

    l_packet.nw_data = nw_data;
    l_packet.nw_data.ip_data.sin_family = AF_INET;
    l_packet.len = response_len;

    memcpy( &l_packet.data, response_buffer, response_len );

    switch( nw_data.protocol )
    {
        case protocol_t::UDP:
            MSG_OUTPUT( SEND_PACKET, UDP, l_packet );
            break;
        case protocol_t::TCP:
            MSG_OUTPUT( SEND_PACKET, TCP, l_packet );
            break;
        default:
            break;
    }
}

static void send_response_to_invite( const sip_status_t response_code, const config_t &config, const call_t &call, users_t *users )
{
    message_t  l_message;
    packet_t   l_packet;
    user_t    *l_user_ptr = nullptr;

    if( nullptr == ( l_user_ptr = users->get_user_by_username( call.a_username ) ) )
    {
        return;
    }

    l_message.cseq = call.invite_cseq;
    l_message.call_id = call.call_id;
    l_message.contact.name = call.b_username;
    l_message.from.name = call.a_username;
    l_message.from.tag = call.invite_from_tag;
    l_message.to.name = call.b_username;
    l_message.to.tag = call.invite_to_tag;
    l_message.vias = call.invite_vias;

    l_packet.len = deparse_response( l_message, config, nullptr, response_code, l_packet.data );
    send_packet( l_packet.data, l_packet.len, l_user_ptr->nw_data );

    DEBUG( "Response generated to %s", l_user_ptr->nw_data.str().c_str() );
    TRACE( KCYN, "%s", l_packet.data );
}

static void send_request( const sip_method_t method, const config_t &config, const call_t &call, const message_t &received_message, user_t *user )
{
    if( nullptr == user )
    {
        return;
    }

    message_t l_message;
    packet_t  l_packet;

    l_message.method = method;
    l_message.cseq.method = method;
    l_message.cseq.num = call.invite_cseq.num + 10000;
    l_message.call_id = call.call_id;

    if( user->name == call.a_username )
    {
        l_message.contact.name = call.b_username;
        l_message.ruri.name = call.a_username;
        l_message.from.name = call.b_username;
        l_message.from.tag = call.invite_to_tag;
        l_message.to.name = call.a_username;
        l_message.to.tag = call.invite_from_tag;
    }
    else
    {
        l_message.contact.name = call.a_username;
        l_message.ruri.name = call.b_username;
        l_message.from.name = call.a_username;
        l_message.from.tag = call.invite_from_tag;
        l_message.to.name = call.b_username;

        if( sip_method_t::SIP_CANCEL != method )
        {
            l_message.to.tag = call.invite_to_tag;
        }
    }

    l_message.vias = received_message.vias;

    l_packet.len = deparse_relay( l_message, config, nullptr, user->nw_data.protocol, call, l_packet.data );
    send_packet( l_packet.data, l_packet.len, user->nw_data );

    DEBUG( "Request generated to %s", user->nw_data.str().c_str() );
    TRACE( KCYN, "%s", l_packet.data );
}

static void terminate_call( const config_t& config, const bool force, calls_t *calls, users_t *users, call_t *call )
{
    const message_t l_message;

    switch( call->state )
    {
        case call_state_t::PRE_ALERTING:
            send_response_to_invite( sip_status_t::SIP_REQUEST_TERM, config, *call, users );
            call->state = call_state_t::CLEAR_PENDING;

            break;

        case call_state_t::ALERTING:
            send_request( sip_method_t::SIP_CANCEL, config, *call, l_message, users->get_user_by_username( call->b_username ) );
            call->state = call_state_t::CANCEL_PENDING;
            send_response_to_invite( sip_status_t::SIP_REQUEST_TERM, config, *call, users );

            break;

        case call_state_t::ACTIVE:
            send_request( sip_method_t::SIP_BYE, config, *call, l_message, users->get_user_by_username( call->a_username ) );
            send_request( sip_method_t::SIP_BYE, config, *call, l_message, users->get_user_by_username( call->b_username ) );
            call->state = call_state_t::CLEAR_PENDING;

            break;

        default:
            break;
    }

    calls->event_log.add_event( event_t::CALL_TERMINATE, *call );

    INFO( "Call %s terminated", call->call_id.c_str() );

    if( force )
    {
        RESET_TIMER( CALL_RELEASE, call->call_id );

        remove_call( *call, calls, users );
    }
}

static void delete_user( const config_t& config, calls_t *calls, users_t *users, user_t *user )
{
    call_t *l_call_ptr = nullptr;

    if( user->is_user_busy()
     && nullptr != ( l_call_ptr = calls->get_call_by_partial_call_id( user->call_id ) ) )
    {
        terminate_call( config, true, calls, users, l_call_ptr );
    }

    if( protocol_t::TCP == user->nw_data.protocol )
    {
        MSG_OUTPUT( CLOSE_TCP_SOCKET, TCP, user->nw_data.socket );
    }

    users->event_log.add_event( event_t::USER_DELETE, *user );
    users->remove_user( user->name );
}

static void process_sip_message( const message_t &received_message, const config_t &config, calls_t *calls, users_t *users )
{
    call_t       *l_idle_call_ptr = nullptr;
    call_t       *l_call_ptr = nullptr;
    user_t       *l_other_party_ptr = nullptr;
    user_t       *l_a_ptr = nullptr;
    user_t       *l_b_ptr = nullptr;
    sip_status_t  l_response = sip_status_t::SIP_STATUS_NOT_DEF;
    int           l_send_len = 0;
    char          l_send_buffer[BUFF_SIZE];
    bool          l_relay_needed = false;
    bool          l_is_message_in_dialog = false;

    if( nullptr != ( l_call_ptr = calls->get_call_by_call_id( received_message.call_id ) ) )
    {
        l_is_message_in_dialog = received_message.is_in_dialog( *l_call_ptr );
    }

    if( received_message.is_request() )
    {
        DEBUG( "%s method received from:%s to:%s", sip_method_t_str[received_message.method], received_message.from.name.c_str(), received_message.to.name.c_str() );

        switch( received_message.method )
        {
            case sip_method_t::SIP_REGISTER:
                users->event_log.add_event( event_t::USER_ATTEMPT, received_message );

                if( sip_status_t::SIP_STATUS_NOT_DEF == ( l_response = handle_saved_users( received_message, config, users ) ) )
                {
                    if( 0 == received_message.expires )
                    {
                        l_response = users->deregister_user( received_message );

                        if( sip_status_t::SIP_OK == l_response )
                        {
                            l_idle_call_ptr = calls->get_call_by_username( *users, received_message.from.name );
                            RESET_TIMER( USER_DEREG, received_message.from.name );
                        }
                    }
                    else
                    {
                        l_response = users->register_user( received_message );

                        if( sip_status_t::SIP_OK == l_response )
                        {
                            SET_TIMER( USER_DEREG, SIP_USER_MAX_REG_TIME * 1'000, received_message.from.name );
                        }
                    }
                }

                break;

            case sip_method_t::SIP_INVITE:
                if( nullptr == ( l_a_ptr = users->get_user_by_username( received_message.from.name ) )
                 || !is_callers_nw_data_valid( received_message.nw_data, received_message.from.name, users ) )
                {
                    calls->event_log.add_event( event_t::CALL_FORBIDDEN, received_message );
                    DEBUG( "Call forbidden" );
                    l_response = sip_status_t::SIP_FORBIDDEN;
                }
                else if( nullptr == ( l_b_ptr = users->get_user_by_username( received_message.to.name ) )
                      || !l_b_ptr->registered )
                {
                    calls->event_log.add_event( event_t::CALL_NOT_FOUND, received_message );
                    DEBUG( "User not found" );
                    l_response = sip_status_t::SIP_NOT_FOUND;
                }
                else if( l_is_message_in_dialog )
                {
                    calls->event_log.add_event( event_t::CALL_RE_INVITE, received_message );
                    l_relay_needed = true;
                }
                else if( received_message.from.name == received_message.to.name
                      || l_a_ptr->is_user_busy()
                      || l_b_ptr->is_user_busy() )
                {
                    calls->event_log.add_event( event_t::CALL_BUSY, received_message );
                    DEBUG( "A or B is busy" );
                    l_response = sip_status_t::SIP_BUSY_HERE;
                }
                else if( nullptr != ( l_call_ptr = add_call( received_message, calls, users ) ) )
                {
                    l_response = sip_status_t::SIP_TRYING;
                    l_relay_needed = true;
                    calls->event_log.add_event( event_t::CALL_ATTEMPT, *l_call_ptr );

                    INFO( "Call %s attempt from: %s to: %s", l_call_ptr->call_id.c_str(), l_call_ptr->a_username.c_str(), l_call_ptr->b_username.c_str() );
                }
                else
                {
                    calls->event_log.add_event( event_t::CALL_NOT_POSS, received_message );
                    DEBUG( "Call not possible" );
                    l_response = sip_status_t::SIP_SERVICE_UNAVAIL;
                }

                break;

            case sip_method_t::SIP_BYE:
                if( l_is_message_in_dialog )
                {
                    l_response = sip_status_t::SIP_OK;
                    l_relay_needed = true;

                    l_idle_call_ptr = l_call_ptr;
                    calls->event_log.add_event( event_t::CALL_RELEASE, received_message );
                    INFO( "Call %s released", l_call_ptr->call_id.c_str() );
                }
                else
                {
                    DEBUG( "BYE to non existent call" );
                    l_response = sip_status_t::SIP_CALL_NOT_EXIST;
                }

                break;

            case sip_method_t::SIP_CANCEL:
                if( l_is_message_in_dialog
                 && call_state_t::ALERTING == l_call_ptr->state )
                {
                    l_response = sip_status_t::SIP_OK;
                    l_relay_needed = true;
                    l_call_ptr->state = call_state_t::CANCEL_PENDING;
                }
                else if( l_is_message_in_dialog
                      && call_state_t::PRE_ALERTING == l_call_ptr->state )
                {
                    l_response = sip_status_t::SIP_REQUEST_TERM;
                    l_idle_call_ptr = l_call_ptr;
                    l_call_ptr->state = call_state_t::CANCEL_PENDING;
                    calls->event_log.add_event( event_t::CALL_CANCEL_PRE_ALERT, *l_call_ptr );
                    INFO( "Call %s cancelled", l_call_ptr->call_id.c_str() );
                }
                else
                {
                    DEBUG( "CANCEL to non existent call" );
                    l_response = sip_status_t::SIP_CALL_NOT_EXIST;
                }

                break;

            case sip_method_t::SIP_ACK:
                if( l_is_message_in_dialog )
                {
                    if( call_state_t::CLEAR_PENDING == l_call_ptr->state )
                    {
                        l_idle_call_ptr = l_call_ptr;
                    }
                    else
                    {
                        if( call_state_t::ACTIVE != l_call_ptr->state )
                        {
                            l_call_ptr->state = call_state_t::ACTIVE;
                            l_call_ptr->time_call_started.set_actual();
                            calls->event_log.add_event( event_t::CALL_ANSWER, *l_call_ptr );
                            INFO( "Call %s answered, number of calls: %zu", l_call_ptr->call_id.c_str(), calls->size() );
                            SET_TIMER( CALL_RELEASE, config.max_call_duration * 1'000, received_message.call_id );
                            DUMP_CALLS( *calls );
                        }

                        l_relay_needed = true;
                    }
                }

                break;

            case sip_method_t::SIP_PRACK:
            case sip_method_t::SIP_UPDATE:
                if( l_is_message_in_dialog )
                {
                    l_relay_needed = true;
                }

                break;

            default:
                l_response = sip_status_t::SIP_NOT_IMPLEMENT;

                break;
        }

        if( sip_method_t::SIP_CANCEL != received_message.method
         && nullptr != l_call_ptr )
        {
            l_call_ptr->last_request_from_tag = received_message.from.tag;
            l_call_ptr->last_request_to_tag = received_message.to.tag;
        }
    }
    else if( received_message.is_valid_response()
          && l_is_message_in_dialog )
    {
        l_relay_needed = true;

        if( !received_message.to.tag.empty()
         && l_call_ptr->invite_to_tag.empty() )
        {
            l_call_ptr->invite_to_tag = received_message.to.tag;
            l_call_ptr->last_request_to_tag = received_message.to.tag;
        }

        /* handle CLI call termination */
        if( call_state_t::CLEAR_PENDING == l_call_ptr->state )
        {
            l_idle_call_ptr = l_call_ptr;
        }

        switch( received_message.response_code )
        {
            case sip_status_t::SIP_RINGING:
                l_call_ptr->state = call_state_t::ALERTING;

                break;

            case sip_status_t::SIP_OK:
                switch( received_message.cseq.method )
                {
                    case sip_method_t::SIP_INVITE:
                    case sip_method_t::SIP_PRACK:
                    case sip_method_t::SIP_UPDATE:
                        break;

                    default:
                        l_relay_needed = false;

                        break;
                }

                break;

            case sip_status_t::SIP_REQUEST_TERM:
                INFO( "Call %s cancelled", l_call_ptr->call_id.c_str() );
                calls->event_log.add_event( event_t::CALL_CANCEL, *l_call_ptr );
                l_call_ptr->state = call_state_t::CLEAR_PENDING;
                send_request( sip_method_t::SIP_ACK, config, *l_call_ptr, received_message, users->get_user_by_username( l_call_ptr->b_username ) );

                break;

            case sip_status_t::SIP_BUSY_HERE:
            case sip_status_t::SIP_TEMP_UNAVAIL:
            case sip_status_t::SIP_DECLINE:
                INFO( "Call %s declined", l_call_ptr->call_id.c_str() );
                calls->event_log.add_event( event_t::CALL_DECLINE, *l_call_ptr );
                l_call_ptr->state = call_state_t::CLEAR_PENDING;
                send_request( sip_method_t::SIP_ACK, config, *l_call_ptr, received_message, users->get_user_by_username( l_call_ptr->b_username ) );

                break;

            default:
                break;
        }
    }

    if( sip_status_t::SIP_STATUS_NOT_DEF != l_response )
    {
        l_send_len = deparse_response( received_message, config, users->get_user_by_username( received_message.from.name ), l_response, l_send_buffer );
        send_packet( l_send_buffer, l_send_len, received_message.nw_data );

        DEBUG( "Sent response to %s", received_message.nw_data.str().c_str() );
        TRACE( KCYN, "%s", l_send_buffer );
    }

    if( l_relay_needed )
    {
        l_other_party_ptr = users->get_other_party( received_message );

        l_send_len = deparse_relay( received_message, config, users->get_user_by_username( received_message.from.name ), l_other_party_ptr->nw_data.protocol, *l_call_ptr, l_send_buffer );
        send_packet( l_send_buffer, l_send_len, l_other_party_ptr->nw_data );

        DEBUG( "Relayed message to %s", l_other_party_ptr->nw_data.str().c_str() );
        TRACE( KRED, "%s", l_send_buffer );
    }

    if( nullptr != l_idle_call_ptr )
    {
        RESET_TIMER( CALL_RELEASE, l_idle_call_ptr->call_id );

        remove_call( *l_idle_call_ptr, calls, users );
    }
}

/***************************************/
/****** MESSAGE HANDLER FUNCTIONS ******/
/***************************************/
static void handle_msg_packet_received( const packet_t *packet_from_queue, const config_t &config, calls_t *calls, users_t *users )
{
    std::vector<message_t> l_received_messages;

    l_received_messages.reserve( 1 );

    try
    {
        parse_packet( *packet_from_queue, &l_received_messages );
    }
    catch( const std::exception &e )
    {
        DEBUG( "Packet discarded, exception during parsing: %s, packet: %s", e.what(), base64_encode( packet_from_queue->data, packet_from_queue->len ).c_str() );
        return;
    }

    for( const auto &i_received_message : l_received_messages )
    {
        if( !i_received_message.is_request()
         && !i_received_message.is_valid_response()
         && nullptr == users->get_user_by_nw_data( i_received_message.nw_data ) )
        {
            if( protocol_t::TCP == i_received_message.nw_data.protocol )
            {
                MSG_OUTPUT( CLOSE_TCP_SOCKET, TCP, i_received_message.nw_data.socket );
            }

            DEBUG( "SIP message discarded, is_request: %d, is_valid_response: %d, nw_data: %s, packet: %s", i_received_message.is_request(), i_received_message.is_valid_response(), i_received_message.nw_data.str().c_str(), base64_encode( packet_from_queue->data, packet_from_queue->len ).c_str() );
            return;
        }

        process_sip_message( i_received_message, config, calls, users );
    }
}

static void handle_msg_report_closed_connections( const network_t *nw_data, users_t *users )
{
    users->deregister_user_by_nw_data( *nw_data );
}

static void handle_msg_get_users( const users_t &users )
{
    std::stringstream l_users_ss;
    gen_buffer_t      l_users_buff;

    users.serialize( &l_users_ss );
    l_users_buff.len = string_to_char_arr( l_users_ss.str(), sizeof( l_users_buff.data ), l_users_buff.data );

    MSG_OUTPUT( GET_USERS_RESP, CLI, l_users_buff );
}

static void handle_msg_get_calls( const calls_t &calls )
{
    std::stringstream l_calls_ss;
    gen_buffer_t      l_calls_buff;

    calls.serialize( &l_calls_ss );
    l_calls_buff.len = string_to_char_arr( l_calls_ss.str(), sizeof( l_calls_buff.data ), l_calls_buff.data );

    MSG_OUTPUT( GET_CALLS_RESP, CLI, l_calls_buff );
}

static void handle_msg_terminate_call( const terminate_call_t *term_call, const config_t &config, calls_t *calls, users_t *users )
{
    terminate_call_resp_t l_terminate_call_resp = {};

    if( '\0' == term_call->call_id[0] )
    {
        /*
         * The syntax here is a bit complicated because we need to iterate while
         * erasing from the map.
         */
        for( auto i_call = calls->begin(), next_i_call = i_call; i_call != calls->end(); i_call = next_i_call )
        {
            ++next_i_call;
            terminate_call( config, term_call->force, calls, users, &i_call->second );
            l_terminate_call_resp.num_calls_terminated++;
        }
    }
    else
    {
        call_t            *l_call_ptr = nullptr;
        const std::string  l_call_id( term_call->call_id );

        if( nullptr != ( l_call_ptr = calls->get_call_by_partial_call_id( l_call_id ) ) )
        {
            terminate_call( config, term_call->force, calls, users, l_call_ptr );
            l_terminate_call_resp.num_calls_terminated++;
        }
    }

    MSG_OUTPUT( TERMINATE_CALL_RESP, CLI, l_terminate_call_resp );
}

static void handle_msg_delete_user( const delete_user_t *del_user, const config_t &config, calls_t *calls, users_t *users )
{
    delete_user_resp_t l_delete_user_resp = {};

    if( '\0' == del_user->user_id[0] )
    {
        /*
         * The syntax here is a bit complicated because we need to iterate while
         * erasing from the map.
         */
        for( auto i_user = users->begin(), next_i_user = i_user; i_user != users->end(); i_user = next_i_user )
        {
            ++next_i_user;
            delete_user( config, calls, users, &i_user->second );
            l_delete_user_resp.num_users_deleted++;
        }
    }
    else
    {
        user_t            *l_user_ptr = nullptr;
        const std::string  l_user_id( del_user->user_id );

        if( nullptr != ( l_user_ptr = users->get_user_by_username( l_user_id ) ) )
        {
            delete_user( config, calls, users, l_user_ptr );
            l_delete_user_resp.num_users_deleted++;
        }
    }

    MSG_OUTPUT( DELETE_USER_RESP, CLI, l_delete_user_resp );
}

static void handle_msg_timer_expired( const timer_msg_t *timer_msg, const config_t &config, calls_t *calls, users_t *users )
{
    switch( timer_msg->type )
    {
        case timer_msg_t::type_t::CALL_RELEASE:
        {
            call_t            *l_call_ptr = nullptr;
            const std::string  l_call_id( timer_msg->data );

            DEBUG( "CALL_RELEASE timer expired for call %s", l_call_id.c_str() );

            if( nullptr != ( l_call_ptr = calls->get_call_by_call_id( l_call_id ) ) )
            {
                terminate_call( config, false, calls, users, l_call_ptr );
            }

            break;
        }

        case timer_msg_t::type_t::USER_DEREG:
        {
            user_t            *l_user_ptr = nullptr;
            const std::string  l_user_id( timer_msg->data );

            DEBUG( "USER_DEREG timer expired for user %s", l_user_id.c_str() );

            if( nullptr != ( l_user_ptr = users->get_user_by_username( l_user_id ) ) )
            {
                l_user_ptr->deregister();

                users->save();
            }

            break;
        }

        default:
            /* nothing to do */
            break;
    }
}

static void handle_msg_config_changed( config_t &common_cfg, config_t *local_cfg )
{
    std::unique_lock l_lock( common_cfg.mtx );
    *local_cfg = common_cfg;
}

static void handle_msg_reload_conf_users( const config_t &config, calls_t *calls, users_t *users )
{
    config_t l_config = config;

    if( !l_config.parse()
     || !users->merge_configured_users( l_config.configured_users ) )
    {
        WARN( "Unable to reload users from file %s", l_config.cfg_path.c_str() );

        return;
    }

    /*
     * At this moment 'users' shoud contain everybody from the
     * config, so we do a second search to find who was removed
     * from the configuration and delete them.
     */

    std::set<std::string> l_configured_user_names;

    for( const auto &i_user : l_config.configured_users )
    {
        l_configured_user_names.insert( i_user.name );
    }

    for( auto i_user = users->begin(), next_i_user = i_user; i_user != users->end(); i_user = next_i_user )
    {
        ++next_i_user;

        if( !l_configured_user_names.contains( i_user->first ) )
        {
            delete_user( l_config, calls, users, &i_user->second );
        }
    }

    INFO( "Users were reloaded from the config" );
}

static void handle_msg_die( const config_t &config, calls_t *calls, users_t *users )
{
    MSG_OUTPUT_EMPTY( DISABLE_INBOUND_TRAFFIC, UDP );
    MSG_OUTPUT_EMPTY( DISABLE_INBOUND_TRAFFIC, TCP );

    if( 0 < calls->size() )
    {
        WARN( "Terminating %zu call(s) on exit", calls->size() );
    }

    for( auto &i_call : *calls )
    {
        terminate_call( config, false, calls, users, &i_call.second );
    }

    MSG_OUTPUT_EMPTY( DIE, UDP );
    MSG_OUTPUT_EMPTY( DIE, TCP );

    throw ExitException();
}

/***************************************/
/*** THREAD MAIN FUNCTION / EVENTLOOP **/
/***************************************/
void sip( config_t *config )
{
    Logger::set_tlid( "SIP" );
    MsgBus::register_thread( msg_t::endpoint_t::SIP );
    DEBUG( "Starting thread, TID: %u", gettid() );

    users_t  l_users;
    calls_t  l_calls;
    config_t l_config = *config;

    l_users.set_users_db_save_path( l_config.users_db_save_path );

    if( l_users.restore() )
    {
        INFO( "%zu user(s) successfully restored from '%s'", l_users.size(), l_config.users_db_save_path.c_str() );
    }
    else if( l_users.merge_configured_users( l_config.configured_users ) )
    {
        l_config.remove_configured_users();

        INFO( "%zu user(s) read from config", l_users.size() );
    }

    l_users.save();

    l_users.event_log.open( l_config.user_event_log_path );
    l_calls.event_log.open( l_config.call_event_log_path );

    MSG_OUTPUT_EMPTY( ENABLE_INBOUND_TRAFFIC, UDP );
    MSG_OUTPUT_EMPTY( ENABLE_INBOUND_TRAFFIC, TCP );

    try
    {
        while( true )
        {
            DECL_MSG_INPUT_DATA();

            switch( MSG_INPUT() )
            {
                case msg_type_t::PACKET_RECEIVED:
                    handle_msg_packet_received( reinterpret_cast<packet_t*>( MSG_INPUT_DATA ), l_config, &l_calls, &l_users );
                    break;

                case msg_type_t::REPORT_CLOSED_CONNECTIONS:
                    handle_msg_report_closed_connections( reinterpret_cast<network_t*>( MSG_INPUT_DATA ), &l_users );
                    break;

                case msg_type_t::GET_USERS:
                    handle_msg_get_users( l_users );
                    break;

                case msg_type_t::GET_CALLS:
                    handle_msg_get_calls( l_calls );
                    break;

                case msg_type_t::TERMINATE_CALL:
                    handle_msg_terminate_call( reinterpret_cast<terminate_call_t*>( MSG_INPUT_DATA ), l_config, &l_calls, &l_users );
                    break;

                case msg_type_t::DELETE_USER:
                    handle_msg_delete_user( reinterpret_cast<delete_user_t*>( MSG_INPUT_DATA ), l_config, &l_calls, &l_users );
                    break;

                case msg_type_t::TIMER_EXPIRED:
                    handle_msg_timer_expired( reinterpret_cast<timer_msg_t*>( MSG_INPUT_DATA ), l_config, &l_calls, &l_users );
                    break;

                case msg_type_t::CONFIG_CHANGED:
                    handle_msg_config_changed( *config, &l_config );
                    break;

                case msg_type_t::RELOAD_CONF_USERS:
                    handle_msg_reload_conf_users( l_config, &l_calls, &l_users );
                    break;

                case msg_type_t::DIE:
                    handle_msg_die( l_config, &l_calls, &l_users );
                    break;

                default:
                    /* nothing to do */
                    break;
            }
        }
    }
    catch( const ExitException &e )
    {
        /* nothing to do */
    }

    unlink( l_config.users_db_save_path.c_str() );

    DEBUG( "Exiting thread, TID: %u", gettid() );
}
