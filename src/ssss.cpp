#include "ssss.h"
#include "common.h"
#include "logger.h"
#include "networking.h"
#include "cli.h"
#include "sip.h"
#include "messaging.h"
#include "sgn.h"
#include "timer.h"

#include <ifaddrs.h>
#include <iostream>

/***************************************/
/* FORWARD DECLARATION OF MSG HANDLERS */
/***************************************/
static void handle_msg_exit_requested( const config_t &config, process_data_t *process_data );

/***************************************/
/*** CLASS AND STRUCT IMPLEMENTATION ***/
/***************************************/
bool cmd_args_t::parse( const int argc, const char **argv )
{
    int i = 1;

    while( i < argc )
    {
        if( 0 == strcmp( "-d", argv[i] ) )
        {
            this->daemon = true;
        }
        else if( 0 == strcmp( "-c", argv[i] ) )
        {
            if( i + 1 < argc )
            {
                if( 0 == access( argv[i + 1], F_OK | R_OK ) )
                {
                    this->cfg_path = std::string( argv[i + 1] );
                    i++;
                }
                else
                {
                    std::cerr << KYEL << "Config file does not exist or unreadable!" << KNRM << NL_STR;
                    return false;
                }
            }
            else
            {
                std::cerr << KYEL << "Config file name not given!" << KNRM << NL_STR;
                return false;
            }
        }
        else if( 0 == strcmp( "-h", argv[i] )
              || 0 == strcmp( "--help", argv[i] ) )
        {
            this->help = true;
        }
        else if( 0 == strcmp( "-v", argv[i] ) )
        {
            this->version = true;
        }
        else
        {
            std::cerr << KYEL << "Unrecognized option: " << KNRM << argv[i] << NL_STR;
            return false;
        }

        i++;
    }

    return true;
}

/***************************************/
/************ FREE FUNCTIONS ***********/
/***************************************/
static void start_threads( config_t *config, process_data_t *process_data )
{
    if( config->tcp_enabled )
    {
        process_data->tcp_thread = std::thread( tcp, config );
    }

    if( config->udp_enabled )
    {
        process_data->udp_thread = std::thread( udp, config );
    }

    process_data->sip_thread = std::thread( sip, config );
    process_data->cli_thread = std::thread( cli, config );
    process_data->sgn_thread = std::thread( sgn );
    process_data->tmr_thread = std::thread( tmr );
}

static void stop_threads( void )
{
    MSG_OUTPUT_EMPTY( DIE, TMR );
    MSG_OUTPUT_EMPTY( DIE, CLI );
    MSG_OUTPUT_EMPTY( DIE, SIP );
    /* NW threads are stopped from SIP */
}

static void join_threads( const config_t &config, process_data_t *process_data )
{
    if( config.tcp_enabled )
    {
        process_data->tcp_thread.join();
    }

    if( config.udp_enabled )
    {
        process_data->udp_thread.join();
    }

    process_data->sip_thread.join();
    process_data->cli_thread.join();
    process_data->sgn_thread.join();
    process_data->tmr_thread.join();
}

static void terminator( const config_t &config, process_data_t *process_data )
{
    stop_threads();

    try
    {
        join_threads( config, process_data );
    }
    catch( std::system_error &e )
    {
        WARN( "Error during thread join: %s", e.what() );
    }
}

static bool get_server_ip_address( const std::string &interface, std::string *server_ip_address )
{
    struct ifaddrs *l_interfaces = nullptr;
    bool            l_found = false;

    if( -1 == getifaddrs( &l_interfaces ) )
    {
        ERROR( "Getting interfaces failed: %s", strerror( errno ) );
        return false;
    }

    struct ifaddrs *l_interface = l_interfaces;

    while( nullptr != l_interface )
    {
        if( nullptr != l_interface->ifa_addr
         && AF_INET == l_interface->ifa_addr->sa_family
         && l_interface->ifa_name == interface )
        {
            *server_ip_address = std::string( inet_ntoa( reinterpret_cast<struct sockaddr_in*>( l_interface->ifa_addr )->sin_addr ) );
            l_found = true;

            break;
        }

        l_interface = l_interface->ifa_next;
    }

    freeifaddrs( l_interfaces );

    return l_found;
}

static void print_help( void )
{
    constexpr const char l_help_str[] =
R"(          _____                    _____                    _____                    _____
         /\    \                  /\    \                  /\    \                  /\    \
        /::\    \                /::\    \                /::\    \                /::\    \
       /::::\    \              /::::\    \              /::::\    \              /::::\    \
      /::::::\    \            /::::::\    \            /::::::\    \            /::::::\    \
     /:::/\:::\    \          /:::/\:::\    \          /:::/\:::\    \          /:::/\:::\    \
    /:::/__\:::\    \        /:::/__\:::\    \        /:::/__\:::\    \        /:::/__\:::\    \
    \:::\   \:::\    \       \:::\   \:::\    \       \:::\   \:::\    \       \:::\   \:::\    \
  ___\:::\   \:::\    \    ___\:::\   \:::\    \    ___\:::\   \:::\    \    ___\:::\   \:::\    \
 /\   \:::\   \:::\    \  /\   \:::\   \:::\    \  /\   \:::\   \:::\    \  /\   \:::\   \:::\    \
/::\   \:::\   \:::\____\/::\   \:::\   \:::\____\/::\   \:::\   \:::\____\/::\   \:::\   \:::\____\
\:::\   \:::\   \::/    /\:::\   \:::\   \::/    /\:::\   \:::\   \::/    /\:::\   \:::\   \::/    /
 \:::\   \:::\   \/____/  \:::\   \:::\   \/____/  \:::\   \:::\   \/____/  \:::\   \:::\   \/____/
  \:::\   \:::\    \       \:::\   \:::\    \       \:::\   \:::\    \       \:::\   \:::\    \
   \:::\   \:::\____\       \:::\   \:::\____\       \:::\   \:::\____\       \:::\   \:::\____\
    \:::\  /:::/    /        \:::\  /:::/    /        \:::\  /:::/    /        \:::\  /:::/    /
     \:::\/:::/    /          \:::\/:::/    /          \:::\/:::/    /          \:::\/:::/    /
      \::::::/    /            \::::::/    /            \::::::/    /            \::::::/    /
       \::::/    /              \::::/    /              \::::/    /              \::::/    /
        \::/    /                \::/    /                \::/    /                \::/    /
         \/____/                  \/____/                  \/____/                  \/____/

Usage: ssss [-h] [-d] [-c <config file path>]

  Super Simple SIP Server
  This software does NOT conform to any RFC, use it at your own risk.

Example:
  ssss -d -c cfg/cfg.txt

Options:
  -h, --help    Print this help
  -d            Start as a daemon
  -c            Config file path
  -v            Version
)";

    std::cout << l_help_str;
}

static void print_version( void )
{
    std::cout << "ssss rev. " << GIT_REV << NL_STR;
}

/***************************************/
/****** MESSAGE HANDLER FUNCTIONS ******/
/***************************************/
static void handle_msg_exit_requested( const config_t &config, process_data_t *process_data )
{
    terminator( config, process_data );
    throw ExitException();
}

/***************************************/
/*** THREAD MAIN FUNCTION / EVENTLOOP **/
/***************************************/
int main( int argc, const char **argv )
{
    config_t       l_config;
    process_data_t l_process_data;
    cmd_args_t     l_cmd_args;

    Logger::set_tlid( "APP" );

    if( !l_cmd_args.parse( argc, argv ) )
    {
        print_help();
        return EXIT_FAILURE;
    }

    if( l_cmd_args.help )
    {
        print_help();
        return EXIT_SUCCESS;
    }

    if( l_cmd_args.version )
    {
        print_version();
        return EXIT_SUCCESS;
    }

    if( l_config.parse( l_cmd_args.cfg_path ) )
    {
        std::cout << KGRN << "Config successfully read from '" << l_cmd_args.cfg_path << "'" << KNRM << NL_STR;
    }

    srand( time( nullptr ) );

    Logger::set_loglevel( l_config.loglevel );
    Logger::set_log_path( l_config.log_path );

    if( !MsgBus::init() )
    {
        return EXIT_FAILURE;
    }

    MsgBus::register_thread( msg_t::endpoint_t::APP );

    if( !get_server_ip_address( l_config.interface, &l_config.static_data.server_ip_address ) )
    {
        ERROR( "Interface %s not found!", l_config.interface.c_str() );
        return EXIT_FAILURE;
    }

    std::cout << NL_STR << "Using config parameters:" << l_config.str();

    if( !bind_master_sockets( &l_config ) )
    {
        return EXIT_FAILURE;
    }

    if( l_cmd_args.daemon )
    {
        if( 0 == daemon( NOCHDIR, NOCLOSE ) )
        {
            std::cout << KGRN << "Daemon successfully started" << KNRM << NL_STR;
            Logger::set_no_color();
        }
        else
        {
            ERROR( "Unable to daemonize the process!" );
            return EXIT_FAILURE;
        }
    }

    try
    {
        start_threads( &l_config, &l_process_data );
    }
    catch( std::system_error &e )
    {
        ERROR( "Failed to start threads: %s", e.what() );
        return EXIT_FAILURE;
    }

    try
    {
        while( true )
        {
            DECL_MSG_INPUT_DATA();

            switch( MSG_INPUT() )
            {
                case msg_type_t::EXIT_REQUESTED:
                    g_exit_requested = true;
                    handle_msg_exit_requested( l_config, &l_process_data );
                    break;

                default:
                    /* nothing to do */
                    break;
            }
        }
    }
    catch( const ExitException &e )
    {
        /* nothing to do */
    }

    DEBUG( "Exiting process" );

    return EXIT_SUCCESS;
}
