#include "parser.h"
#include "logger.h"

/***************************************/
/***** STATIC AND GLOBAL VARIABLES *****/
/***************************************/
constexpr header_map_t HEADER_MAP[] =
{
    { header_type_t::VIA,               "Via",              sizeof( "Via" ) - 1            },
    { header_type_t::FROM,              "From",             sizeof( "From" ) - 1           },
    { header_type_t::TO,                "To",               sizeof( "To" ) - 1             },
    { header_type_t::CALL_ID,           "Call-ID",          sizeof( "Call-ID" ) - 1        },
    { header_type_t::CONTACT,           "Contact",          sizeof( "Contact" ) - 1        },
    { header_type_t::CONTENT_TYPE,      "Content-Type",     sizeof( "Content-Type" ) - 1   },
    { header_type_t::CSEQ,              "CSeq",             sizeof( "CSeq" ) - 1           },
    { header_type_t::RSEQ,              "RSeq",             sizeof( "RSeq" ) - 1           },
    { header_type_t::RACK,              "RAck",             sizeof( "RAck" ) - 1           },
    { header_type_t::SUPPORTED,         "Supported",        sizeof( "Supported" ) - 1      },
    { header_type_t::REQUIRE,           "Require",          sizeof( "Require" ) - 1        },
    { header_type_t::ALLOW,             "Allow",            sizeof( "Allow" ) - 1          },
    { header_type_t::CONTENT_LENGTH,    "Content-Length",   sizeof( "Content-Length" ) - 1 },
    { header_type_t::EXPIRES,           "Expires",          sizeof( "Expires" ) - 1        },
    { header_type_t::ROUTE,             "Route",            sizeof( "Route" ) - 1          },
    { header_type_t::RECORD_ROUTE,      "Record-Route",     sizeof( "Record-Route" ) - 1   },
    { header_type_t::AUTHORIZATION,     "Authorization",    sizeof( "Authorization" ) - 1  },
    { header_type_t::MAX_FORWARDS,      "Max-Forwards",     sizeof( "Max-Forwards" ) - 1   },
};

/***************************************/
/************ FREE FUNCTIONS ***********/
/***************************************/
template <typename T>
static T get_smaller_not_null( T a, T b )
{
    if( 0 == a )
    {
        return b;
    }
    else if( 0 == b )
    {
        return a;
    }
    else
    {
        return std::min( a, b );
    }
}

static const char* find_next_non_space( const char *line_ptr, const int line_len )
{
    for( int i = 0; i < line_len; i++ )
    {
        if( ' ' != line_ptr[i] )
        {
            return line_ptr + i;
        }
    }

    return nullptr;
}

static const char* find_header_data( const char *line_ptr, const int line_len )
{
    const char *l_ns_ptr = find_next_non_space( line_ptr, line_len );

    if( nullptr != l_ns_ptr )
    {
        if( ':' == *l_ns_ptr )
        {
            return find_next_non_space( l_ns_ptr + 1, line_len - ( l_ns_ptr - line_ptr ) - 1 );
        }
        else
        {
            DEBUG( "No colon in header: %.*s", line_len, line_ptr );
        }
    }

    return nullptr;
}

static int parse_next_line( const char *packet, const int packet_len, int *packet_iter, int *line_start )
{
    int         l_line_len = 0;
    const char *l_nl_ptr = static_cast<const char*>( memchr( packet + *packet_iter, '\n', packet_len - *packet_iter ) );

    *line_start = *packet_iter;

    if( *packet_iter >= packet_len
     || nullptr == l_nl_ptr )
    {
        return 0;
    }

    *packet_iter = l_nl_ptr - packet + 1;
    l_line_len = *packet_iter - *line_start - 1;

    if( '\r' == packet[*packet_iter - 2] )
    {
        l_line_len--;
    }

    return l_line_len;
}

static bool parse_token( const char *line_ptr, const int line_len, const is_quoted_t is_quoted, const char *token, std::string *output )
{
    const int   l_token_len = strlen( token );
    const char *l_token_ptr = static_cast<const char*>( memmem( line_ptr, line_len, token, l_token_len ) );
    int         l_line_elem_iter = 0;
    int         l_string_start = 0;

    if( nullptr != l_token_ptr )
    {
        l_line_elem_iter = l_token_ptr - line_ptr + l_token_len + static_cast<int>( is_quoted );
        l_string_start = l_line_elem_iter;

        while( l_line_elem_iter < line_len )
        {
            if( ( is_quoted
               && '"' == line_ptr[l_line_elem_iter] )
             || ( !is_quoted
               && ( ',' == line_ptr[l_line_elem_iter]
                 || ';' == line_ptr[l_line_elem_iter]
                 || ' ' == line_ptr[l_line_elem_iter]
                 || l_line_elem_iter + 1 == line_len ) ) )
            {
                output->assign( line_ptr + l_string_start, l_line_elem_iter - l_string_start + ( l_line_elem_iter + 1 == line_len ? ( is_quoted ? 0 : 1 ) : 0 ) );
                return true;
            }

            l_line_elem_iter++;
        }
    }

    return false;
}

static sip_method_t parse_method( const char* line_ptr )
{
    int l_request_method = sip_method_t::SIP_REGISTER;

    while( '\0' != sip_method_t_str[l_request_method][0] )
    {
        if( 0 == memcmp( line_ptr, sip_method_t_str[l_request_method], strlen( sip_method_t_str[l_request_method] ) ) )
        {
            return static_cast<sip_method_t>( l_request_method );
        }

        l_request_method++;
    }

    return sip_method_t::SIP_NOT_DEF;
}

static sip_status_t parse_response_code( const char *line_ptr )
{
    return static_cast<sip_status_t>( atoi( line_ptr + strlen( "SIP/2.0 " ) ) );
}

static void parse_numeric_header( const char *line_ptr, const int line_len, int *result )
{
    char l_temp_str[100] = {};

    memcpy( l_temp_str, line_ptr, std::min( static_cast<int>( sizeof( l_temp_str ) ) - 1, line_len ) );

    *result = atoi( l_temp_str );
}

static void parse_whole_header( const char *line_ptr, const int line_len, std::string *header )
{
    header->assign( line_ptr, line_len );
}

static void parse_cseq( const char *line_ptr, const int line_len, cseq_t *cseq )
{
    char *l_endptr = nullptr;
    char  l_temp_str[20] = {};

    cseq->num = strtol( line_ptr, &l_endptr, 10 );

    if( nullptr == l_endptr )
    {
        return;
    }

    memcpy( l_temp_str, l_endptr + 1, line_len - ( l_endptr - line_ptr + 1 ) );
    cseq->method = parse_method( l_temp_str );
}

static void parse_uri( const char *line_ptr, const int line_len, uri_t *uri )
{
    if( 0 == memcmp( line_ptr, "sip:", strlen( "sip:" ) ) )
    {
        uri->scheme = uri_scheme_t::SIP;
    }
    else if( 0 == memcmp( line_ptr, "tel:", strlen( "tel:" ) ) )
    {
        uri->scheme = uri_scheme_t::TEL;
    }
    else
    {
        DEBUG( "Unsupported scheme in uri: %.*s", line_len, line_ptr );
        return;
    }

    const char *l_param_ptr = nullptr;
    const char *l_port_ptr = nullptr;
    const char *l_end_ptr = nullptr;

    if( uri_scheme_t::SIP == uri->scheme )
    {
        const char *l_at_ptr = static_cast<const char*>( memchr( line_ptr, '@', line_len ) );

        if( nullptr != l_at_ptr )
        {
            l_param_ptr = static_cast<const char*>( memchr( line_ptr, ';', l_at_ptr - line_ptr ) );
            l_end_ptr = get_smaller_not_null( l_at_ptr, l_param_ptr );

            uri->name.assign( line_ptr + strlen( "sip:" ), l_end_ptr - line_ptr - strlen( "sip:" ) );

            l_param_ptr = static_cast<const char*>( memchr( l_at_ptr, ';', line_len ) );
            l_port_ptr = static_cast<const char*>( memchr( l_at_ptr, ':', line_len ) );
            l_end_ptr = get_smaller_not_null( line_ptr + line_len, get_smaller_not_null( l_param_ptr, l_port_ptr ) );

            uri->domain.assign( l_at_ptr + 1, l_end_ptr - l_at_ptr - 1 );

            if( nullptr != l_port_ptr )
            {
                uri->port = atoi( l_port_ptr + 1 );
            }
        }
        else
        {
            l_param_ptr = static_cast<const char*>( memchr( line_ptr + strlen( "sip:" ), ';', line_len ) );
            l_port_ptr = static_cast<const char*>( memchr( line_ptr + strlen( "sip:" ), ':', line_len ) );
            l_end_ptr = get_smaller_not_null( line_ptr + line_len, get_smaller_not_null( l_param_ptr, l_port_ptr ) );

            uri->domain.assign( line_ptr + strlen( "sip:" ), l_end_ptr - line_ptr - strlen( "sip:" ) );

            if( nullptr != l_port_ptr )
            {
                uri->port = atoi( l_port_ptr + 1 );
            }
        }
    }
    else if( uri_scheme_t::TEL == uri->scheme )
    {
        l_param_ptr = static_cast<const char*>( memchr( line_ptr + strlen( "tel:" ), ';', line_len ) );
        l_end_ptr = get_smaller_not_null( line_ptr + line_len, l_param_ptr );

        uri->name.assign( line_ptr + strlen( "sip:" ), l_end_ptr - line_ptr - strlen( "tel:" ) );
    }
}

static void parse_nameaddr_w_tag( const char *line_ptr, const int line_len, uri_t *uri )
{
    const bool  l_only_uri = ( 0 == memcmp( line_ptr, "sip:", strlen( "sip:" ) ) || 0 == memcmp( line_ptr, "sip:", strlen( "sip:" ) ) );
    const char *l_uri_start_ptr = nullptr;
    int         l_uri_length = 0;

    if( l_only_uri )
    {
        l_uri_start_ptr = line_ptr;
        l_uri_length = get_smaller_not_null( line_ptr + line_len, static_cast<const char*>( memchr( line_ptr, ';', line_len ) ) ) - line_ptr;
    }
    else
    {
        if( '<' != *line_ptr )
        {
            parse_token( line_ptr, line_len, static_cast<is_quoted_t>( '"' == *line_ptr ), "", &uri->display_name );
        }

        const int l_dn_length = uri->display_name.length() + 2 * ( '"' == *line_ptr );

        const char *l_start_brk_ptr = static_cast<const char*>( memchr( line_ptr + l_dn_length, '<', line_len - l_dn_length ) );
        const char *l_close_brk_ptr = static_cast<const char*>( memchr( line_ptr + l_dn_length, '>', line_len - l_dn_length ) );

        if( nullptr == l_start_brk_ptr
         || nullptr == l_close_brk_ptr )
        {
            DEBUG( "Missing < or > in header: %.*s", line_len, line_ptr );
            return;
        }

        l_uri_start_ptr = l_start_brk_ptr + 1;
        l_uri_length = l_close_brk_ptr - l_start_brk_ptr - 1;
    }

    parse_uri( l_uri_start_ptr, l_uri_length, uri );

    const char *l_tag_ptr = static_cast<const char*>( memmem( l_uri_start_ptr + l_uri_length, line_len - ( l_uri_start_ptr - line_ptr ) - l_uri_length, "tag=", strlen( "tag=" ) ) );

    if( nullptr != l_tag_ptr )
    {
        parse_token( l_tag_ptr, line_len - ( l_tag_ptr - line_ptr ), is_quoted_t::NO, "tag=", &uri->tag );
    }
}

static void parse_supp_req( const char *line_ptr, const int line_len, supp_req_t *supp_req )
{
    if( nullptr != memmem( line_ptr, line_len, "100rel", strlen( "100rel" ) ) )
    {
        supp_req->rel100 = true;
    }
}

static void parse_allow( const char *line_ptr, const int line_len, allow_t *allow )
{
    if( nullptr != memmem( line_ptr, line_len, "PRACK", strlen( "PRACK" ) ) )
    {
        allow->prack = true;
    }

    if( nullptr != memmem( line_ptr, line_len, "UPDATE", strlen( "UPDATE" ) ) )
    {
        allow->update = true;
    }
}

static void parse_authorization( const char *line_ptr, const int line_len, authorization_t *authorization )
{
    parse_token( line_ptr, line_len, is_quoted_t::YES, "nonce=",    &authorization->nonce );
    parse_token( line_ptr, line_len, is_quoted_t::YES, "uri=",      &authorization->uri );
    parse_token( line_ptr, line_len, is_quoted_t::NO,  "nc=",       &authorization->nc );
    parse_token( line_ptr, line_len, is_quoted_t::YES, "cnonce=",   &authorization->cnonce );
    parse_token( line_ptr, line_len, is_quoted_t::YES, "response=", &authorization->response );
    parse_token( line_ptr, line_len, is_quoted_t::YES, "opaque=",   &authorization->opaque );
}

static void parse_via( const char *line_ptr, const int line_len, vias_t *vias )
{
    via_t l_via;

    parse_whole_header( line_ptr, line_len, &l_via.full );
    vias->push_back( l_via );
}

static void parse_route( const char *line_ptr, const int line_len, routes_t *routes )
{
    route_t l_route;

    parse_whole_header( line_ptr, line_len, &l_route.full );
    routes->push_back( l_route );
}

static void parse_startline( const char *line_ptr, const int line_len, sip_method_t *method, sip_status_t *response_code, uri_t *ruri )
{
    if( 0 == memcmp( line_ptr, "SIP", strlen( "SIP" ) ) )
    {
        *response_code = parse_response_code( line_ptr );
    }
    else if( sip_method_t::SIP_NOT_DEF != ( *method = parse_method( line_ptr ) ) )
    {
        const char *l_ruri_start = find_next_non_space( line_ptr + strlen( sip_method_t_str[*method] ), line_len - strlen( sip_method_t_str[*method] ) );
        const char *l_ruri_end = static_cast<const char*>( memchr( l_ruri_start, ' ', line_len - ( l_ruri_start - line_ptr ) ) );

        parse_uri( l_ruri_start, l_ruri_end - l_ruri_start, ruri );
    }
    else
    {
        DEBUG( "Neither response nor request: %.*s", line_len, line_ptr );
    }
}

static void parse_payload( const char *packet, const int packet_len, const int content_len, const int hint, char *payload )
{
    const int l_content_len = std::min( content_len, packet_len - hint );

    memcpy( payload, packet + hint, l_content_len );
    payload[l_content_len] = '\0';
}

static void locate_headers( const packet_t &packet_from_queue, const int start, std::vector<header_t> *header_list )
{
    int l_packet_iter = start;
    int l_line_len = 0;
    int l_line_start = 0;

    while( 0 != ( l_line_len = parse_next_line( packet_from_queue.data, packet_from_queue.len, &l_packet_iter, &l_line_start ) ) )
    {
        const char    *l_line_ptr = packet_from_queue.data + l_line_start;
        header_type_t  l_header_type = header_type_t::NOT_DEF;
        int            l_name_len = 0;

        if( start == l_line_start )
        {
            l_header_type = header_type_t::STARTLINE;
            l_name_len = 0;
        }
        else
        {
            for( int i = 0; i < static_cast<int>( header_type_t::SIZE ) - static_cast<int>( header_type_t::VIA ); i++ )
            {
                if( l_line_len > HEADER_MAP[i].len + 1
                 && 0 == memcmp( l_line_ptr, HEADER_MAP[i].str, HEADER_MAP[i].len )
                 && ( ' ' == l_line_ptr[HEADER_MAP[i].len]
                   || ':' == l_line_ptr[HEADER_MAP[i].len] ) )
                {
                    l_header_type = HEADER_MAP[i].type;
                    l_name_len = HEADER_MAP[i].len;
                }
            }
        }

        if( header_type_t::NOT_DEF != l_header_type
         && l_name_len + 1 < l_line_len )
        {
            const char *l_header_data_ptr = l_line_ptr;

            if( header_type_t::STARTLINE == l_header_type
             || nullptr != ( l_header_data_ptr = find_header_data( l_line_ptr + l_name_len, l_line_len - l_name_len ) ) )
            {
                header_list->emplace_back( l_header_type, l_header_data_ptr - packet_from_queue.data, l_line_len - ( l_header_data_ptr - l_line_ptr ) );
            }
        }
    }
}

static void parse_headers( const char *packet, const std::vector<header_t> &header_list, message_t *received_message )
{
    for( const header_t &i_header : header_list )
    {
        const char *l_line_ptr = packet + i_header.start;
        const int   l_line_len = i_header.len;

        switch( i_header.type )
        {
            case header_type_t::STARTLINE:
                parse_startline( l_line_ptr, l_line_len, &received_message->method, &received_message->response_code, &received_message->ruri );
                break;

            case header_type_t::VIA:
                parse_via( l_line_ptr, l_line_len, &received_message->vias );
                break;

            case header_type_t::FROM:
                parse_nameaddr_w_tag( l_line_ptr, l_line_len, &received_message->from );
                break;

            case header_type_t::TO:
                parse_nameaddr_w_tag( l_line_ptr, l_line_len, &received_message->to );
                break;

            case header_type_t::CALL_ID:
                parse_whole_header( l_line_ptr, l_line_len, &received_message->call_id );
                break;

            case header_type_t::CONTACT:
                parse_nameaddr_w_tag( l_line_ptr, l_line_len, &received_message->contact );
                break;

            case header_type_t::CONTENT_TYPE:
                parse_whole_header( l_line_ptr, l_line_len, &received_message->content.type );
                break;

            case header_type_t::CSEQ:
                parse_cseq( l_line_ptr, l_line_len, &received_message->cseq );
                break;

            case header_type_t::RSEQ:
                parse_whole_header( l_line_ptr, l_line_len, &received_message->rseq );
                break;

            case header_type_t::RACK:
                parse_whole_header( l_line_ptr, l_line_len, &received_message->rack );
                break;

            case header_type_t::SUPPORTED:
                parse_supp_req( l_line_ptr, l_line_len, &received_message->supported );
                break;

            case header_type_t::REQUIRE:
                parse_supp_req( l_line_ptr, l_line_len, &received_message->require );
                break;

            case header_type_t::ALLOW:
                parse_allow( l_line_ptr, l_line_len, &received_message->allow );
                break;

            case header_type_t::CONTENT_LENGTH:
                parse_numeric_header( l_line_ptr, l_line_len, &received_message->content.len );
                break;

            case header_type_t::EXPIRES:
                parse_numeric_header( l_line_ptr, l_line_len, &received_message->expires );
                break;

            case header_type_t::ROUTE:
                parse_route( l_line_ptr, l_line_len, &received_message->routes );
                break;

            case header_type_t::RECORD_ROUTE:
                parse_route( l_line_ptr, l_line_len, &received_message->record_routes );
                break;

            case header_type_t::AUTHORIZATION:
                parse_authorization( l_line_ptr, l_line_len, &received_message->authorization );
                break;

            case header_type_t::MAX_FORWARDS:
                parse_numeric_header( l_line_ptr, l_line_len, &received_message->max_forwards );
                break;

            default:
                break;
        }
    }
}

static void locate_message_sections( const packet_t &packet, std::vector<int> *sections )
{
    const char *l_section_ptr_old = packet.data;
    const char *l_section_ptr_act = nullptr;
    int         l_section_offset = 0;

    while( nullptr != ( l_section_ptr_act = static_cast<const char*>( memmem( l_section_ptr_old, packet.len - l_section_offset, "\r\n\r\n", strlen( "\r\n\r\n" ) ) ) ) )
    {
        l_section_offset = l_section_ptr_act - packet.data + strlen( "\r\n\r\n" );

        if( packet.len <= l_section_offset )
        {
            break;
        }

        sections->push_back( l_section_offset );
        l_section_ptr_old = l_section_ptr_act + strlen( "\r\n\r\n" );
    }
}

void parse_packet( const packet_t &packet_from_queue, std::vector<message_t> *received_messages )
{
    int              l_next_message_start = 0;
    int              l_section_idx = 0;
    std::vector<int> l_sections;

    TRACE( KMAG, "%s", packet_from_queue.data );

    locate_message_sections( packet_from_queue, &l_sections );

    while( l_next_message_start < packet_from_queue.len )
    {
        std::vector<header_t> l_header_list;
        message_t             l_received_message;

        l_header_list.reserve( 100 );

        l_received_message.init();
        l_received_message.nw_data = packet_from_queue.nw_data;

        locate_headers( packet_from_queue, l_next_message_start, &l_header_list );

        if( l_header_list.empty() )
        {
            DEBUG( "Malformed SIP message received with length %dB", packet_from_queue.len );
            return;
        }

        parse_headers( packet_from_queue.data, l_header_list, &l_received_message );

        try
        {
            if( 0 != l_received_message.content.len )
            {
                parse_payload( packet_from_queue.data, packet_from_queue.len, l_received_message.content.len, l_sections.at( l_section_idx++ ), l_received_message.content.payload );
            }

            received_messages->push_back( l_received_message );
            l_next_message_start = l_sections.at( l_section_idx++ );
        }
        catch( const std::out_of_range &e )
        {
            break;
        }
    }
}
