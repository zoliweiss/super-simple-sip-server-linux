#include "common.h"
#include "c_plus_plus_serializer.h"

/***************************************/
/*** CLASS AND STRUCT IMPLEMENTATION ***/
/***************************************/
std::string cseq_t::str( void ) const
{
    std::string l_out_str;

    l_out_str.reserve( 20 );
    l_out_str += std::to_string( num ) + " " + sip_method_t_str[method];

    return l_out_str;
}

content_t::content_t( void )
{
    init();
}

std::string network_t::ip_str( void ) const
{
    if( AF_UNSPEC != ip_data.sin_family )
    {
        return std::string( inet_ntoa( ip_data.sin_addr ) );
    }

    return "-";
}

std::string network_t::port_str( void ) const
{
    if( 0 != ip_data.sin_port )
    {
        return std::to_string( ntohs( ip_data.sin_port ) );
    }

    return "-";
}

std::string network_t::socket_str( void ) const
{
    if( 0 != socket )
    {
        return std::to_string( socket );
    }

    return "-";
}

std::string network_t::str( void ) const
{
    std::string l_out;

    if( protocol_t::PROTO_NOT_DEF != protocol
     && AF_UNSPEC != ip_data.sin_family )
    {
        l_out = std::string( inet_ntoa( ip_data.sin_addr ) ) + ":" + std::to_string( ntohs( ip_data.sin_port ) );

        if( protocol_t::UDP == protocol )
        {
            l_out += " UDP";
        }
        else
        {
            l_out += " TCP socket " + std::to_string( socket );
        }
    }
    else
    {
        l_out = "-";
    }

    return l_out;
}

std::string utctime_t::get_iso8601_str( void ) const
{
    if( !valid )
    {
        return "-";
    }

    char          l_timebuf[36] = {};
    constexpr int l_datetime_len = 19; /* 2022-12-26T06:56:10 */

    strftime( l_timebuf, sizeof( l_timebuf ), "%FT%TZ", gmtime( &sec ) );
    snprintf( l_timebuf + l_datetime_len, sizeof( l_timebuf ) - l_datetime_len, ".%3.3ldZ", nsec / 1'000'000 );

    return std::string( l_timebuf );
}

std::string utctime_t::get_elapsed_str( void ) const
{
    if( !valid )
    {
        return "-";
    }

    long int    l_time_diff_in_sec = static_cast<long int>( difftime( time( nullptr ), sec ) );
    long int    l_hours = l_time_diff_in_sec / 3600;
    long int    l_minutes = ( l_time_diff_in_sec % 3600 ) / 60;
    long int    l_seconds = l_time_diff_in_sec % 60;
    std::string l_out_str;

    l_out_str.reserve( 30 );
    l_out_str = ( 0 == l_hours                   ? "" : std::to_string( l_hours )   + "h " )
              + ( 0 == l_hours && 0 == l_minutes ? "" : std::to_string( l_minutes ) + "m " )
              + (                                       std::to_string( l_seconds ) + "s"  );

    return l_out_str;
}

void utctime_t::set( const time_t sec )
{
    this->set( sec, 0 );
}

void utctime_t::set( const time_t sec, const int nsec )
{
    this->sec = sec;
    this->nsec = nsec;
    this->valid = true;
}

void utctime_t::set_actual( void )
{
    struct timespec l_rawtime;
    clock_gettime( CLOCK_REALTIME, &l_rawtime );

    sec = l_rawtime.tv_sec;
    nsec = l_rawtime.tv_nsec;
    valid = true;
}

std::string call_t::str( void ) const
{
    std::string l_out_str;
    int         l_idx = 0;

    l_out_str.reserve( 1000 );

    l_out_str += "\n";
    l_out_str += "  Call state .......... " + std::string( call_state_t_str[state] ) + "\n";
    l_out_str += "  Call ID ............. " + call_id + "\n";
    l_out_str += "  A user .............. " + a_username + "\n";
    l_out_str += "  B user .............. " + b_username + "\n";
    l_out_str += "  INVITE Vias ......... ";

    for( const auto &i_via : invite_vias )
    {
        if( 0 < l_idx )
        {
            l_out_str += "\n                        ";
        }

        l_out_str += i_via.full;
        l_idx++;
    }

    l_out_str += "\n";

    l_out_str += "  INVITE CSeq ......... " + invite_cseq.str() + "\n";
    l_out_str += "  INVITE From tag ..... " + invite_from_tag + "\n";
    l_out_str += "  INVITE To tag ....... " + invite_to_tag + "\n";
    l_out_str += "  Last req From tag ... " + last_request_from_tag + "\n";
    l_out_str += "  Last req To tag ..... " + last_request_to_tag + "\n";
    l_out_str += "  Call started ........ " + time_call_started.get_iso8601_str() + "\n";
    l_out_str += "    Duration .......... " + time_call_started.get_elapsed_str() + "\n";
    l_out_str += "\n";

    return l_out_str;
}

std::string call_t::get_csv_line( void ) const
{
    std::string l_csv;

    l_csv.reserve( 200 );

    l_csv += "\"" + call_id + "\",";
    l_csv += "\"" + a_username + "\",";
    l_csv += "\"" + b_username + "\"";

    return l_csv;
}

bool message_t::is_in_dialog( const call_t &call ) const
{
    if( this->is_request() )
    {
        if( sip_method_t::SIP_CANCEL == this->method )
        {
            if( this->from.tag == call.invite_from_tag )
            {
                return true;
            }
        }
        else
        {
            if( ( this->from.name == call.b_username
               && this->to.tag == call.invite_from_tag
               && this->from.tag == call.invite_to_tag )
             || ( this->from.name == call.a_username
               && this->from.tag == call.invite_from_tag
               && this->to.tag == call.invite_to_tag ) )
            {
                return true;
            }
        }
    }
    else if( this->from.tag == call.last_request_from_tag
          && ( call_state_t::PRE_ALERTING == call.state
            || this->to.tag == call.last_request_to_tag ) )
    {
        return true;
    }

    return false;
}

std::string message_t::get_csv_line( void ) const
{
    std::string l_csv;

    l_csv.reserve( 200 );

    if( sip_method_t::SIP_REGISTER == method )
    {
        l_csv += "\"" + from.name + "\",";
        l_csv += "\"\",";                             /* auth_name is preconfigured */
        l_csv += "\"\",";                             /* display_name is preconfigured */
        l_csv += "\"" + nw_data.ip_str() + "\",";
        l_csv += "\"" + nw_data.port_str() + "\",";
        l_csv += "\"" + nw_data.socket_str() + "\",";
        l_csv += "\"" + std::string( protocol_t_str[nw_data.protocol] ) + "\"";
    }
    else if( sip_method_t::SIP_INVITE == method
          || sip_method_t::SIP_BYE == method )
    {
        l_csv += "\"" + call_id + "\",";
        l_csv += "\"" + from.name + "\",";
        l_csv += "\"" + to.name + "\"";
    }

    return l_csv;
}

std::string user_t::str( void ) const
{
    std::string l_out_str;

    l_out_str.reserve( 1000 );

    l_out_str += "\n";
    l_out_str += "  Name ................ " + name + "\n";
    l_out_str += "  Auth name ........... " + std::string( auth_name.empty() ? "-" : auth_name ) + "\n";
    l_out_str += "  Display name ........ " + std::string( display_name.empty() ? "-" : display_name ) + "\n";
    l_out_str += "  Network ............. " + nw_data.str() + "\n";
    l_out_str += "  Call ID ............. " + std::string( call_id.empty() ? "-" : call_id ) + "\n";
    l_out_str += "  Password ............ " + std::string( password.empty() ? "-" : password ) + "\n";
    l_out_str += "  Registered .......... " + std::string( registered ? "YES" : "NO" ) + "\n";
    l_out_str += "    Initreg time ...... " + time_initial_reg.get_iso8601_str() + "\n";
    l_out_str += "    Last rereg time ... " + time_re_reg.get_iso8601_str() + "\n";
    l_out_str += "    Last dereg time ... " + time_de_reg.get_iso8601_str() + "\n";
    l_out_str += "\n";

    return l_out_str;
}

std::string user_t::get_csv_line( void ) const
{
    std::string l_csv;

    l_csv.reserve( 200 );

    l_csv += "\"" + name + "\",";
    l_csv += "\"" + auth_name + "\",";
    l_csv += "\"" + display_name + "\",";
    l_csv += "\"" + nw_data.ip_str() + "\",";
    l_csv += "\"" + nw_data.port_str() + "\",";
    l_csv += "\"" + nw_data.socket_str() + "\",";
    l_csv += "\"" + std::string( protocol_t_str[nw_data.protocol] ) + "\"";

    return l_csv;
}

packet_t::packet_t( void )
{
    init();
}

std::string config_t::clean_line( const std::string &line )
{
    std::string l_clean_line;
    size_t      l_clean_line_start = line.find_first_not_of(" ");
    size_t      l_clean_line_end = line.find_first_of("#");

    if( std::string::npos == l_clean_line_start )
    {
        l_clean_line_start = line.length();
    }

    if( std::string::npos == l_clean_line_end )
    {
        l_clean_line_end = line.length();
    }

    if( 0 != l_clean_line_end - l_clean_line_start )
    {
        /* remove space before inline comments */
        while( ' ' == line[l_clean_line_end - 1] )
        {
            l_clean_line_end--;
        }

        l_clean_line.assign( line, l_clean_line_start, l_clean_line_end - l_clean_line_start );
    }

    return l_clean_line;
}

bool config_t::parse_param( const std::string &param, std::string *key, std::string *value )
{
    size_t l_separator_pos = 0;

    if( std::string::npos == ( l_separator_pos = param.find( '=' ) ) )
    {
        return false;
    }

    key->assign( param, 0, l_separator_pos );
    value->assign( param, l_separator_pos + 1, param.length() - l_separator_pos - 1 );

    trim_ws( *key );
    trim_ws( *value );
    trim_dq( *value );

    return true;
}

void config_t::parse_general_params( const std::vector<std::string> &general_params )
{
    std::string l_key;
    std::string l_value;
    int         l_temp_int = 0;

    for( const std::string &i_param : general_params )
    {
        if( parse_param( i_param, &l_key, &l_value ) )
        {
            if( "interface" == l_key )
            {
                this->interface = l_value;
            }
            else if( "port" == l_key )
            {
                if( string_to_int( l_value, &l_temp_int ) )
                {
                    this->server_port = l_temp_int;
                }
            }
            else if( "force_auth" == l_key )
            {
                this->force_auth = ( "true" == l_value );
            }
            else if( "loglevel" == l_key )
            {
                this->loglevel = Logger::from_str( l_value );
            }
            if( "log_path" == l_key )
            {
                this->log_path = l_value;
            }
            else if( "protocols" == l_key )
            {
                bool l_tcp_enabled = false;
                bool l_udp_enabled = false;

                if( std::string::npos != l_value.find( "tcp" ) )
                {
                    l_tcp_enabled = true;
                }
                if( std::string::npos != l_value.find( "udp" ) )
                {
                    l_udp_enabled = true;
                }

                /*
                 * Possible scenarios coming from the config file:
                 *   UDP off | TCP off -> enable both, handled by config_t constructor, no action needed
                 *   UDP off | TCP on  -> enable only TCP
                 *   UDP on  | TCP off -> enable only UDP
                 *   UDP on  | TCP on  -> enable both, handled by config_t constructor, no action needed
                 */
                if( l_tcp_enabled != l_udp_enabled )
                {
                    this->udp_enabled = l_udp_enabled;
                    this->tcp_enabled = l_tcp_enabled;
                }
            }
            else if( "cli_sock_path" == l_key )
            {
                this->cli_sock_path = l_value;
            }
            else if( "users_db_save_path" == l_key )
            {
                this->users_db_save_path = l_value;
            }
            else if( "user_event_log_path" == l_key )
            {
                this->user_event_log_path = l_value;
            }
            else if( "call_event_log_path" == l_key )
            {
                this->call_event_log_path = l_value;
            }
            else if( "max_call_duration" == l_key )
            {
                if( string_to_int( l_value, &l_temp_int ) )
                {
                    this->max_call_duration = l_temp_int;
                }
            }
        }
    }
}

bool config_t::parse_user( const std::vector<std::string> &user_params, user_t *user )
{
    std::string l_key;
    std::string l_value;

    for( const std::string &i_user_param : user_params )
    {
        if( parse_param( i_user_param, &l_key, &l_value ) )
        {
            if( "username" == l_key )
            {
                user->name = l_value;
            }
            else if( "auth" == l_key )
            {
                user->auth_name = l_value;
            }
            else if( "password" == l_key )
            {
                user->password = l_value;
            }
            else if( "display_name" == l_key )
            {
                user->display_name = l_value;
            }
        }
    }

    return !user->name.empty()
        && !user->auth_name.empty()
        && !user->password.empty();
}

void config_t::parse_users( const std::vector<std::string> &users_lines )
{
    std::vector<std::string> l_user_params;
    user_t                   l_user;

    for( const std::string &i_users_line : users_lines )
    {
        if( "[USER]" == i_users_line )
        {
            l_user_params.clear();
        }
        else if( "[ENDUSER]" == i_users_line )
        {
            if( parse_user( l_user_params, &l_user ) )
            {
                configured_users.push_back( l_user );
            }
        }
        else
        {
            l_user_params.push_back( i_users_line );
        }
    }
}

bool config_t::parse( void )
{
    return this->parse( this->cfg_path );
}

bool config_t::parse( const std::string &cfg_path )
{
    std::string              l_line;
    std::vector<std::string> l_general_params;
    std::vector<std::string> l_users;
    bool                     l_general_section = false;
    bool                     l_users_section = false;

    std::ifstream l_config_file( cfg_path );

    if( l_config_file.is_open() )
    {
        this->cfg_path = cfg_path;

        while( getline( l_config_file, l_line ) )
        {
            const std::string l_clean_line = clean_line( l_line );

            if( 0 != l_clean_line.length() )
            {
                if( "[GENERAL]" == l_clean_line )
                {
                    l_general_section = true;
                }
                else if( "[ENDGENERAL]" == l_clean_line )
                {
                    l_general_section = false;
                }
                else if( "[USERS]" == l_clean_line )
                {
                    l_users_section = true;
                }
                else if( "[ENDUSERS]" == l_clean_line )
                {
                    l_users_section = false;
                }
                else
                {
                    if( l_general_section )
                    {
                        l_general_params.push_back( l_clean_line );
                    }
                    else if( l_users_section )
                    {
                        l_users.push_back( l_clean_line );
                    }
                }
            }
        }

        parse_general_params( l_general_params );
        parse_users( l_users );

        l_config_file.close();
    }
    else
    {
        WARN( "Config file '%s' not found!", cfg_path.c_str() );

        return false;
    }

    return true;
}

void config_t::remove_configured_users( void )
{
    this->configured_users.clear();
    this->configured_users.shrink_to_fit();
}

std::string config_t::str( void ) const
{
    std::string l_out_str;

    l_out_str.reserve(1000);

    l_out_str += "\n";
    l_out_str += "  Server IP address ........... " + this->static_data.server_ip_address + "\n";
    l_out_str += "  Server port ................. " + std::to_string( this->server_port ) + "\n";
    l_out_str += "  Enabled protocols ........... ";

    if( this->udp_enabled )
    {
        l_out_str += "UDP";
    }

    if( this->udp_enabled
     && this->tcp_enabled )
    {
        l_out_str += ", ";
    }

    if( this->tcp_enabled )
    {
        l_out_str += "TCP";
    }

    l_out_str += "\n";
    l_out_str += "  Network interface ........... " + this->interface + "\n";
    l_out_str += "  Force authorization ......... " + std::string( this->force_auth ? "YES" : "NO" ) + "\n";
    l_out_str += "  Loglevel .................... " + std::string( Logger::get_loglevel_str() ) + "\n";
    l_out_str += "  Log path .................... " + this->log_path + "\n";
    l_out_str += "  CLI socket path ............. " + this->cli_sock_path + "\n";
    l_out_str += "  Users DB save path .......... " + std::string( this->users_db_save_path.empty() ? "-" : this->users_db_save_path ) + "\n";
    l_out_str += "  User event log path ......... " + std::string( this->user_event_log_path.empty() ? "-" : this->user_event_log_path ) + "\n";
    l_out_str += "  Call event log path ......... " + std::string( this->call_event_log_path.empty() ? "-" : this->call_event_log_path ) + "\n";
    l_out_str += "  Maximum call duration ....... " + std::to_string( this->max_call_duration ) + "\n";
    l_out_str += "\n";

    return l_out_str;
}

std::string users_t::str( void ) const
{
    std::string l_out_str;
    char        l_tmp_str[100];

    l_out_str.reserve( 1000 );

                /*| user_10000 |  user_10000 |  255.255.255.255 |  65535 |  65535 |   UDP |        YES |  YES |*/
    l_out_str += "+-------------------------------------------------------------------------------------------+\n";
    l_out_str += "|    Name    |  Auth name  |    IP address    |  Port  | Socket | Proto | Registered | Busy |\n";
    l_out_str += "+-------------------------------------------------------------------------------------------+\n";

    for( const auto &[i_name, i_user] : *this )
    {
        snprintf( l_tmp_str, sizeof( l_tmp_str ),
            "| %10.10s | %11.11s | %16.16s | %6.6s | %6.6s | %5.5s | %10.10s | %4.4s |\n",
            i_name.c_str(),
            i_user.auth_name.c_str(),
            i_user.nw_data.ip_str().c_str(),
            i_user.nw_data.port_str().c_str(),
            i_user.nw_data.socket_str().c_str(),
            protocol_t_str[i_user.nw_data.protocol],
            i_user.registered ? "YES" : "NO",
            i_user.call_id.empty() ? "NO" : "YES" );

        l_out_str += l_tmp_str;
    }

    l_out_str += "+-------------------------------------------------------------------------------------------+\n";
    l_out_str += "| TOTAL OF " + std::to_string( this->size() ) + " ENTRIES\n";
    l_out_str += "+-------------------------------------------------------------------------------------------+\n";

    return l_out_str;
}

user_t* users_t::get_user_by_username( const std::string &username )
{
    auto search = this->find( username );

    if( search != this->end() )
    {
        return &search->second;
    }

  return nullptr;
}

user_t* users_t::get_user_by_nw_data( const network_t &nw_data )
{
    for( auto &[i_name, i_user] : *this )
    {
        if( i_user.nw_data == nw_data )
        {
            return &i_user;
        }
    }

    return nullptr;
}

int users_t::get_number_of_registered_users( void ) const
{
    int l_count = 0;

    for( const auto &[i_name, i_user] : *this )
    {
        if( i_user.registered )
        {
            l_count++;
        }
    }

    return l_count;
}

sip_status_t users_t::register_user( const message_t &received_message )
{
    user_t        l_new_user;
    user_t       *l_existing_user = nullptr;
    sip_status_t  l_status = sip_status_t::SIP_STATUS_NOT_DEF;

    if( nullptr != ( l_existing_user = this->get_user_by_username( received_message.from.name ) ) )
    {
        l_existing_user->nw_data = received_message.nw_data;

        if( l_existing_user->registered )
        {
            l_existing_user->time_re_reg.set_actual();
            this->event_log.add_event( event_t::USER_RE_REG, *l_existing_user );
            INFO( "User %s reregistered, number of users: %d", received_message.from.name.c_str(), this->get_number_of_registered_users() );
        }
        else
        {
            l_existing_user->registered = true;
            l_existing_user->time_initial_reg.set_actual();
            this->event_log.add_event( event_t::USER_INIT_REG_EXIST, *l_existing_user );
            INFO( "User %s registered, number of users: %d", received_message.from.name.c_str(), this->get_number_of_registered_users() );
        }

        l_status = sip_status_t::SIP_OK;
    }
    else
    {
        l_new_user.name = received_message.from.name;
        l_new_user.nw_data = received_message.nw_data;
        l_new_user.registered = true;
        l_new_user.time_initial_reg.set_actual();

        try
        {
            this->insert( {received_message.from.name, l_new_user} );
            this->event_log.add_event( event_t::USER_INIT_REG, l_new_user );
            INFO( "User %s added, number of users: %d", received_message.from.name.c_str(), this->get_number_of_registered_users() );

            l_status = sip_status_t::SIP_OK;
        }
        catch( const std::bad_alloc& )
        {
            this->event_log.add_event( event_t::USER_ERROR, l_new_user );
            WARN( "Unable to add user %s", received_message.from.name.c_str() );

            l_status = sip_status_t::SIP_SERVICE_UNAVAIL;
        }
    }

    DUMP_USERS( *this );
    this->save();

    return l_status;
}

sip_status_t users_t::deregister_user( const message_t &received_message )
{
    user_t       *l_user_ptr = this->get_user_by_username( received_message.from.name );
    sip_status_t  l_status = sip_status_t::SIP_STATUS_NOT_DEF;

    if( nullptr != l_user_ptr )
    {
        l_user_ptr->deregister();

        this->save();
        this->event_log.add_event( event_t::USER_DE_REG, *l_user_ptr );

        INFO( "User %s deregistered, number of users: %d", received_message.from.name.c_str(), this->get_number_of_registered_users() );
        DUMP_USERS( *this );

        l_status = sip_status_t::SIP_OK;
    }
    else
    {
        this->event_log.add_event( event_t::USER_NOT_FOUND, received_message );

        l_status = sip_status_t::SIP_NOT_FOUND;
    }

    return l_status;
}

void users_t::deregister_user_by_nw_data( const network_t &nw_data )
{
    for( auto &[i_name, i_user] : *this )
    {
        if( i_user.nw_data == nw_data
         && i_user.registered )
        {
            i_user.clear_auth();
            i_user.deregister();

            this->event_log.add_event( event_t::USER_DISCONNECT, i_user );

            INFO( "User %s deregistered by disconnect, number of users: %d", i_name.c_str(), this->get_number_of_registered_users() );
            DUMP_USERS( *this );

            break;
        }
    }

    this->save();
}

user_t* users_t::get_other_party( const message_t &received_message )
{
    if( received_message.is_request() )
    {
        return this->get_user_by_username( received_message.ruri.name );
    }
    else
    {
        return this->get_user_by_username( received_message.from.name );
    }
}

bool users_t::add_call( const std::string &a_username, const std::string &b_username, const std::string &call_id )
{
    user_t *l_a_ptr = this->get_user_by_username( a_username );
    user_t *l_b_ptr = this->get_user_by_username( b_username );

    /* conditions are OR-ed to ensure atomicity */
    if( nullptr == l_a_ptr
     || nullptr == l_b_ptr )
    {
        return false;
    }

    l_a_ptr->call_id = call_id;
    l_b_ptr->call_id = call_id;

    return true;
}

void users_t::remove_call( const call_t &call )
{
    user_t *l_a_ptr = this->get_user_by_username( call.a_username );
    user_t *l_b_ptr = this->get_user_by_username( call.b_username );

    /* remove separately to ensure proper clean-up */
    if( nullptr != l_a_ptr )
    {
        l_a_ptr->call_id.clear();
    }

    if( nullptr != l_b_ptr )
    {
        l_b_ptr->call_id.clear();
    }
}

void users_t::remove_user( const std::string &username )
{
    INFO( "User %s removed, number of users: %zu", username.c_str(), this->size() - 1 );
    this->erase( username );
    DUMP_USERS( *this );

    this->save();
}

void users_t::serialize( std::stringstream *users ) const
{
    *users << bits( this->size() );

    for( auto &[i_name, i_user] : *this )
    {
        *users << bits( i_user.name )
               << bits( i_user.auth_name )
               << bits( i_user.display_name )
               << bits( i_user.nw_data )
               << bits( i_user.call_id )
               << bits( i_user.password )
               << bits( i_user.registered )
               << bits( i_user.time_initial_reg )
               << bits( i_user.time_re_reg )
               << bits( i_user.time_de_reg );
    }
}

void users_t::deserialize( std::stringstream *users )
{
    size_t l_size = 0;

    *users >> bits( l_size );

    while( 0 < l_size
        && users->good() )
    {
        user_t l_user;

        *users >> bits( l_user.name )
               >> bits( l_user.auth_name )
               >> bits( l_user.display_name )
               >> bits( l_user.nw_data )
               >> bits( l_user.call_id )

               /*
                * Although password is "sensitive", it is needed for restoration
                *
                * Consider the scenario, when a user tries to re-register after restoration,
                * but we don't have the password. This will then fail if forced authorization
                * is enforced.
                *
                * The other option to handle this would be to merge
                * the configured users with the restored ones
                */
               >> bits( l_user.password )
               >> bits( l_user.registered )
               >> bits( l_user.time_initial_reg )
               >> bits( l_user.time_re_reg )
               >> bits( l_user.time_de_reg );

        this->insert( {l_user.name, l_user} );
        l_size--;
    }
}

void users_t::save( void ) const
{
    if( users_db_save_path.empty() )
    {
        return;
    }

    std::ofstream l_users_db_file( users_db_save_path, std::ios::binary );

    if( !l_users_db_file.is_open() )
    {
        ERROR( "Unable to save user DB!" );

        return;
    }

    std::stringstream l_users_db;

    this->serialize( &l_users_db );

    l_users_db_file << l_users_db.rdbuf();
    l_users_db_file.close();
}

bool users_t::restore( void )
{
    std::ifstream     l_users_db_file( users_db_save_path );
    std::stringstream l_users_db;

    if( !l_users_db_file.is_open() )
    {
        return false;
    }

    l_users_db << l_users_db_file.rdbuf();
    l_users_db_file.close();

    this->deserialize( &l_users_db );

    for( auto &[i_name, i_user] : *this )
    {
        /*
         * After restoration the original TCP connections are dead, so
         * we deregister the user to prevent the server from assuming
         * the user is live.
         */
        if( protocol_t::TCP == i_user.nw_data.protocol
         && i_user.registered )
        {
            i_user.deregister();
        }
    }

    return true;
}

bool users_t::merge_configured_users( const std::vector<user_t> &configured_users )
{
    for( const user_t &i_user : configured_users )
    {
        user_t *l_user_ptr = get_user_by_username( i_user.name );

        if( nullptr != l_user_ptr )
        {
            /* update fields */
            l_user_ptr->auth_name    = i_user.auth_name;
            l_user_ptr->password     = i_user.password;
            l_user_ptr->display_name = i_user.display_name;
        }
        else
        {
            try
            {
                this->insert( {i_user.name, i_user} );
            }
            catch( const std::bad_alloc& )
            {
                WARN( "Unable to save configured user: %s", i_user.name.c_str() );
                return false;
            }
        }
    }

    return true;
}

void users_t::set_users_db_save_path( const std::string &users_db_save_path )
{
    this->users_db_save_path = users_db_save_path;
}

std::string calls_t::str( void ) const
{
    std::string l_out_str;
    char        l_tmp_str[100];

    l_out_str.reserve( 1000 );

                /*| call-id-1234567890-abcdefghijkl | user_10000 | user_20000 | CANCEL PENDING | 1000h 59m 59s |*/
    l_out_str += "+--------------------------------------------------------------------------------------------+\n";
    l_out_str += "|             Call ID             |   A user   |   B user   |   Call state   | Call duration |\n";
    l_out_str += "+--------------------------------------------------------------------------------------------+\n";

    for( const auto &[i_call_id, i_call] : *this )
    {
        char l_call_id[32];
        string_to_char_arr( i_call_id, sizeof( l_call_id ), l_call_id );

        if( i_call_id.length() > 31 )
        {
            l_call_id[29] = ' ';
            l_call_id[30] = '$';
        }

        snprintf( l_tmp_str, sizeof( l_tmp_str ),
            "| %31.31s | %10.10s | %10.10s | %14.14s | %13.13s |\n",
            l_call_id,
            i_call.a_username.c_str(),
            i_call.b_username.c_str(),
            call_state_t_str[i_call.state],
            i_call.time_call_started.get_elapsed_str().c_str() );

        l_out_str += l_tmp_str;
    }

    l_out_str += "+--------------------------------------------------------------------------------------------+\n";
    l_out_str += "| TOTAL OF " + std::to_string( this->size() ) + " ENTRIES\n";
    l_out_str += "+--------------------------------------------------------------------------------------------+\n";

    return l_out_str;
}

void calls_t::remove_call( const std::string &call_id )
{
    INFO( "Call %s removed, number of calls: %zu", call_id.c_str(), this->size() - 1 );
    this->erase( call_id );
    DUMP_CALLS( *this );
}

call_t* calls_t::add_call( const message_t &received_message )
{
    call_t l_call;

    l_call.state = call_state_t::PRE_ALERTING;
    l_call.a_username = received_message.from.name;
    l_call.b_username = received_message.to.name;
    l_call.invite_from_tag = received_message.from.tag;
    l_call.invite_to_tag = received_message.to.tag;
    l_call.call_id = received_message.call_id;
    l_call.invite_vias = received_message.vias;
    l_call.invite_cseq = received_message.cseq;

    try
    {
        auto l_result = this->insert( {received_message.call_id, l_call} );
        DUMP_CALLS( *this );

        return &l_result.first->second;
    }
    catch( const std::bad_alloc& )
    {
        WARN( "Unable to add call %s", received_message.call_id.c_str() );
        return nullptr;
    }
}

call_t* calls_t::get_call_by_call_id( const std::string &call_id )
{
    auto l_search = this->find( call_id );

    if( l_search != this->end() )
    {
        return &l_search->second;
    }

    return nullptr;
}

call_t* calls_t::get_call_by_partial_call_id( const std::string &partial_call_id )
{
    if( partial_call_id.empty() )
    {
        return nullptr;
    }

    for( auto &[i_call_id, i_call] : *this )
    {
        if( std::string::npos != i_call_id.find( partial_call_id ) )
        {
            return &i_call;
        }
    }

    return nullptr;
}

call_t* calls_t::get_call_by_username( users_t &users, const std::string &username )
{
    const user_t *l_user_ptr = users.get_user_by_username( username );

    if( nullptr == l_user_ptr )
    {
        return nullptr;
    }

    return this->get_call_by_call_id( l_user_ptr->call_id );
}

void calls_t::serialize( std::stringstream *calls ) const
{
    *calls << bits( this->size() );

    for( auto &[i_call_id, i_call] : *this )
    {
        *calls << bits( i_call.state )
               << bits( i_call.call_id );

        *calls << bits( i_call.invite_vias.size() );

        for( auto &invite_via : i_call.invite_vias )
        {
            *calls << bits( invite_via.full );
        }

        *calls << bits( i_call.invite_cseq )
               << bits( i_call.invite_from_tag )
               << bits( i_call.invite_to_tag )
               << bits( i_call.a_username )
               << bits( i_call.b_username )
               << bits( i_call.last_request_from_tag )
               << bits( i_call.last_request_to_tag )
               << bits( i_call.time_call_started );
    }
}

void calls_t::deserialize( std::stringstream *calls )
{
    size_t l_size = 0;

    *calls >> bits( l_size );

    while( 0 < l_size
        && calls->good() )
    {
        call_t l_call;
        size_t l_invite_vias_length;

        *calls >> bits( l_call.state )
               >> bits( l_call.call_id );

        *calls >> bits( l_invite_vias_length );

        for( size_t i = 0; i < l_invite_vias_length; i++ )
        {
            via_t l_invite_via;

            *calls >> bits( l_invite_via.full );

            l_call.invite_vias.push_back( l_invite_via );
        }

        *calls >> bits( l_call.invite_cseq )
               >> bits( l_call.invite_from_tag )
               >> bits( l_call.invite_to_tag )
               >> bits( l_call.a_username )
               >> bits( l_call.b_username )
               >> bits( l_call.last_request_from_tag )
               >> bits( l_call.last_request_to_tag )
               >> bits( l_call.time_call_started );

        this->insert( {l_call.call_id, l_call} );
        l_size--;
    }
}

void event_log_t::open( const std::string &event_log_path )
{
    if( event_log_path.empty() )
    {
        return;
    }

    if( nullptr == ( fp = fopen( event_log_path.c_str(), "a" ) ) )
    {
        ERROR( "Unable to open event log file '%s'", event_log_path.c_str() );
    }
}

/***************************************/
/************ FREE FUNCTIONS ***********/
/***************************************/
static void ltrim_ws( std::string &s )
{
    s.erase( s.begin(), std::find_if( s.begin(), s.end(), []( char ch )
        {
            return !std::isspace( ch );
        }
        ) );
}

static void rtrim_ws( std::string &s )
{
    s.erase( std::find_if( s.rbegin(), s.rend(), []( char ch )
        {
            return !std::isspace( ch );
        }
        ).base(), s.end() );
}

void trim_ws( std::string &s )
{
    rtrim_ws( s );
    ltrim_ws( s );
}

static void ltrim_dq( std::string &s )
{
    s.erase( s.begin(), std::find_if( s.begin(), s.end(), []( char ch )
        {
            return '"' != ch;
        }
        ) );
}

static void rtrim_dq( std::string &s )
{
    s.erase( std::find_if( s.rbegin(), s.rend(), []( char ch )
        {
            return '"' != ch;
        }
        ).base(), s.end() );
}

void trim_dq( std::string &s )
{
    rtrim_dq( s );
    ltrim_dq( s );
}

size_t string_to_char_arr( const std::string &in, const size_t out_maxlen, char *out )
{
    const size_t l_maxlen = std::min( out_maxlen - 1, in.length() );

    memcpy( out, in.data(), l_maxlen );
    out[l_maxlen] = '\0';

    return l_maxlen;
}

bool string_to_int( const std::string &in, int *out )
{
    try
    {
        *out = std::stoi( in );
        return true;
    }
    catch( const std::exception &e )
    {
        return false;
    }
}

std::string base64_encode( const char *src, const int len )
{
    constexpr char  l_base64_table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
    const char     *l_end_ptr = src + len;
    const char     *l_curr_ptr = src;
    std::string     l_out;

    l_out.reserve( len * 4 / 3 + 4 );

    while( 3 <= l_end_ptr - l_curr_ptr )
    {
        l_out += l_base64_table[l_curr_ptr[0] >> 2];
        l_out += l_base64_table[( ( l_curr_ptr[0] & 0x03 ) << 4 ) | ( l_curr_ptr[1] >> 4 )];
        l_out += l_base64_table[( ( l_curr_ptr[1] & 0x0f ) << 2 ) | ( l_curr_ptr[2] >> 6 )];
        l_out += l_base64_table[l_curr_ptr[2] & 0x3f];

        l_curr_ptr += 3;
    }

    if( 0 != l_end_ptr - l_curr_ptr )
    {
        l_out += l_base64_table[l_curr_ptr[0] >> 2];

        if( 1 == l_end_ptr - l_curr_ptr )
        {
            l_out += l_base64_table[( l_curr_ptr[0] & 0x03 ) << 4];
            l_out += '=';
        }
        else
        {
            l_out += l_base64_table[( ( l_curr_ptr[0] & 0x03 ) << 4 ) | ( l_curr_ptr[1] >> 4 )];
            l_out += l_base64_table[( l_curr_ptr[1] & 0x0f ) << 2];
        }

        l_out += '=';
    }

    return l_out;
}
