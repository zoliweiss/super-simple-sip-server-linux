#include "cli.h"
#include "logger.h"
#include "messaging.h"
#include "timer.h"

#include <sstream>

/***************************************/
/* FORWARD DECLARATION OF MSG HANDLERS */
/***************************************/
static void handle_msg_get_users_resp( const gen_buffer_t *buff, users_t *users );
static void handle_msg_get_calls_resp( const gen_buffer_t *buff, calls_t *calls );
static void handle_msg_get_tcp_network_data_resp( const cli_tcp_nw_data_t* in, cli_tcp_nw_data_t *out );
static void handle_msg_terminate_call_resp( const  terminate_call_resp_t *terminate_call_resp, Terminal *terminal );
static void handle_msg_delete_user_resp_resp( const  delete_user_resp_t *delete_user_resp, Terminal *terminal );
static void handle_msg_die( Terminal *terminal );
static void handle_msg_timer_expired( void );

/***************************************/
/***** STATIC AND GLOBAL VARIABLES *****/
/***************************************/
std::string CLI_TREE = R"({
    show:{
        calls:[
            SHOW_CALLS,
            ?:"  Shows info about the calls. If no Call-ID is given, then the list of users
  is dumped. The Call-ID can be a partial match.

  Syntax:
    show calls [Call-ID]"
        ],
        users:[
            SHOW_USERS,
            ?:"  Shows info about the users. If no User-ID is given, then the list of users
  is dumped. The supplied User ID must be an exact match.

  Syntax:
    show users [User-ID]"
        ],
        info:[
            SHOW_INFO,
            ?:"  Shows info about the server."
        ]
    },
    delete:{
        calls:[
            DELETE_CALLS,
            ?:"  Terminates calls. If no Call-ID is given, then all calls are terminated.
  The Call-ID can be a partial match.

  If the '--force' parameter is supplied, then the server does not wait for the clients
  response and deletes the call from the database immediately.

  Syntax:
    delete calls [Call-ID] [--force]"
        ],
        users:[
        DELETE_USERS,
            ?:"  Deletes users. If no User ID is given, then all users are deleted.
  The supplied User ID must be an exact match

  Syntax:
    delete users [User-ID]"
        ]
    },
    set:{
        loglevel:{
            error:SET_LOG_ERROR,
            warn:SET_LOG_WARN,
            info:SET_LOG_INFO,
            debug:SET_LOG_DEBUG,
            trace:SET_LOG_TRACE,
            ?:"  Modifies the loglevel."
        },
        logpath:[
            SET_LOG_PATH,
            ?:"  Modifies the log path."
        ],
        forceauth:{
            on:SET_FORCE_AUTH_ON,
            off:SET_FORCE_AUTH_OFF,
            ?:"  Turns the force authorization on or off."
        },
        maxcallduration:[
            SET_MAX_CALL_DURATION,
            ?:"  Sets the maximum call duration in seconds.
  The change doesn't affect already established calls."
        ]
    },
    reload:{
        configusers:[
            RELOAD_CONFIGUSERS,
            ?:"  Reloads the configured users from the config file."
        ]
    },
    exit:[
        EXIT,
        ?:"  Terminates the server and the calls."
    ]
})";

/***************************************/
/*** CLASS AND STRUCT IMPLEMENTATION ***/
/***************************************/
elems_t::elems_t( std::string &cli_str ) : elems_t( flatten_str( cli_str ).c_str() )
{
}

elems_t::elems_t( const char *cli_str )
{
    parse( &cli_str );
}

elems_t::elems_t( const char **cli_str )
{
    parse( cli_str );
}

void elems_t::parse( const char **cli_str )
{
    const char *l_temp_id_start = nullptr;
    elems_t     l_temp_elems;
    bool        l_is_help = false;
    bool        l_is_ep_help = false;

    while( true )
    {
        l_temp_elems.init();

        advance_one_char( cli_str );
        l_temp_id_start = *cli_str;
        find_next( cli_str, ':' );
        l_temp_elems.id.assign( l_temp_id_start, *cli_str - l_temp_id_start );

        if( "?" == l_temp_elems.id )
        {
            l_is_help = true;
        }

        advance_one_char( cli_str );

        if( is_endpoint_w_help_start( cli_str ) )
        {
            l_is_ep_help = true;
            advance_one_char( cli_str );
        }

        if( is_child_start( cli_str ) )
        {
            l_temp_elems.child = std::make_unique<elems_t>( cli_str );
            this->push_back( std::move( l_temp_elems ) );

            find_next( cli_str, '}' );
            advance_one_char( cli_str );

            if( is_last_elem( cli_str ) )
            {
                return;
            }
        }
        else
        {
            if( l_is_help )
            {
                this->help = get_help_str( cli_str );
                return;
            }
            else
            {
                l_temp_elems.endpoint = get_endpoint_str( cli_str );

                if( l_is_ep_help )
                {
                    l_temp_elems.help = get_help_str( cli_str );
                    advance_one_char( cli_str );
                }

                this->push_back( std::move( l_temp_elems ) );

                if( is_last_elem( cli_str ) )
                {
                    return;
                }
            }
        }
    }
}

std::ostream &operator <<( std::ostream& os, const elems_t &elems )
{
    os << "ELEMS HELP:" << elems.help << NL_STR;

    for( const auto &elem : elems )
    {
        os << "ID:" << elem.id << " ENDPOINT:" << elem.endpoint << " CHILD:" << ( elem.child ? "YES" : "NO" ) << " HELP:" << elem.help << NL_STR;

        if( elem.child )
        {
            os << NL_STR;
            os << "---- CHILDREN OF " << elem.id << " ----" << NL_STR;
            os << *elem.child;
            os << "---- END OF CHILDREN ----" << NL_STR << NL_STR;
        }
    }

    return os;
}

History::History( void )
{
    this->push_back( "" );
}

void History::add( const std::string &input )
{
    const bool l_add = !( this->back() == input || ( this->size() > 1 && (*this)[this->size() - 2] == input ) );

    if( l_add )
    {
        this->pop_back();
        this->push_back( input );
        this->push_back( "" );

        this->history_idx = this->size() - 1;
    }
}

std::string History::move( const direction_t direction )
{
    size_t l_max_history = this->size() - 1;

    if( direction_t::UP == direction )
    {
        0 < this->history_idx ? this->history_idx-- : this->history_idx = 0;
    }
    else
    {
        l_max_history > this->history_idx ? this->history_idx++ : this->history_idx = l_max_history;
    }

    return (*this)[this->history_idx];
}

Terminal::Terminal( const std::string &socket_path )
{
    if( -1 != ( this->sock_desc = socket( PF_UNIX, SOCK_DGRAM, 0 ) ) )
    {
        memset( &this->addr, 0, sizeof( this->addr ) );

        this->addr.sun_family = AF_UNIX;
        string_to_char_arr( socket_path, sizeof( this->addr.sun_path ), this->addr.sun_path );
        unlink( socket_path.c_str() );

        if( -1 == bind( this->sock_desc, reinterpret_cast<struct sockaddr *>( &this->addr ), sizeof( this->addr ) ) )
        {
            close_socket();
        }
    }

    if( !is_socket_open() )
    {
        ERROR( "Unable to open CLI socket: %s", strerror( errno ) );
        throw ExitException();
    }
}

Terminal::~Terminal( void )
{
    this->close_socket();
}

char Terminal::get_char( void )
{
    char l_return = NUL;

    if( is_socket_open()
     && !this->buf.good() )
    {
        char l_buf[BUFF_SIZE] = {};
        this->fromlen = sizeof( this->from );

        if( 0 < recvfrom( this->sock_desc, l_buf, sizeof( l_buf ) - 1, 0, reinterpret_cast<struct sockaddr *>( &this->from ), &this->fromlen ) )
        {
            this->buf.clear();
            this->buf << l_buf;
        }
    }

    if( this->buf.good() )
    {
        this->buf.get( l_return );
    }

    return l_return;
}

int Terminal::get_symbol( void )
{
    char l_return = this->get_char();

    switch( l_return )
    {
        case BS:
        case DEL:
            l_return = BS;

            break;

        case LF:
        case CR:
            l_return = LF;

            break;

        case TAB:
            l_return = TAB;

            break;

        case ESC:
            if( '[' == this->get_char() )
            {
                switch( this->get_char() )
                {
                    case 'A':
                        l_return = ARR_UP;

                        break;

                    case 'B':
                        l_return = ARR_DOWN;

                        break;
                }
            }

            break;

        default:
            if( !is_allowed( l_return ) )
            {
                l_return = NUL;
            }

            break;
    }

    return l_return;
}

void Terminal::delete_back_n_chars( const int n )
{
    for( int i = 0; i < n; i++ )
    {
        *this << BS_STR;
    }

    *this << CLEAR_REST_OF_LINE;
}

template <class T>
Terminal & Terminal::operator <<( const T &t )
{
    std::stringstream l_ss;

    l_ss << t;

    if( is_socket_open()
     && 0 != this->fromlen
     && 0 != l_ss.str().length() )
    {
        if( -1 == sendto( this->sock_desc, l_ss.str().c_str(), l_ss.str().length(), 0, reinterpret_cast<struct sockaddr *>( &this->from ), this->fromlen ) )
        {
            WARN( "Error during CLI write: %s", strerror( errno ) );
        }
    }

    return *this;
}

template <>
Terminal &Terminal::operator<< <int>(const int &t)
{
    *this << static_cast<char>( t );

    return *this;
}

template <class T>
Terminal & Terminal::operator >>( T &t )
{
    if( is_socket_open() )
    {
        char l_buf[BUFF_SIZE] = {};
        int  l_buflen = 0;

        if( -1 != ( l_buflen = recvfrom( this->sock_desc, l_buf, sizeof( l_buf ), 0, reinterpret_cast<struct sockaddr *>( &this->from ), &this->fromlen ) ) )
        {
            l_buf[l_buflen - 1] = NUL;
            t << l_buf;
        }
        else
        {
            ERROR( "Error during CLI read: %s", strerror( errno ) );
        }
    }

    return *this;
}

/***************************************/
/************ FREE FUNCTIONS ***********/
/***************************************/
static void wait_peer_resp( Terminal *terminal )
{
    DECL_MSG_INPUT_DATA();

    while( true )
    {
        switch( MSG_INPUT() )
        {
            case msg_type_t::TERMINATE_CALL_RESP:
                handle_msg_terminate_call_resp( reinterpret_cast<terminate_call_resp_t*>( MSG_INPUT_DATA ), terminal );
                return;

            case msg_type_t::DELETE_USER_RESP:
                handle_msg_delete_user_resp_resp( reinterpret_cast<delete_user_resp_t*>( MSG_INPUT_DATA ), terminal );
                return;

            case msg_type_t::TIMER_EXPIRED:
                /* nothing to do */
                return;

            default:
                SAVE_MSG();
                break;
        }
    }
}

static void execute( const std::string endpoint, std::list<std::string> &params, Terminal *terminal, config_t *config, const cli_tcp_nw_data_t &tcp, calls_t &calls, users_t &users )
{
    bool l_force = false;

    for( auto i = params.begin(); i != params.end(); i++ )
    {
        if( "--force" == *i )
        {
            l_force = true;
            params.erase( i );

            break;
        }
    }

    const std::string l_param = params.empty() ? "" : params.front();

    *terminal << NL_STR;

    if( "SHOW_CALLS" == endpoint )
    {
        if( params.empty() )
        {
            *terminal << calls.str();
        }
        else
        {
            call_t *l_call_ptr;

            if( nullptr == ( l_call_ptr = calls.get_call_by_partial_call_id( l_param ) ) )
            {
                *terminal << NL_STR << KYEL << "  No such call!" << KNRM << NL_STR << NL_STR;
            }
            else
            {
                *terminal << l_call_ptr->str();
            }
        }
    }
    else if( "SHOW_USERS" == endpoint )
    {
        if( params.empty() )
        {
            *terminal << users.str();
        }
        else
        {
            user_t *l_user_ptr;

            if( nullptr == ( l_user_ptr = users.get_user_by_username( l_param ) ) )
            {
                *terminal << NL_STR << KYEL << "  No such user!" << KNRM << NL_STR << NL_STR;
            }
            else
            {
                *terminal << l_user_ptr->str();
            }
        }
    }
    else if( "SHOW_INFO" == endpoint )
    {
        int l_call_states[6] = {};

        for( const auto &[i_call_id, i_call] : calls )
        {
            l_call_states[i_call.state]++;
        }

        /* config is only read here */
        std::shared_lock l_lock( config->mtx );

        *terminal << NL_STR;
        *terminal << "  Total number of users ....... " << users.size() << NL_STR;
        *terminal << "    Registered users .......... " << users.get_number_of_registered_users() << NL_STR;

        *terminal << NL_STR;
        *terminal << "  Number of calls ............. " << calls.size() << NL_STR;
        *terminal << "    Pre alerting .............. " << l_call_states[call_state_t::PRE_ALERTING] << NL_STR;
        *terminal << "    Alerting .................. " << l_call_states[call_state_t::ALERTING] << NL_STR;
        *terminal << "    Active .................... " << l_call_states[call_state_t::ACTIVE] << NL_STR;
        *terminal << "    Cancel pending ............ " << l_call_states[call_state_t::CANCEL_PENDING] << NL_STR;
        *terminal << "    Clear pending ............. " << l_call_states[call_state_t::CLEAR_PENDING] << NL_STR;

        *terminal << NL_STR;
        *terminal << "  Server TCP socket ........... " << ( -1 == config->static_data.master_sockets.tcp ? "-" : std::to_string( config->static_data.master_sockets.tcp ) ) << NL_STR;
        *terminal << "  Server UDP socket ........... " << ( -1 == config->static_data.master_sockets.udp ? "-" : std::to_string( config->static_data.master_sockets.udp ) ) << NL_STR;

        *terminal << NL_STR;
        *terminal << "  Number of TCP connections ... " << tcp.num_tcp_clients << NL_STR;

        *terminal << config->str();
    }
    else if( "DELETE_CALLS" == endpoint
          || "DELETE_USERS" == endpoint )
    {
        if( "DELETE_CALLS" == endpoint )
        {
            terminate_call_t l_terminate_call = {};

            l_terminate_call.force = l_force;
            string_to_char_arr( l_param, sizeof( l_terminate_call.call_id ), l_terminate_call.call_id );

            MSG_OUTPUT( TERMINATE_CALL, SIP, l_terminate_call );
        }

        if( "DELETE_USERS" == endpoint )
        {
            delete_user_t l_delete_user = {};

            string_to_char_arr( l_param, sizeof( l_delete_user.user_id ), l_delete_user.user_id );

            MSG_OUTPUT( DELETE_USER, SIP, l_delete_user );
        }

        SET_TIMER_EMPTY( CLI_TIMEOUT, 500 );
        wait_peer_resp( terminal );
        RESET_TIMER_EMPTY( CLI_TIMEOUT );
    }
    else if( "SET_LOG_ERROR" == endpoint )
    {
        Logger::set_loglevel( loglevel_t::ERROR );
    }
    else if( "SET_LOG_WARN" == endpoint )
    {
        Logger::set_loglevel( loglevel_t::WARN );
    }
    else if( "SET_LOG_INFO" == endpoint )
    {
        Logger::set_loglevel( loglevel_t::INFO );
    }
    else if( "SET_LOG_DEBUG" == endpoint )
    {
        Logger::set_loglevel( loglevel_t::DEBUG );
    }
    else if( "SET_LOG_TRACE" == endpoint )
    {
        Logger::set_loglevel( loglevel_t::TRACE );
    }
    else if( "SET_LOG_PATH" == endpoint )
    {
        Logger::set_log_path( l_param );
    }
    else if( "SET_FORCE_AUTH_ON" == endpoint
          || "SET_FORCE_AUTH_OFF" == endpoint
          || "SET_MAX_CALL_DURATION" == endpoint )
    {
        int l_temp_int = 0;
        std::unique_lock l_lock( config->mtx );

        if( "SET_FORCE_AUTH_ON" == endpoint )
        {
            config->force_auth = true;
        }
        else if( "SET_FORCE_AUTH_OFF" == endpoint )
        {
            config->force_auth = false;
        }
        else if( "SET_MAX_CALL_DURATION" == endpoint
              && string_to_int( l_param, &l_temp_int ) )
        {
            config->max_call_duration = l_temp_int;
        }

        MSG_OUTPUT_EMPTY( CONFIG_CHANGED, SIP );
    }
    else if( "RELOAD_CONFIGUSERS" == endpoint )
    {
        MSG_OUTPUT_EMPTY( RELOAD_CONF_USERS, SIP );
    }
    else if( "EXIT" == endpoint )
    {
        MSG_OUTPUT_EMPTY( EXIT_REQUESTED, APP );
    }
}

static void split_input( const std::string &input, std::list<std::string> *commands )
{
    std::string       l_segment;
    std::stringstream l_input( input );

    while( getline( l_input, l_segment, ' ' ) )
    {
        if(!l_segment.empty())
        {
            commands->push_back( l_segment );
        }
    }
}

static std::string_view get_cli_help( const std::string &help )
{
    constexpr std::string_view l_default_str = "  No help for this command!";

    if( !help.empty() )
    {
        return help;
    }

    return l_default_str;
}

static void autocomplete( const elems_t &cli_elems, const bool exec_mode, Terminal *terminal, std::string *input, std::string *endpoint, std::list<std::string> *params )
{
    std::list<std::string>    l_commands;
    size_t                    l_idx = 0;
    std::vector<std::string>  l_matches;
    const elems_t            *l_curr_cli_child_elems_ptr = &cli_elems;
    const elems_t            *l_curr_cli_elems_ptr = &cli_elems;

    split_input( *input, &l_commands );
    endpoint->clear();

    for( const auto &i_command : l_commands )
    {
        if( "?" == i_command )
        {
            *terminal << NL_STR << NL_STR << KGRY;
            *terminal << ( l_curr_cli_child_elems_ptr->help.empty() ? get_cli_help( l_curr_cli_elems_ptr->help ) : get_cli_help( l_curr_cli_child_elems_ptr->help ) );
            *terminal << KNRM << NL_STR << NL_STR << PROMPT << *input;

            endpoint->clear();
            return;
        }

        if( !endpoint->empty() )
        {
            break;
        }

        l_idx++;
        l_matches.clear();

        for( const auto &i_cli_elem : *l_curr_cli_elems_ptr )
        {
            if( 0 == i_cli_elem.id.find( i_command ) )
            {
                l_curr_cli_child_elems_ptr = &i_cli_elem;
                l_matches.push_back( i_cli_elem.id );
            }
        }

        if( 0 == l_matches.size() )
        {
            *terminal << NL_STR << NL_STR << KYEL << "  Invalid command: " << KNRM << i_command << NL_STR << NL_STR << PROMPT << *input;

            return;
        }
        else if( 1 == l_matches.size() )
        {
            if( l_curr_cli_child_elems_ptr->child )
            {
                l_curr_cli_elems_ptr = l_curr_cli_child_elems_ptr->child.get();
            }
            else
            {
                *endpoint = l_curr_cli_child_elems_ptr->endpoint;
            }

            if( l_idx == l_commands.size()
             && ' ' != (*input)[input->length() - 1] )
            {
                *terminal << l_curr_cli_child_elems_ptr->id.substr( i_command.length(), std::string::npos ).append( " " );
                input->append( l_curr_cli_child_elems_ptr->id.substr( i_command.length(), std::string::npos ) ).append( " " );

                return;
            }
        }
        else
        {
            *terminal << NL_STR << NL_STR << KYEL << "  Ambigous command: " << KNRM << i_command << NL_STR << NL_STR << PROMPT << *input;
        }
    }

    if( endpoint->empty() )
    {
        if( 0 == l_matches.size()
         || 1 == l_matches.size() )
        {
            if( 1 == l_curr_cli_elems_ptr->size() )
            {
                *terminal << l_curr_cli_elems_ptr->at( 0 ).id << " ";
                input->append( l_curr_cli_elems_ptr->at( 0 ).id ).append( " " );
            }
            else
            {
                *terminal << NL_STR;

                for( const auto &i_cli_elem : *l_curr_cli_elems_ptr )
                {
                    *terminal << NL_STR << KCYN << "  * " << KNRM << i_cli_elem.id;
                }

                *terminal << NL_STR << NL_STR << PROMPT << *input;
            }
        }
        else
        {
            *terminal << NL_STR;

            for( const auto &i_match : l_matches )
            {
                *terminal << NL_STR << KCYN << "  * " << KNRM << i_match;
            }

            *terminal << NL_STR << NL_STR << PROMPT << *input;
        }
    }
    else
    {
        if( !exec_mode )
        {
            *terminal << NL_STR << NL_STR << KCYN << "  End of command chain reached" << KNRM << NL_STR << NL_STR << PROMPT << *input;
        }

        *params = l_commands;

        for( size_t i = 0; i < l_idx; i++ )
        {
            params->pop_front();
        }
    }
}

static void get_process_data( calls_t *calls, users_t *users, cli_tcp_nw_data_t *tcp )
{
    MSG_OUTPUT_EMPTY( GET_USERS, SIP );
    MSG_OUTPUT_EMPTY( GET_CALLS, SIP );
    MSG_OUTPUT_EMPTY( GET_TCP_NETWORK_DATA, TCP );

    SET_TIMER_EMPTY( CLI_TIMEOUT, 500 );

    int l_responses = 0;

    while( l_responses < 3 )
    {
        DECL_MSG_INPUT_DATA();

        switch( MSG_INPUT() )
        {
            case msg_type_t::GET_USERS_RESP:
                handle_msg_get_users_resp( reinterpret_cast<gen_buffer_t*>( MSG_INPUT_DATA ), users );
                l_responses++;
                break;

            case msg_type_t::GET_CALLS_RESP:
                handle_msg_get_calls_resp( reinterpret_cast<gen_buffer_t*>( MSG_INPUT_DATA ), calls );
                l_responses++;
                break;

            case msg_type_t::GET_TCP_NETWORK_DATA_RESP:
                handle_msg_get_tcp_network_data_resp( reinterpret_cast<cli_tcp_nw_data_t*>( MSG_INPUT_DATA ), tcp );
                l_responses++;
                break;

            case msg_type_t::TIMER_EXPIRED:
                handle_msg_timer_expired();
                break;

            default:
                SAVE_MSG();
                break;
        }
    }

    RESET_TIMER_EMPTY( CLI_TIMEOUT );
}

std::string &flatten_str( std::string &str )
{
    bool l_keep_space = false;

    str.erase( std::remove_if( str.begin(), str.end(), [&]( char ch )
        {
            if( '"' == ch )
            {
                l_keep_space = !l_keep_space;
            }

            if( l_keep_space )
            {
                return false;
            }

            return static_cast<bool>( isspace( ch ) );
        } ), str.end() );

    return str;
}

/***************************************/
/****** MESSAGE HANDLER FUNCTIONS ******/
/***************************************/
static void handle_msg_get_users_resp( const gen_buffer_t *buff, users_t *users )
{
    std::stringstream l_ss;

    l_ss.write( buff->data, buff->len );
    users->deserialize( &l_ss );
}

static void handle_msg_get_calls_resp( const gen_buffer_t *buff, calls_t *calls )
{
    std::stringstream l_ss;

    l_ss.write( buff->data, buff->len );
    calls->deserialize( &l_ss );
}

static void handle_msg_get_tcp_network_data_resp( const cli_tcp_nw_data_t* in, cli_tcp_nw_data_t *out )
{
    *out = *in;
}

static void handle_msg_terminate_call_resp( const terminate_call_resp_t *terminate_call_resp, Terminal *terminal )
{
    *terminal << NL_STR << terminate_call_resp->num_calls_terminated << " call(s) terminated" << NL_STR << NL_STR;
}

static void handle_msg_delete_user_resp_resp( const delete_user_resp_t *delete_user_resp, Terminal *terminal )
{
    *terminal << NL_STR << delete_user_resp->num_users_deleted << " user(s) deleted" << NL_STR << NL_STR;
}

static void handle_msg_die( Terminal *terminal )
{
    *terminal << NL_STR << NL_STR << KCYN << "  Application stopped, have a nice day!" << KNRM << NL_STR << NL_STR;
    throw ExitException();
}

static void handle_msg_timer_expired( void )
{
    throw TimeoutException();
}

/***************************************/
/*** THREAD MAIN FUNCTION / EVENTLOOP **/
/***************************************/
void cli( config_t *config )
{
    Logger::set_tlid( "CLI" );
    MsgBus::register_thread( msg_t::endpoint_t::CLI );
    DEBUG( "Starting thread, TID: %u", gettid() );

    try
    {
        std::string            l_input;
        std::string            l_endpoint;
        History                l_history;
        poll_t                 l_pollfds;
        const elems_t          l_cli_elems( CLI_TREE );
        Terminal               l_terminal( config->cli_sock_path );

        l_pollfds.add_fd( l_terminal.get_socket() );
        l_pollfds.add_fd( MsgBus::get_cli_eventfd() );

        l_terminal << PROMPT;

        while( true )
        {
            std::list<std::string> l_params;

            if( 0 > poll( l_pollfds.get_fds(), l_pollfds.get_len(), NO_TIMEOUT ) )
            {
                ERROR( "CLI poll error: %s", strerror( errno ) );
                throw ExitException();
            }

            if( l_pollfds.is_fd_set( MsgBus::get_cli_eventfd() ) )
            {
                DECL_MSG_INPUT_DATA();

                switch( MSG_INPUT() )
                {
                    case msg_type_t::DIE:
                        handle_msg_die( &l_terminal );
                        break;

                    default:
                        /* nothing to do */
                        break;
                }
            }

            if( l_pollfds.is_fd_set( l_terminal.get_socket() ) )
            {
                do
                {
                    int l_symbol = l_terminal.get_symbol();

                    switch( l_symbol )
                    {
                        case NUL:
                            break;

                        case BS:
                            if( 0 < l_input.length() )
                            {
                                l_input.pop_back();
                                l_terminal.delete_back_n_chars( 1 );
                            }

                            break;

                        case LF:
                            l_history.add( l_input );
                            autocomplete( l_cli_elems, true, &l_terminal, &l_input, &l_endpoint, &l_params );

                            if( !l_endpoint.empty() )
                            {
                                calls_t           l_calls;
                                users_t           l_users;
                                cli_tcp_nw_data_t l_tcp;

                                try
                                {
                                    get_process_data( &l_calls, &l_users, &l_tcp );
                                    execute( l_endpoint, l_params, &l_terminal, config, l_tcp, l_calls, l_users );
                                }
                                catch( const TimeoutException &e )
                                {
                                    l_terminal << NL_STR << NL_STR << KYEL << "  Unable to get data!" << KNRM << NL_STR << NL_STR;
                                }

                                l_input.clear();
                                l_terminal << PROMPT;
                            }

                            break;

                        case TAB:
                            autocomplete( l_cli_elems, false, &l_terminal, &l_input, &l_endpoint, &l_params );

                            break;

                        case ARR_UP:
                            l_terminal.delete_back_n_chars( l_input.length() );
                            l_input = l_history.move( direction_t::UP );
                            l_terminal << l_input;

                            break;

                        case ARR_DOWN:
                            l_terminal.delete_back_n_chars( l_input.length() );
                            l_input = l_history.move( direction_t::DOWN );
                            l_terminal << l_input;

                            break;

                        default:
                            l_input += l_symbol;
                            l_terminal << l_symbol;

                            break;
                    }
                } while( l_terminal.has_data() );
            }
        }
    }
    catch( const ExitException &e )
    {
        /* nothing to do */
    }

    unlink( config->cli_sock_path.c_str() );

    DEBUG( "Exiting thread, TID: %u", gettid() );
}
