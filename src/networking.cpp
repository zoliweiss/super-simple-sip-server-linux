#include "networking.h"
#include "messaging.h"

/***************************************/
/* FORWARD DECLARATION OF MSG HANDLERS */
/***************************************/
static void handle_msg_send_packet_udp( packet_t *packet );
static void handle_msg_send_packet_tcp( packet_t *packet );
static void handle_msg_get_tcp_network_data( const std::set<int> &client_sockets );
static void handle_msg_enable_inbound_traffic( bool *is_inbound_packet_processing_allowed );
static void handle_msg_disable_inbound_traffic( bool *is_inbound_packet_processing_allowed );
static void handle_msg_close_tcp_socket( int *client_socket, poll_t *pollfds, std::set<int> *client_sockets );
static void handle_msg_die( void );

/***************************************/
/************ FREE FUNCTIONS ***********/
/***************************************/
bool bind_master_sockets( config_t *config )
{
    struct sockaddr_in l_si_server = {};

    l_si_server.sin_family = AF_INET;
    l_si_server.sin_port = htons( config->server_port );
    l_si_server.sin_addr.s_addr = htonl( INADDR_ANY );

    if( config->tcp_enabled )
    {
        if( -1 == ( config->static_data.master_sockets.tcp = socket( AF_INET, SOCK_STREAM, 0 ) ) )
        {
            ERROR( "TCP socket failed: %s", strerror( errno ) );
            return false;
        }

        if( -1 == bind( config->static_data.master_sockets.tcp, reinterpret_cast<struct sockaddr*>( &l_si_server ), sizeof( l_si_server ) ) )
        {
            ERROR( "TCP bind failed: %s", strerror( errno ) );
            return false;
        }

        if( -1 == listen( config->static_data.master_sockets.tcp, BACKLOG ) )
        {
            ERROR( "TCP listen failed: %s", strerror( errno ) );
            return false;
        }
    }

    if( config->udp_enabled )
    {
        if( -1 == ( config->static_data.master_sockets.udp = socket( AF_INET, SOCK_DGRAM, 0 ) ) )
        {
            ERROR( "UDP socket failed: %s", strerror( errno ) );
            return false;
        }

        if( -1 == bind( config->static_data.master_sockets.udp, reinterpret_cast<struct sockaddr*>( &l_si_server ), sizeof( l_si_server ) ) )
        {
            ERROR( "UDP bind failed: %s", strerror( errno ) );
            return false;
        }
    }

    return true;
}

static void remove_client_socket( const int client_socket, poll_t *pollfds, std::set<int> *client_sockets )
{
    if( client_sockets->contains( client_socket ) )
    {
        close( client_socket );
        pollfds->remove_fd( client_socket );
        client_sockets->erase( client_socket );
    }
}

/***************************************/
/****** MESSAGE HANDLER FUNCTIONS ******/
/***************************************/
static void handle_msg_send_packet_udp( packet_t *packet )
{
    sendto( packet->nw_data.socket, packet->data, packet->len, 0, reinterpret_cast<struct sockaddr*>( &packet->nw_data.ip_data ), sizeof( packet->nw_data.ip_data ) );
}

static void handle_msg_send_packet_tcp( packet_t *packet )
{
    send( packet->nw_data.socket, packet->data, packet->len, 0 );
}

static void handle_msg_get_tcp_network_data( const std::set<int> &client_sockets )
{
    MSG_OUTPUT( GET_TCP_NETWORK_DATA_RESP, CLI, client_sockets.size() );
}

static void handle_msg_enable_inbound_traffic( bool *is_inbound_packet_processing_allowed )
{
    *is_inbound_packet_processing_allowed = true;
}

static void handle_msg_disable_inbound_traffic( bool *is_inbound_packet_processing_allowed )
{
    *is_inbound_packet_processing_allowed = false;
}

static void handle_msg_close_tcp_socket( int *client_socket, poll_t *pollfds, std::set<int> *client_sockets )
{
    remove_client_socket( *client_socket, pollfds, client_sockets );
}

static void handle_msg_die( void )
{
    throw ExitException();
}

/***************************************/
/*** THREAD MAIN FUNCTION / EVENTLOOP **/
/***************************************/
void udp( config_t *config )
{
    Logger::set_tlid( "UDP" );
    MsgBus::register_thread( msg_t::endpoint_t::UDP );
    DEBUG( "Starting thread, TID: %u", gettid() );

    packet_t  l_packet;
    socklen_t l_slen = sizeof( l_packet.nw_data.ip_data );
    poll_t    l_pollfds;
    bool      l_is_inbound_packet_processing_allowed = false;

    l_pollfds.add_fd( config->static_data.master_sockets.udp );
    l_pollfds.add_fd( MsgBus::get_udp_eventfd() );

    try
    {
        while( true )
        {
            if( 0 > poll( l_pollfds.get_fds(), l_pollfds.get_len(), NO_TIMEOUT ) )
            {
                ERROR( "UDP poll error: %s", strerror( errno ) );
                throw ExitException();
            }

            if( l_pollfds.is_fd_set( MsgBus::get_udp_eventfd() ) )
            {
                DECL_MSG_INPUT_DATA();

                switch( MSG_INPUT() )
                {
                    case msg_type_t::SEND_PACKET:
                        handle_msg_send_packet_udp( reinterpret_cast<packet_t*>( MSG_INPUT_DATA ) );
                        break;

                    case msg_type_t::ENABLE_INBOUND_TRAFFIC:
                        handle_msg_enable_inbound_traffic( &l_is_inbound_packet_processing_allowed );
                        break;

                    case msg_type_t::DISABLE_INBOUND_TRAFFIC:
                        handle_msg_disable_inbound_traffic( &l_is_inbound_packet_processing_allowed );
                        break;

                    case msg_type_t::DIE:
                        handle_msg_die();
                        break;

                    default:
                        /* nothing to do */
                        break;
                }
            }
            else if( l_is_inbound_packet_processing_allowed )
            {
                if( l_pollfds.is_fd_set( config->static_data.master_sockets.udp ) )
                {
                    l_packet.len = recvfrom( config->static_data.master_sockets.udp, &l_packet.data, BUFF_SIZE - 1, 0, reinterpret_cast<struct sockaddr*>( &l_packet.nw_data.ip_data ), &l_slen );

                    if( -1 == l_packet.len )
                    {
                        WARN( "UDP recvfrom error: %s", strerror( errno ) );
                    }
                    else if( 0 == l_packet.len )
                    {
                        DEBUG( "Empty UDP packet received" );
                    }
                    else
                    {
                        l_packet.nw_data.socket = config->static_data.master_sockets.udp;
                        l_packet.nw_data.protocol = protocol_t::UDP;
                        l_packet.terminate_buffer();

                        DEBUG( "Received UDP packet from %s, length %d", l_packet.nw_data.str().c_str(), l_packet.len );

                        MSG_OUTPUT( PACKET_RECEIVED, SIP, l_packet );
                    }
                }
            }
        }
    }
    catch( const ExitException &e )
    {
        /* nothing to do */
    }

    DEBUG( "Exiting thread, TID: %u", gettid() );
}

void tcp( config_t *config )
{
    Logger::set_tlid( "TCP" );
    MsgBus::register_thread( msg_t::endpoint_t::TCP );
    DEBUG( "Starting thread, TID: %u", gettid() );

    packet_t      l_packet;
    socklen_t     l_slen = sizeof( l_packet.nw_data.ip_data );
    poll_t        l_pollfds;
    std::set<int> l_client_sockets;
    bool          l_is_inbound_packet_processing_allowed = false;

    l_pollfds.add_fd( config->static_data.master_sockets.tcp );
    l_pollfds.add_fd( MsgBus::get_tcp_eventfd() );

    try
    {
        while( true )
        {
            l_packet.init();
            l_packet.nw_data.protocol = protocol_t::TCP;

            if( 0 > poll( l_pollfds.get_fds(), l_pollfds.get_len(), NO_TIMEOUT ) )
            {
                ERROR( "TCP poll error: %s", strerror( errno ) );
                throw ExitException();
            }

            if( l_pollfds.is_fd_set( MsgBus::get_tcp_eventfd() ) )
            {
                DECL_MSG_INPUT_DATA();

                switch( MSG_INPUT() )
                {
                    case msg_type_t::SEND_PACKET:
                        handle_msg_send_packet_tcp( reinterpret_cast<packet_t*>( MSG_INPUT_DATA ) );
                        break;

                    case msg_type_t::GET_TCP_NETWORK_DATA:
                        handle_msg_get_tcp_network_data( l_client_sockets );
                        break;

                    case msg_type_t::ENABLE_INBOUND_TRAFFIC:
                        handle_msg_enable_inbound_traffic( &l_is_inbound_packet_processing_allowed );
                        break;

                    case msg_type_t::DISABLE_INBOUND_TRAFFIC:
                        handle_msg_disable_inbound_traffic( &l_is_inbound_packet_processing_allowed );
                        break;

                    case msg_type_t::CLOSE_TCP_SOCKET:
                        handle_msg_close_tcp_socket( reinterpret_cast<int*>( MSG_INPUT_DATA ), &l_pollfds, &l_client_sockets );
                        break;

                    case msg_type_t::DIE:
                        handle_msg_die();
                        break;

                    default:
                        /* nothing to do */
                        break;
                }
            }
            else if( l_is_inbound_packet_processing_allowed )
            {
                if( l_pollfds.is_fd_set( config->static_data.master_sockets.tcp ) )
                {
                    if( 0 > ( l_packet.nw_data.socket = accept( config->static_data.master_sockets.tcp, reinterpret_cast<struct sockaddr*>( &l_packet.nw_data.ip_data ), &l_slen ) ) )
                    {
                        WARN( "TCP accept error from %s", l_packet.nw_data.str().c_str() );
                    }
                    else
                    {
                        getpeername( l_packet.nw_data.socket, reinterpret_cast<struct sockaddr*>( &l_packet.nw_data.ip_data ), &l_slen );

                        INFO( "New TCP connection on master socket %d, client %s", config->static_data.master_sockets.tcp, l_packet.nw_data.str().c_str() );

                        l_pollfds.add_fd( l_packet.nw_data.socket );
                        l_client_sockets.insert( l_packet.nw_data.socket );
                    }
                }

                for( const auto &client_socket : l_client_sockets )
                {
                    if( l_pollfds.is_fd_set( client_socket ) )
                    {
                        l_packet.nw_data.socket = client_socket;

                        getpeername( l_packet.nw_data.socket, reinterpret_cast<struct sockaddr*>( &l_packet.nw_data.ip_data ), &l_slen );

                        l_packet.len = read( client_socket, &l_packet.data, BUFF_SIZE - 1 );

                        if( -1 == l_packet.len )
                        {
                            WARN( "TCP read error: %s", strerror( errno ) );
                        }
                        else if( 0 == l_packet.len )
                        {
                            DEBUG( "TCP host disconnected from %s", l_packet.nw_data.str().c_str() );

                            remove_client_socket( client_socket, &l_pollfds, &l_client_sockets );

                            MSG_OUTPUT( REPORT_CLOSED_CONNECTIONS, SIP, l_packet.nw_data );

                            break;
                        }
                        else
                        {
                            l_packet.terminate_buffer();

                            DEBUG( "Received TCP packet from %s, length %d", l_packet.nw_data.str().c_str(), l_packet.len );

                            MSG_OUTPUT( PACKET_RECEIVED, SIP, l_packet );
                        }
                    }
                }
            }
        }
    }
    catch( const ExitException &e )
    {
        /* nothing to do */
    }

    DEBUG( "Exiting thread, TID: %u", gettid() );
}
