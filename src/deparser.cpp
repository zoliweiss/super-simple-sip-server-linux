#include "deparser.h"

/***************************************/
/************ FREE FUNCTIONS ***********/
/***************************************/
static std::string deparse_response_code( const sip_status_t response_code )
{
    switch( response_code )
    {
        case sip_status_t::SIP_TRYING:
            return "Trying";
        case sip_status_t::SIP_RINGING:
            return "Ringing";
        case sip_status_t::SIP_OK:
            return "OK";
        case sip_status_t::SIP_UNAUTHORIZED:
            return "Unauthorized";
        case sip_status_t::SIP_FORBIDDEN:
            return "Forbidden";
        case sip_status_t::SIP_NOT_FOUND:
            return "Not Found";
        case sip_status_t::SIP_TEMP_UNAVAIL:
            return "Temporarily Unavailable";
        case sip_status_t::SIP_CALL_NOT_EXIST:
            return "Call Does Not Exist";
        case sip_status_t::SIP_BUSY_HERE:
            return "Busy Here";
        case sip_status_t::SIP_REQUEST_TERM:
            return "Request Terminated";
        case sip_status_t::SIP_NOT_ACC_HERE:
            return "Not Acceptable Here";
        case sip_status_t::SIP_SERVER_ERROR:
            return "Server Internal Error";
        case sip_status_t::SIP_NOT_IMPLEMENT:
            return "Not Implemented";
        case sip_status_t::SIP_SERVICE_UNAVAIL:
            return "Service Unavailable";
        case sip_status_t::SIP_DECLINE:
            return "Decline";
        case sip_status_t::SIP_STATUS_NOT_DEF:
            return "Kiskutya";
    }

    return "Kiskutya";
}

static void deparse_static( const char *header, char *buff, int *buff_len )
{
    *buff_len += snprintf( buff + *buff_len,
        BUFF_SIZE - *buff_len - 1,
        "%s\r\n",
        header );
}

static void deparse_simple( const int value, const char *header, char *buff, int *buff_len )
{
    *buff_len += snprintf( buff + *buff_len,
        BUFF_SIZE - *buff_len - 1,
        "%s: %d\r\n",
        header, value );

}

static void deparse_simple( const std::string &value, const char *header, char *buff, int *buff_len )
{
    if( !value.empty() )
    {
        *buff_len += snprintf( buff + *buff_len,
            BUFF_SIZE - *buff_len - 1,
            "%s: %s\r\n",
            header, value.c_str() );
    }
}

static void deparse_startline( const message_t &received_message, const std::string &server_ip_address, const bool is_request, char *buff, int *buff_len )
{
    if( is_request )
    {
        *buff_len += snprintf( buff + *buff_len,
            BUFF_SIZE - *buff_len - 1,
            "%s sip:%s@%s SIP/2.0\r\n",
            sip_method_t_str[received_message.method], received_message.ruri.name.c_str(), server_ip_address.c_str() );
    }
    else
    {
        *buff_len += snprintf( buff + *buff_len,
            BUFF_SIZE - *buff_len - 1,
            "SIP/2.0 %d %s\r\n",
            received_message.response_code, deparse_response_code( received_message.response_code ).c_str() );
    }
}

static void deparse_vias( const vias_t &vias, const std::string &server_ip_address, const int server_port, const bool add_own, const bool drop_topmost, const protocol_t protocol, char *buff, int *buff_len )
{
    int l_limit = 0;

    if( drop_topmost )
    {
        l_limit = 1;
    }

    if( add_own )
    {
        *buff_len += snprintf( buff + *buff_len,
            BUFF_SIZE - *buff_len - 1,
            "Via: SIP/2.0/%s %s:%d;branch=z9hG4bK%d\r\n",
            protocol_t_str[protocol], server_ip_address.c_str(), server_port, rand() );
    }

    for( size_t i = l_limit; i < vias.size(); i++ )
    {
        *buff_len += snprintf( buff + *buff_len,
            BUFF_SIZE - *buff_len - 1,
            "Via: %s\r\n",
            vias[i].full.c_str() );
    }
}

static void deparse_uri( const uri_t &uri, const char *header_name, const std::string &server_ip_address, const user_t *user, const sip_status_t response_code, const int port, char *buff, int *buff_len )
{
    char l_tag[100] = "";
    char l_display_name[100] = "";
    char l_port[100] = "";

    if( sip_status_t::SIP_TRYING != response_code
     && sip_status_t::SIP_STATUS_NOT_DEF != response_code
     && uri.tag.empty() )
    {
        snprintf( l_tag, sizeof( l_tag ), ";tag=%d", rand() );
    }
    else if( !uri.tag.empty() )
    {
        snprintf( l_tag, sizeof( l_tag ), ";tag=%s", uri.tag.c_str() );
    }

    if( nullptr != user
     && !user->display_name.empty() )
    {
        snprintf( l_display_name, sizeof( l_display_name ), "%s ", user->display_name.c_str() );
    }

    if( 0 != port )
    {
        snprintf( l_port, sizeof( l_port ), ":%d", port );
    }

    if( uri.name.empty() )
    {
        *buff_len += snprintf( buff + *buff_len,
            BUFF_SIZE - *buff_len - 1,
            "%s: <sip:%s%s>%s\r\n",
            header_name, server_ip_address.c_str(), l_port, l_tag );
    }
    else
    {
        *buff_len += snprintf( buff + *buff_len,
            BUFF_SIZE - *buff_len - 1,
            "%s: %s<sip:%s@%s%s>%s\r\n",
            header_name, l_display_name, uri.name.c_str(), server_ip_address.c_str(), l_port, l_tag );
    }
}

static void deparse_call_id( const std::string &call_id, char *buff, int *buff_len )
{
    *buff_len += snprintf( buff + *buff_len,
        BUFF_SIZE - *buff_len - 1,
        "Call-ID: %s\r\n",
        call_id.c_str() );
}

static void deparse_cseq( const cseq_t &cseq, char *buff, int *buff_len )
{
    *buff_len += snprintf( buff + *buff_len,
        BUFF_SIZE - *buff_len - 1,
        "CSeq: %d %s\r\n",
        cseq.num, sip_method_t_str[cseq.method] );
}

static void deparse_www_authenticate( const std::string &server_ip_address, const user_t *user, char *buff, int *buff_len )
{
    if( nullptr == user )
    {
        return;
    }

    *buff_len += snprintf( buff + *buff_len,
        BUFF_SIZE - *buff_len - 1,
        "WWW-Authenticate: Digest \
realm=\"%s\", \
qop=\"auth\", \
algorithm=MD5, \
nonce=\"%s\", \
opaque=\"%s\"\r\n",
        server_ip_address.c_str(),
        user->nonce.c_str(),
        user->opaque.c_str() );
}

static void deparse_content( const content_t &content, char *buff, int *buff_len )
{
    if( 0 != content.len
     && !content.type.empty() )
    {
        *buff_len += snprintf( buff + *buff_len,
            BUFF_SIZE - *buff_len - 1,
            "Content-Type: %s\r\n\
Content-Length: %d\r\n\
\r\n\
%s",
            content.type.c_str(), content.len, content.payload );
    }
    else
    {
        *buff_len += snprintf( buff + *buff_len,
            BUFF_SIZE - *buff_len - 1,
            "Content-Length: 0\r\n\r\n" );
    }
}

static void deparse_supp_req( const supp_req_t &supp_req, const char *header, char *buff, int *buff_len )
{
    if( supp_req.is_set() )
    {
        *buff_len += snprintf( buff + *buff_len,
            BUFF_SIZE - *buff_len - 1,
            "%s: ",
            header );

            if( supp_req.rel100 )
            {
                *buff_len += snprintf( buff + *buff_len,
                  BUFF_SIZE - *buff_len - 1,
                  "100rel\r\n" );
            }
    }
}

static void deparse_allow( const allow_t &allow, char *buff, int *buff_len )
{
    *buff_len += snprintf( buff + *buff_len,
        BUFF_SIZE - *buff_len - 1,
        "Allow: INVITE, ACK, BYE, CANCEL" );

    if( allow.prack )
    {
        *buff_len += snprintf( buff + *buff_len, BUFF_SIZE - *buff_len - 1, ", PRACK" );
    }

    if( allow.update )
    {
        *buff_len += snprintf( buff + *buff_len, BUFF_SIZE - *buff_len - 1, ", UPDATE" );
    }

    *buff_len += snprintf( buff + *buff_len, BUFF_SIZE - *buff_len - 1, "\r\n" );
}

static void deparse_record_route( const routes_t &record_routes, const std::string &server_ip_address, const int server_port, const bool add_own, const protocol_t protocol, char *buff, int *buff_len )
{
    if( add_own )
    {
        *buff_len += snprintf( buff + *buff_len,
            BUFF_SIZE - *buff_len - 1,
            "Record-Route: <sip:%s:%d;transport=%s;lr>\r\n",
            server_ip_address.c_str(), server_port, protocol_t_str[protocol] );
    }

    for( const route_t& i_record_route : record_routes )
    {
        *buff_len += snprintf( buff + *buff_len,
            BUFF_SIZE - *buff_len - 1,
            "Record-Route: %s\r\n",
            i_record_route.full.c_str() );
    }
}

static void deparse_route( const routes_t &routes, char *buff, int *buff_len )
{
    for( size_t i = 1; i < routes.size(); i++ )
    {
        *buff_len += snprintf( buff + *buff_len,
            BUFF_SIZE - *buff_len - 1,
            "Route: %s\r\n",
            routes[i].full.c_str() );
    }
}

int deparse_response( const message_t &received_message, const config_t &config, const user_t *user, const sip_status_t response_code, char *response_buffer )
{
    message_t l_message;
    int       l_response_len = 0;

    l_message.response_code = response_code;

    deparse_startline( l_message, config.static_data.server_ip_address, false, response_buffer, &l_response_len );
    deparse_vias( received_message.vias, config.static_data.server_ip_address, config.server_port, false, false, PROTO_NOT_DEF, response_buffer, &l_response_len );
    deparse_uri( received_message.from, "From", config.static_data.server_ip_address, nullptr, response_code, 0, response_buffer, &l_response_len );
    deparse_uri( received_message.to, "To", config.static_data.server_ip_address, nullptr, response_code, 0, response_buffer, &l_response_len );
    deparse_call_id( received_message.call_id, response_buffer, &l_response_len );
    deparse_cseq( received_message.cseq, response_buffer, &l_response_len );
    deparse_record_route( received_message.record_routes, config.static_data.server_ip_address, config.server_port, false, PROTO_EMPTY, response_buffer, &l_response_len );

    if( sip_status_t::SIP_UNAUTHORIZED == response_code )
    {
        deparse_www_authenticate( config.static_data.server_ip_address, user, response_buffer, &l_response_len );
    }

    if( sip_status_t::SIP_OK == response_code
     && sip_method_t::SIP_REGISTER == received_message.method )
    {
        deparse_uri( received_message.contact, "Contact", config.static_data.server_ip_address, nullptr, sip_status_t::SIP_STATUS_NOT_DEF, config.server_port, response_buffer, &l_response_len );
        deparse_static( "Expires: 900", response_buffer, &l_response_len );
    }

    deparse_content( l_message.content, response_buffer, &l_response_len );

    return l_response_len;
}

int deparse_relay( const message_t &received_message, const config_t &config, const user_t *user, const protocol_t protocol, const call_t &call, char *relay_buffer )
{
    int        l_relay_len = 0;
    const bool l_is_request = received_message.is_request();
    const bool l_drop_topmost_via = !l_is_request;
    const bool l_add_own_record_route = l_is_request;
    const bool l_add_own_via = l_is_request && ( sip_method_t::SIP_ACK != received_message.method || ( call_state_t::CLEAR_PENDING != call.state && sip_method_t::SIP_ACK == received_message.method ) );

    deparse_startline( received_message, config.static_data.server_ip_address, l_is_request, relay_buffer, &l_relay_len );
    deparse_vias( received_message.vias, config.static_data.server_ip_address, config.server_port, l_add_own_via, l_drop_topmost_via, protocol, relay_buffer, &l_relay_len );
    deparse_uri( received_message.from, "From", config.static_data.server_ip_address, user, received_message.response_code, 0, relay_buffer, &l_relay_len );
    deparse_uri( received_message.to, "To", config.static_data.server_ip_address, nullptr, received_message.response_code, 0, relay_buffer, &l_relay_len );
    deparse_call_id( received_message.call_id, relay_buffer, &l_relay_len );
    deparse_simple( received_message.max_forwards - 1, "Max-Forwards", relay_buffer, &l_relay_len );
    deparse_cseq( received_message.cseq, relay_buffer, &l_relay_len );
    deparse_uri( received_message.contact, "Contact", config.static_data.server_ip_address, nullptr, sip_status_t::SIP_STATUS_NOT_DEF, config.server_port, relay_buffer, &l_relay_len );
    deparse_simple( received_message.rseq, "RSeq", relay_buffer, &l_relay_len );
    deparse_simple( received_message.rack, "RAck", relay_buffer, &l_relay_len );
    deparse_supp_req( received_message.supported, "Supported", relay_buffer, &l_relay_len );
    deparse_supp_req( received_message.require, "Require", relay_buffer, &l_relay_len );
    deparse_allow( received_message.allow, relay_buffer, &l_relay_len );
    deparse_record_route( received_message.record_routes, config.static_data.server_ip_address, config.server_port, l_add_own_record_route, protocol, relay_buffer, &l_relay_len );

    if( l_is_request )
    {
        deparse_route( received_message.routes, relay_buffer, &l_relay_len );
    }

    deparse_content( received_message.content, relay_buffer, &l_relay_len );

    return l_relay_len;
}
